-- For Ads in other sites.
delete from pagestatic where pageid='advs';

INSERT INTO pagestatic (pageid,pagename,title,content)
VALUES ('advs','Advertisements','Advertisements',
' 
<h3 class="orange-text">Business apps in Bael</h3>

<div class="row">

	<div class="col l4 m6 s12 ">
	<div class="card medium"><div class="card-content">

		<div class="card-title"> <h5> Reservations App</h5> </div>

		<p class="red-text">Software to manage table reservations</p>

		<p> * Add timings 			</p>
		<p> * Begin table reservations 	</p>
		<p> * Free for first three months	</p>
		<p class="chip"><b> * 10 US$ / month	</b></p>

	</div>
		<div class="card-action"> 
		<a href="http://www.bael.io/leaf/table_reservation" class="btn red center"> 		Reservation Software &rarr; </a> </div>
		</div>
	</div>


	<div class="col l4 m6 s12 ">
	<div class="card medium">
	<div class="card-content">

		<div class="card-title"> <h5> Restaurant App</h5>
			</div>


		<p class="red-text"> Software to manage orders, kitchen and billing  		</p>

		<p> * Add menu categories and items  		</p>
		<p> * Add taxes. Start Operations 					</p>
		<p> * Free for first three months	</p>
		<p class="chip"> <b>* 20 US$ / month</b>	</p>


	</div>
		<div class="card-action"> 
		     <a href="http://www.bael.io/leaf/restaurant" class="btn red center"> 
		     			Restaurant Software &rarr; </a> </div>
			</div>
	</div>


	<div class="col l4 m6 s12 ">
	<div class="card medium">
	<div class="card-content">

		<div class="card-title">	<h5>Coffee Shop App</h5>
			</div>


		<p class="red-text"> Software to manage orders and billing  		</p>
		<p> * Coffee shop POS		</p>
		<p> * Add taxes. Start Operations 					</p>
		<p> * Free for first three months	</p>
		<p class="chip"><b> * 10 US$ / month </b>	</p>


	</div>
		<div class="card-action"> <a href="http://www.bael.io/leaf/foodtakeout" class="btn red center"> Coffee shop Software &rarr; </a> </div>
	</div>
	</div>


	<div class="col l4 m6 s12 ">
	<div class="card medium">
	<div class="card-content">

		<div class="card-title"> <h5>Retail App</h5>
			</div>


		<p class="red-text"> Software to manage orders and billing  		</p>
		<p> * Add menu categories and items  		</p>
		<p> * Add taxes. Start Operations 					</p>
		<p> * Free for first three months	</p>
		<p class="chip"> <b>* 10 US$ / month </b>	</p>


	</div>
		<div class="card-action"> <a href="http://www.bael.io/leaf/retail" class="btn red center"> Retail Software &rarr; </a> </div>
	</div>
	</div>


	<div class="col l4 m6 s12 ">
	<div class="card medium">
	<div class="card-content">

		<div class="card-title"> <h5>Acquisition App </h5>
			</div>


		<p class="red-text"> Software to manage orders and billing  		</p>
		<p> * Create Purchase Orders  		</p>
		<p> * Create returns from Purchase Orders 					</p>
		<p> * Free for first three months	</p>
		<p class="chip"> <b>* 10 US$ / month</b>	</p>


	</div>
		<div class="card-action"> <a href="http://www.bael.io/leaf/acquisition" class="btn red center"> Acquisition Software  &rarr; </a> </div>
	</div>
	</div>

	<div class="col l4 m6 s12 ">
	<div class="card medium">
	<div class="card-content">

		<div class="card-title">	<h5>Reports</h5>
			</div>


		<p class="red-text"> Use various reports   		</p>
		<p> * Report for daily sale	</p>
		<p> * Report for daily tax collections  		</p>
		<p> * Free with Retail,Foodtakeout and Restaurant apps	</p>
	</div>
		<div class="card-action"> <a href="http://www.bael.io/leaf/reports" class="btn red center"> Reports &rarr; </a> </div>
	</div>
	</div>

</div>

');

INSERT into tagsofpage values ('meta-desc',	'advs',1,'Software for Restaurants,Table Reservation, Coffee shops. Browser based software for Point-of-Sale. Manage advs online from anywhere. Manage staff. Monitor sales. ','t');

INSERT into tagsofpage values ('meta-keywords','advs',2,'App for Restaurant');
INSERT into tagsofpage values ('meta-keywords','advs',3,'Restaurant POS');
INSERT into tagsofpage values ('meta-keywords','advs',4,'Restaurant Sofware');
INSERT into tagsofpage values ('meta-keywords','advs',12,'App for Coffee shop');
INSERT into tagsofpage values ('meta-keywords','advs',13,'Coffee shop POS');
INSERT into tagsofpage values ('meta-keywords','advs',14,'Coffee shop Sofware');
INSERT into tagsofpage values ('meta-keywords','advs',22,'App for Retail');
INSERT into tagsofpage values ('meta-keywords','advs',23,'Retail shop POS');
INSERT into tagsofpage values ('meta-keywords','advs',24,'Retail shop Software');
INSERT into tagsofpage values ('meta-keywords','advs',32,'App for Restaurant Table Reservation');
INSERT into tagsofpage values ('meta-keywords','advs',33,'Software for Restaurant reservation');
INSERT into tagsofpage values ('meta-keywords','advs',42,'App for Reports');
INSERT into tagsofpage values ('meta-keywords','advs',43,'Daily Sale Reports');
INSERT into tagsofpage values ('meta-keywords','advs',44,'Daily Tax collected');
