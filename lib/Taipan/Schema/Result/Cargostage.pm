use utf8;
package Taipan::Schema::Result::Cargostage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Cargostage

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<cargostage>

=cut

__PACKAGE__->table("cargostage");

=head1 ACCESSORS

=head2 cargoid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 stageid

  data_type: 'char'
  is_nullable: 1
  size: 24

=head2 reason

  data_type: 'text'
  is_nullable: 1

=head2 country

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 state

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 city

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "cargoid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "stageid",
  { data_type => "char", is_nullable => 1, size => 24 },
  "reason",
  { data_type => "text", is_nullable => 1 },
  "country",
  { data_type => "char", is_nullable => 1, size => 3 },
  "state",
  { data_type => "char", is_nullable => 1, size => 3 },
  "city",
  { data_type => "char", is_nullable => 1, size => 20 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</cargoid>

=item * L</created_at>

=back

=cut

__PACKAGE__->set_primary_key("cargoid", "created_at");

=head1 RELATIONS

=head2 cargoid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->belongs_to(
  "cargoid",
  "Taipan::Schema::Result::Cargo",
  { cargoid => "cargoid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "CASCADE" },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:+UXd105SUswEiS9VCWBcYA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
