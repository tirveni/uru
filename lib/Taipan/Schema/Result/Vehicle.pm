use utf8;
package Taipan::Schema::Result::Vehicle;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Vehicle

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<vehicle>

=cut

__PACKAGE__->table("vehicle");

=head1 ACCESSORS

=head2 vehicleid

  data_type: 'char'
  is_nullable: 0
  size: 24

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 valid

  data_type: 'boolean'
  is_nullable: 1

=head2 ownerid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "vehicleid",
  { data_type => "char", is_nullable => 0, size => 24 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "valid",
  { data_type => "boolean", is_nullable => 1 },
  "ownerid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</vehicleid>

=back

=cut

__PACKAGE__->set_primary_key("vehicleid");

=head1 RELATIONS

=head2 ownerid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "ownerid",
  "Taipan::Schema::Result::Appuser",
  { userid => "ownerid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 trips

Type: has_many

Related object: L<Taipan::Schema::Result::Trip>

=cut

__PACKAGE__->has_many(
  "trips",
  "Taipan::Schema::Result::Trip",
  { "foreign.vehicleid" => "self.vehicleid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tripstages

Type: has_many

Related object: L<Taipan::Schema::Result::Tripstage>

=cut

__PACKAGE__->has_many(
  "tripstages",
  "Taipan::Schema::Result::Tripstage",
  { "foreign.vehicleid" => "self.vehicleid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6MAD8aNMDbO9U38vnOs1pQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
