use utf8;
package Taipan::Schema::Result::Cargo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Cargo

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<cargo>

=cut

__PACKAGE__->table("cargo");

=head1 ACCESSORS

=head2 cargoid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'cargo_cargoid_seq'

=head2 valid

  data_type: 'boolean'
  is_nullable: 1

=head2 product_name

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 comments

  data_type: 'text'
  is_nullable: 1

=head2 stageid

  data_type: 'char'
  default_value: 'BOOKED'
  is_nullable: 1
  size: 24

=head2 shipping_weight

  data_type: 'smallint'
  is_nullable: 1

=head2 src_customerid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=head2 src_idtype

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 src_id

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 src_country

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 src_state

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 src_city

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 20

=head2 dst_customerid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=head2 dst_idtype

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 dst_id

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 dst_country

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 dst_state

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 dst_city

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 20

=head2 is_payby_src

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 is_cartage_paid

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 cartage_price

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 is_door_delivery

  data_type: 'boolean'
  is_nullable: 1

=head2 transporter_invoiceid

  data_type: 'text'
  is_nullable: 1

=head2 transactionid

  data_type: 'bigint'
  is_nullable: 1

=head2 agentid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "cargoid",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "cargo_cargoid_seq",
  },
  "valid",
  { data_type => "boolean", is_nullable => 1 },
  "product_name",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "comments",
  { data_type => "text", is_nullable => 1 },
  "stageid",
  {
    data_type => "char",
    default_value => "BOOKED",
    is_nullable => 1,
    size => 24,
  },
  "shipping_weight",
  { data_type => "smallint", is_nullable => 1 },
  "src_customerid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
  "src_idtype",
  { data_type => "char", is_nullable => 1, size => 32 },
  "src_id",
  { data_type => "char", is_nullable => 1, size => 32 },
  "src_country",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "src_state",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "src_city",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 20 },
  "dst_customerid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
  "dst_idtype",
  { data_type => "char", is_nullable => 1, size => 32 },
  "dst_id",
  { data_type => "char", is_nullable => 1, size => 32 },
  "dst_country",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "dst_state",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "dst_city",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 20 },
  "is_payby_src",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "is_cartage_paid",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "cartage_price",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "is_door_delivery",
  { data_type => "boolean", is_nullable => 1 },
  "transporter_invoiceid",
  { data_type => "text", is_nullable => 1 },
  "transactionid",
  { data_type => "bigint", is_nullable => 1 },
  "agentid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</cargoid>

=back

=cut

__PACKAGE__->set_primary_key("cargoid");

=head1 RELATIONS

=head2 agentid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "agentid",
  "Taipan::Schema::Result::Appuser",
  { userid => "agentid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 cargoaddresses

Type: has_many

Related object: L<Taipan::Schema::Result::Cargoaddress>

=cut

__PACKAGE__->has_many(
  "cargoaddresses",
  "Taipan::Schema::Result::Cargoaddress",
  { "foreign.cargoid" => "self.cargoid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargomore

Type: might_have

Related object: L<Taipan::Schema::Result::Cargomore>

=cut

__PACKAGE__->might_have(
  "cargomore",
  "Taipan::Schema::Result::Cargomore",
  { "foreign.cargoid" => "self.cargoid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargostages

Type: has_many

Related object: L<Taipan::Schema::Result::Cargostage>

=cut

__PACKAGE__->has_many(
  "cargostages",
  "Taipan::Schema::Result::Cargostage",
  { "foreign.cargoid" => "self.cargoid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 city_dst_country_dst_state_dst_city

Type: belongs_to

Related object: L<Taipan::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city_dst_country_dst_state_dst_city",
  "Taipan::Schema::Result::City",
  {
    city_country => "dst_country",
    city_state   => "dst_state",
    citycode     => "dst_city",
  },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 city_src_country_src_state_src_city

Type: belongs_to

Related object: L<Taipan::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city_src_country_src_state_src_city",
  "Taipan::Schema::Result::City",
  {
    city_country => "src_country",
    city_state   => "src_state",
    citycode     => "src_city",
  },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 dst_customerid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "dst_customerid",
  "Taipan::Schema::Result::Appuser",
  { userid => "dst_customerid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 reporttripcargoes

Type: has_many

Related object: L<Taipan::Schema::Result::Reporttripcargo>

=cut

__PACKAGE__->has_many(
  "reporttripcargoes",
  "Taipan::Schema::Result::Reporttripcargo",
  { "foreign.cargoid" => "self.cargoid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 src_customerid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "src_customerid",
  "Taipan::Schema::Result::Appuser",
  { userid => "src_customerid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 tripcargoes

Type: has_many

Related object: L<Taipan::Schema::Result::Tripcargo>

=cut

__PACKAGE__->has_many(
  "tripcargoes",
  "Taipan::Schema::Result::Tripcargo",
  { "foreign.cargoid" => "self.cargoid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:03:07
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dEvEaGhkNCvZ+cJkRMCs1w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
