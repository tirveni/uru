use utf8;
package Taipan::Schema::Result::Reporttrip;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Reporttrip

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<reporttrip>

=cut

__PACKAGE__->table("reporttrip");

=head1 ACCESSORS

=head2 reportid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 tripid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 stageid

  data_type: 'integer'
  is_nullable: 1

=head2 trip_info

  data_type: 'json'
  is_nullable: 1

=head2 trip_stage_info

  data_type: 'json'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "reportid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "tripid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "stageid",
  { data_type => "integer", is_nullable => 1 },
  "trip_info",
  { data_type => "json", is_nullable => 1 },
  "trip_stage_info",
  { data_type => "json", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</reportid>

=item * L</tripid>

=back

=cut

__PACKAGE__->set_primary_key("reportid", "tripid");

=head1 RELATIONS

=head2 reportid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Report>

=cut

__PACKAGE__->belongs_to(
  "reportid",
  "Taipan::Schema::Result::Report",
  { reportid => "reportid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "CASCADE" },
);

=head2 reporttripcargoes

Type: has_many

Related object: L<Taipan::Schema::Result::Reporttripcargo>

=cut

__PACKAGE__->has_many(
  "reporttripcargoes",
  "Taipan::Schema::Result::Reporttripcargo",
  {
    "foreign.reportid" => "self.reportid",
    "foreign.tripid"   => "self.tripid",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tripid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Trip>

=cut

__PACKAGE__->belongs_to(
  "tripid",
  "Taipan::Schema::Result::Trip",
  { tripid => "tripid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "CASCADE" },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ih2IFLb0ZFFvPif+E8mG+w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
