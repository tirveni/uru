use utf8;
package Taipan::Schema::Result::Appuser;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Appuser

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<appuser>

=cut

__PACKAGE__->table("appuser");

=head1 ACCESSORS

=head2 userid

  data_type: 'text'
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 details

  data_type: 'text'
  is_nullable: 1

=head2 password

  data_type: 'text'
  is_nullable: 1

=head2 date_joined

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 active

  data_type: 'boolean'
  is_nullable: 1

=head2 role

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 8

=head2 dob

  data_type: 'date'
  is_nullable: 1

=head2 sex

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 email

  data_type: 'text'
  is_nullable: 1

=head2 verification_code

  data_type: 'text'
  is_nullable: 1

=head2 podid

  data_type: 'char'
  is_nullable: 1
  size: 12

=head2 phone

  data_type: 'char'
  is_nullable: 1
  size: 24

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "userid",
  { data_type => "text", is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "details",
  { data_type => "text", is_nullable => 1 },
  "password",
  { data_type => "text", is_nullable => 1 },
  "date_joined",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "active",
  { data_type => "boolean", is_nullable => 1 },
  "role",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 8 },
  "dob",
  { data_type => "date", is_nullable => 1 },
  "sex",
  { data_type => "char", is_nullable => 1, size => 1 },
  "email",
  { data_type => "text", is_nullable => 1 },
  "verification_code",
  { data_type => "text", is_nullable => 1 },
  "podid",
  { data_type => "char", is_nullable => 1, size => 12 },
  "phone",
  { data_type => "char", is_nullable => 1, size => 24 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</userid>

=back

=cut

__PACKAGE__->set_primary_key("userid");

=head1 RELATIONS

=head2 appuserkey_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Appuserkey>

=cut

__PACKAGE__->has_many(
  "appuserkey_update_userids",
  "Taipan::Schema::Result::Appuserkey",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 appuserkey_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Appuserkey>

=cut

__PACKAGE__->has_many(
  "appuserkey_userids",
  "Taipan::Schema::Result::Appuserkey",
  { "foreign.userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargo_agentids

Type: has_many

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->has_many(
  "cargo_agentids",
  "Taipan::Schema::Result::Cargo",
  { "foreign.agentid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargo_dst_customerids

Type: has_many

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->has_many(
  "cargo_dst_customerids",
  "Taipan::Schema::Result::Cargo",
  { "foreign.dst_customerid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargo_src_customerids

Type: has_many

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->has_many(
  "cargo_src_customerids",
  "Taipan::Schema::Result::Cargo",
  { "foreign.src_customerid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargo_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->has_many(
  "cargo_update_userids",
  "Taipan::Schema::Result::Cargo",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargoaddress_customerids

Type: has_many

Related object: L<Taipan::Schema::Result::Cargoaddress>

=cut

__PACKAGE__->has_many(
  "cargoaddress_customerids",
  "Taipan::Schema::Result::Cargoaddress",
  { "foreign.customerid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargoaddress_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Cargoaddress>

=cut

__PACKAGE__->has_many(
  "cargoaddress_update_userids",
  "Taipan::Schema::Result::Cargoaddress",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargomores

Type: has_many

Related object: L<Taipan::Schema::Result::Cargomore>

=cut

__PACKAGE__->has_many(
  "cargomores",
  "Taipan::Schema::Result::Cargomore",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 cargostages

Type: has_many

Related object: L<Taipan::Schema::Result::Cargostage>

=cut

__PACKAGE__->has_many(
  "cargostages",
  "Taipan::Schema::Result::Cargostage",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 customerinfo_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Customerinfo>

=cut

__PACKAGE__->has_many(
  "customerinfo_update_userids",
  "Taipan::Schema::Result::Customerinfo",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 customerinfo_userid

Type: might_have

Related object: L<Taipan::Schema::Result::Customerinfo>

=cut

__PACKAGE__->might_have(
  "customerinfo_userid",
  "Taipan::Schema::Result::Customerinfo",
  { "foreign.userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 languagetypes

Type: has_many

Related object: L<Taipan::Schema::Result::Languagetype>

=cut

__PACKAGE__->has_many(
  "languagetypes",
  "Taipan::Schema::Result::Languagetype",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 logexceptions

Type: has_many

Related object: L<Taipan::Schema::Result::Logexception>

=cut

__PACKAGE__->has_many(
  "logexceptions",
  "Taipan::Schema::Result::Logexception",
  { "foreign.userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 loginattempts

Type: has_many

Related object: L<Taipan::Schema::Result::Loginattempt>

=cut

__PACKAGE__->has_many(
  "loginattempts",
  "Taipan::Schema::Result::Loginattempt",
  { "foreign.userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 notifications

Type: has_many

Related object: L<Taipan::Schema::Result::Notification>

=cut

__PACKAGE__->has_many(
  "notifications",
  "Taipan::Schema::Result::Notification",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pagestaticlangs

Type: has_many

Related object: L<Taipan::Schema::Result::Pagestaticlang>

=cut

__PACKAGE__->has_many(
  "pagestaticlangs",
  "Taipan::Schema::Result::Pagestaticlang",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pagestatics

Type: has_many

Related object: L<Taipan::Schema::Result::Pagestatic>

=cut

__PACKAGE__->has_many(
  "pagestatics",
  "Taipan::Schema::Result::Pagestatic",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 reports

Type: has_many

Related object: L<Taipan::Schema::Result::Report>

=cut

__PACKAGE__->has_many(
  "reports",
  "Taipan::Schema::Result::Report",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 reporttrips

Type: has_many

Related object: L<Taipan::Schema::Result::Reporttrip>

=cut

__PACKAGE__->has_many(
  "reporttrips",
  "Taipan::Schema::Result::Reporttrip",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 role

Type: belongs_to

Related object: L<Taipan::Schema::Result::Role>

=cut

__PACKAGE__->belongs_to(
  "role",
  "Taipan::Schema::Result::Role",
  { role => "role" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 tagsofpages

Type: has_many

Related object: L<Taipan::Schema::Result::Tagsofpage>

=cut

__PACKAGE__->has_many(
  "tagsofpages",
  "Taipan::Schema::Result::Tagsofpage",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tagtypes

Type: has_many

Related object: L<Taipan::Schema::Result::Tagtype>

=cut

__PACKAGE__->has_many(
  "tagtypes",
  "Taipan::Schema::Result::Tagtype",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 trip_driverids

Type: has_many

Related object: L<Taipan::Schema::Result::Trip>

=cut

__PACKAGE__->has_many(
  "trip_driverids",
  "Taipan::Schema::Result::Trip",
  { "foreign.driverid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 trip_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Trip>

=cut

__PACKAGE__->has_many(
  "trip_update_userids",
  "Taipan::Schema::Result::Trip",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tripcargoes

Type: has_many

Related object: L<Taipan::Schema::Result::Tripcargo>

=cut

__PACKAGE__->has_many(
  "tripcargoes",
  "Taipan::Schema::Result::Tripcargo",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tripstage_driverids

Type: has_many

Related object: L<Taipan::Schema::Result::Tripstage>

=cut

__PACKAGE__->has_many(
  "tripstage_driverids",
  "Taipan::Schema::Result::Tripstage",
  { "foreign.driverid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 tripstage_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Tripstage>

=cut

__PACKAGE__->has_many(
  "tripstage_update_userids",
  "Taipan::Schema::Result::Tripstage",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 typevalues

Type: has_many

Related object: L<Taipan::Schema::Result::Typevalue>

=cut

__PACKAGE__->has_many(
  "typevalues",
  "Taipan::Schema::Result::Typevalue",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 usernotified_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Usernotified>

=cut

__PACKAGE__->has_many(
  "usernotified_update_userids",
  "Taipan::Schema::Result::Usernotified",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 usernotified_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Usernotified>

=cut

__PACKAGE__->has_many(
  "usernotified_userids",
  "Taipan::Schema::Result::Usernotified",
  { "foreign.userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 vehicle_ownerids

Type: has_many

Related object: L<Taipan::Schema::Result::Vehicle>

=cut

__PACKAGE__->has_many(
  "vehicle_ownerids",
  "Taipan::Schema::Result::Vehicle",
  { "foreign.ownerid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 vehicle_update_userids

Type: has_many

Related object: L<Taipan::Schema::Result::Vehicle>

=cut

__PACKAGE__->has_many(
  "vehicle_update_userids",
  "Taipan::Schema::Result::Vehicle",
  { "foreign.update_userid" => "self.userid" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PnNSzJ5wtRPYnRFobmnSQQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
