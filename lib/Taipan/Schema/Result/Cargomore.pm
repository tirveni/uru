use utf8;
package Taipan::Schema::Result::Cargomore;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Cargomore

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<cargomore>

=cut

__PACKAGE__->table("cargomore");

=head1 ACCESSORS

=head2 cargoid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 cargo_type

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 cargo_sub_type

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 supply_type

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 supply_comments

  data_type: 'text'
  is_nullable: 1

=head2 txn_type

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 txn_type_outward

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 txn_type_inward

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 txn_type_comment

  data_type: 'text'
  is_nullable: 1

=head2 cargonumber

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 hsn

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 insurance_info

  data_type: 'text'
  is_nullable: 1

=head2 cargo_value

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 unit_details

  data_type: 'text'
  is_nullable: 1

=head2 unit_number

  data_type: 'smallint'
  is_nullable: 1

=head2 dimension_length

  data_type: 'smallint'
  is_nullable: 1

=head2 dimension_width

  data_type: 'smallint'
  is_nullable: 1

=head2 dimension_thickness

  data_type: 'smallint'
  is_nullable: 1

=head2 amt_hamali

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 amt_green_tax

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 amt_other

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 amt_grand_total

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "cargoid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "cargo_type",
  { data_type => "char", is_nullable => 1, size => 32 },
  "cargo_sub_type",
  { data_type => "char", is_nullable => 1, size => 32 },
  "supply_type",
  { data_type => "char", is_nullable => 1, size => 32 },
  "supply_comments",
  { data_type => "text", is_nullable => 1 },
  "txn_type",
  { data_type => "char", is_nullable => 1, size => 32 },
  "txn_type_outward",
  { data_type => "char", is_nullable => 1, size => 32 },
  "txn_type_inward",
  { data_type => "char", is_nullable => 1, size => 32 },
  "txn_type_comment",
  { data_type => "text", is_nullable => 1 },
  "cargonumber",
  { data_type => "char", is_nullable => 1, size => 32 },
  "hsn",
  { data_type => "char", is_nullable => 1, size => 32 },
  "insurance_info",
  { data_type => "text", is_nullable => 1 },
  "cargo_value",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "unit_details",
  { data_type => "text", is_nullable => 1 },
  "unit_number",
  { data_type => "smallint", is_nullable => 1 },
  "dimension_length",
  { data_type => "smallint", is_nullable => 1 },
  "dimension_width",
  { data_type => "smallint", is_nullable => 1 },
  "dimension_thickness",
  { data_type => "smallint", is_nullable => 1 },
  "amt_hamali",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "amt_green_tax",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "amt_other",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "amt_grand_total",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</cargoid>

=back

=cut

__PACKAGE__->set_primary_key("cargoid");

=head1 RELATIONS

=head2 cargoid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->belongs_to(
  "cargoid",
  "Taipan::Schema::Result::Cargo",
  { cargoid => "cargoid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "CASCADE" },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KzL5glDJ43fU4dUWhLEp+Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
