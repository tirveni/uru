use utf8;
package Taipan::Schema::Result::Reporttripcargo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Reporttripcargo

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<reporttripcargo>

=cut

__PACKAGE__->table("reporttripcargo");

=head1 ACCESSORS

=head2 tripid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 reportid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 entryid

  data_type: 'integer'
  is_nullable: 0

=head2 cargoid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 stageid

  data_type: 'integer'
  is_nullable: 1

=head2 cargo_info

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "tripid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "reportid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "entryid",
  { data_type => "integer", is_nullable => 0 },
  "cargoid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "stageid",
  { data_type => "integer", is_nullable => 1 },
  "cargo_info",
  { data_type => "json", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</reportid>

=item * L</tripid>

=item * L</entryid>

=back

=cut

__PACKAGE__->set_primary_key("reportid", "tripid", "entryid");

=head1 RELATIONS

=head2 cargoid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Cargo>

=cut

__PACKAGE__->belongs_to(
  "cargoid",
  "Taipan::Schema::Result::Cargo",
  { cargoid => "cargoid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 reporttrip

Type: belongs_to

Related object: L<Taipan::Schema::Result::Reporttrip>

=cut

__PACKAGE__->belongs_to(
  "reporttrip",
  "Taipan::Schema::Result::Reporttrip",
  { reportid => "reportid", tripid => "tripid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4kjLPW16Yu+L0xWKUjrTYA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
