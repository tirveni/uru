use utf8;
package Taipan::Schema::Result::VAtrip;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::VAtrip

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<v_atrip>

=cut

__PACKAGE__->table("v_atrip");
__PACKAGE__->result_source_instance->view_definition(" SELECT tr.tripid,\n    tr.driverid AS trip_driver,\n    tr.vehicleid AS trip_vehicle,\n    tr.start_date,\n    tr.src_city AS source_city,\n    tr.dst_city AS destination_city,\n    st.stageid,\n    st.driverid AS stage_driver,\n    st.vehicleid AS stage_vehicle,\n    st.src_city AS source_city_stage,\n    st.dst_city AS destination_city_stage\n   FROM (trip tr\n     LEFT JOIN tripstage st ON ((st.tripid = tr.tripid)))");

=head1 ACCESSORS

=head2 tripid

  data_type: 'integer'
  is_nullable: 1

=head2 trip_driver

  data_type: 'text'
  is_nullable: 1

=head2 trip_vehicle

  data_type: 'char'
  is_nullable: 1
  size: 24

=head2 start_date

  data_type: 'date'
  is_nullable: 1

=head2 source_city

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 destination_city

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 stageid

  data_type: 'smallint'
  is_nullable: 1

=head2 stage_driver

  data_type: 'text'
  is_nullable: 1

=head2 stage_vehicle

  data_type: 'char'
  is_nullable: 1
  size: 24

=head2 source_city_stage

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 destination_city_stage

  data_type: 'char'
  is_nullable: 1
  size: 20

=cut

__PACKAGE__->add_columns(
  "tripid",
  { data_type => "integer", is_nullable => 1 },
  "trip_driver",
  { data_type => "text", is_nullable => 1 },
  "trip_vehicle",
  { data_type => "char", is_nullable => 1, size => 24 },
  "start_date",
  { data_type => "date", is_nullable => 1 },
  "source_city",
  { data_type => "char", is_nullable => 1, size => 20 },
  "destination_city",
  { data_type => "char", is_nullable => 1, size => 20 },
  "stageid",
  { data_type => "smallint", is_nullable => 1 },
  "stage_driver",
  { data_type => "text", is_nullable => 1 },
  "stage_vehicle",
  { data_type => "char", is_nullable => 1, size => 24 },
  "source_city_stage",
  { data_type => "char", is_nullable => 1, size => 20 },
  "destination_city_stage",
  { data_type => "char", is_nullable => 1, size => 20 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UTzPexfG30C3PR7a/XjhAQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
