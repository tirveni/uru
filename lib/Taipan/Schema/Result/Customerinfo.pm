use utf8;
package Taipan::Schema::Result::Customerinfo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Customerinfo

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<customerinfo>

=cut

__PACKAGE__->table("customerinfo");

=head1 ACCESSORS

=head2 userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 src_idtype

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 src_id

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 src_country

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 src_state

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 src_city

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 20

=head2 addressid

  data_type: 'integer'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "src_idtype",
  { data_type => "char", is_nullable => 1, size => 32 },
  "src_id",
  { data_type => "char", is_nullable => 1, size => 32 },
  "src_country",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "src_state",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "src_city",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 20 },
  "addressid",
  { data_type => "integer", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</userid>

=back

=cut

__PACKAGE__->set_primary_key("userid");

=head1 RELATIONS

=head2 city

Type: belongs_to

Related object: L<Taipan::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city",
  "Taipan::Schema::Result::City",
  {
    city_country => "src_country",
    city_state   => "src_state",
    citycode     => "src_city",
  },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "userid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lYROJSb98/Ptkz1cT3NWRw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
