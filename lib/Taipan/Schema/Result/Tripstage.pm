use utf8;
package Taipan::Schema::Result::Tripstage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Tripstage

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<tripstage>

=cut

__PACKAGE__->table("tripstage");

=head1 ACCESSORS

=head2 tripid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 stageid

  data_type: 'smallint'
  is_nullable: 0

=head2 valid

  data_type: 'boolean'
  is_nullable: 1

=head2 start_date

  data_type: 'date'
  is_nullable: 1

=head2 end_date

  data_type: 'date'
  is_nullable: 1

=head2 is_trip_complete

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 began

  data_type: 'timestamp with time zone'
  is_nullable: 1

=head2 reached

  data_type: 'timestamp with time zone'
  is_nullable: 1

=head2 src_country

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 src_state

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 src_city

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 20

=head2 dst_country

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 dst_state

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 3

=head2 dst_city

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 20

=head2 stage_transporterid

  data_type: 'text'
  is_nullable: 1

=head2 vehicle_cost

  data_type: 'numeric'
  is_nullable: 1
  size: [8,2]

=head2 driverid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=head2 vehicleid

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 1
  size: 24

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "tripid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "stageid",
  { data_type => "smallint", is_nullable => 0 },
  "valid",
  { data_type => "boolean", is_nullable => 1 },
  "start_date",
  { data_type => "date", is_nullable => 1 },
  "end_date",
  { data_type => "date", is_nullable => 1 },
  "is_trip_complete",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "began",
  { data_type => "timestamp with time zone", is_nullable => 1 },
  "reached",
  { data_type => "timestamp with time zone", is_nullable => 1 },
  "src_country",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "src_state",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "src_city",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 20 },
  "dst_country",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "dst_state",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 3 },
  "dst_city",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 20 },
  "stage_transporterid",
  { data_type => "text", is_nullable => 1 },
  "vehicle_cost",
  { data_type => "numeric", is_nullable => 1, size => [8, 2] },
  "driverid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
  "vehicleid",
  { data_type => "char", is_foreign_key => 1, is_nullable => 1, size => 24 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</tripid>

=item * L</stageid>

=back

=cut

__PACKAGE__->set_primary_key("tripid", "stageid");

=head1 RELATIONS

=head2 city_dst_country_dst_state_dst_city

Type: belongs_to

Related object: L<Taipan::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city_dst_country_dst_state_dst_city",
  "Taipan::Schema::Result::City",
  {
    city_country => "dst_country",
    city_state   => "dst_state",
    citycode     => "dst_city",
  },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 city_src_country_src_state_src_city

Type: belongs_to

Related object: L<Taipan::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city_src_country_src_state_src_city",
  "Taipan::Schema::Result::City",
  {
    city_country => "src_country",
    city_state   => "src_state",
    citycode     => "src_city",
  },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 driverid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "driverid",
  "Taipan::Schema::Result::Appuser",
  { userid => "driverid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 tripid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Trip>

=cut

__PACKAGE__->belongs_to(
  "tripid",
  "Taipan::Schema::Result::Trip",
  { tripid => "tripid" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "CASCADE" },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);

=head2 vehicleid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Vehicle>

=cut

__PACKAGE__->belongs_to(
  "vehicleid",
  "Taipan::Schema::Result::Vehicle",
  { vehicleid => "vehicleid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:w+gOxLnp9dFHPg1Cx8QQzw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
