use utf8;
package Taipan::Schema::Result::Report;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Report

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<report>

=cut

__PACKAGE__->table("report");

=head1 ACCESSORS

=head2 reportid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'report_reportid_seq'

=head2 status

  data_type: 'char'
  is_nullable: 1
  size: 24

=head2 tripid

  data_type: 'integer'
  is_nullable: 1

=head2 cargoid

  data_type: 'integer'
  is_nullable: 1

=head2 tripstage

  data_type: 'integer'
  is_nullable: 1

=head2 customerid

  data_type: 'text'
  is_nullable: 1

=head2 reason

  data_type: 'text'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "reportid",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "report_reportid_seq",
  },
  "status",
  { data_type => "char", is_nullable => 1, size => 24 },
  "tripid",
  { data_type => "integer", is_nullable => 1 },
  "cargoid",
  { data_type => "integer", is_nullable => 1 },
  "tripstage",
  { data_type => "integer", is_nullable => 1 },
  "customerid",
  { data_type => "text", is_nullable => 1 },
  "reason",
  { data_type => "text", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
  "update_userid",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</reportid>

=back

=cut

__PACKAGE__->set_primary_key("reportid");

=head1 RELATIONS

=head2 reporttrips

Type: has_many

Related object: L<Taipan::Schema::Result::Reporttrip>

=cut

__PACKAGE__->has_many(
  "reporttrips",
  "Taipan::Schema::Result::Reporttrip",
  { "foreign.reportid" => "self.reportid" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 update_userid

Type: belongs_to

Related object: L<Taipan::Schema::Result::Appuser>

=cut

__PACKAGE__->belongs_to(
  "update_userid",
  "Taipan::Schema::Result::Appuser",
  { userid => "update_userid" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:raLjJF4Tag27DzvRkLhAyA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
