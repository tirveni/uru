use utf8;
package Taipan::Schema::Result::VTripcargo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::VTripcargo

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<v_tripcargo>

=cut

__PACKAGE__->table("v_tripcargo");
__PACKAGE__->result_source_instance->view_definition(" SELECT c.cargoid,\n    c.valid,\n    c.product_name,\n    c.description,\n    c.comments,\n    c.stageid,\n    c.shipping_weight,\n    c.src_customerid,\n    c.src_idtype,\n    c.src_id,\n    c.src_country,\n    c.src_state,\n    c.src_city,\n    c.dst_customerid,\n    c.dst_idtype,\n    c.dst_id,\n    c.dst_country,\n    c.dst_state,\n    c.dst_city,\n    c.is_payby_src,\n    c.is_cartage_paid,\n    c.cartage_price,\n    c.is_door_delivery,\n    c.transporter_invoiceid,\n    c.transactionid,\n    c.agentid,\n    c.created_at,\n    c.update_userid,\n    tc.tripid,\n    tc.valid AS tripcargo_valid\n   FROM tripcargo tc,\n    cargo c\n  WHERE (c.cargoid = tc.cargoid)");

=head1 ACCESSORS

=head2 cargoid

  data_type: 'integer'
  is_nullable: 1

=head2 valid

  data_type: 'boolean'
  is_nullable: 1

=head2 product_name

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 comments

  data_type: 'text'
  is_nullable: 1

=head2 stageid

  data_type: 'char'
  is_nullable: 1
  size: 24

=head2 shipping_weight

  data_type: 'smallint'
  is_nullable: 1

=head2 src_customerid

  data_type: 'text'
  is_nullable: 1

=head2 src_idtype

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 src_id

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 src_country

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 src_state

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 src_city

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 dst_customerid

  data_type: 'text'
  is_nullable: 1

=head2 dst_idtype

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 dst_id

  data_type: 'char'
  is_nullable: 1
  size: 32

=head2 dst_country

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 dst_state

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 dst_city

  data_type: 'char'
  is_nullable: 1
  size: 20

=head2 is_payby_src

  data_type: 'boolean'
  is_nullable: 1

=head2 is_cartage_paid

  data_type: 'boolean'
  is_nullable: 1

=head2 cartage_price

  data_type: 'numeric'
  is_nullable: 1
  size: [12,2]

=head2 is_door_delivery

  data_type: 'boolean'
  is_nullable: 1

=head2 transporter_invoiceid

  data_type: 'text'
  is_nullable: 1

=head2 transactionid

  data_type: 'bigint'
  is_nullable: 1

=head2 agentid

  data_type: 'text'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  is_nullable: 1

=head2 update_userid

  data_type: 'text'
  is_nullable: 1

=head2 tripid

  data_type: 'integer'
  is_nullable: 1

=head2 tripcargo_valid

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "cargoid",
  { data_type => "integer", is_nullable => 1 },
  "valid",
  { data_type => "boolean", is_nullable => 1 },
  "product_name",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "comments",
  { data_type => "text", is_nullable => 1 },
  "stageid",
  { data_type => "char", is_nullable => 1, size => 24 },
  "shipping_weight",
  { data_type => "smallint", is_nullable => 1 },
  "src_customerid",
  { data_type => "text", is_nullable => 1 },
  "src_idtype",
  { data_type => "char", is_nullable => 1, size => 32 },
  "src_id",
  { data_type => "char", is_nullable => 1, size => 32 },
  "src_country",
  { data_type => "char", is_nullable => 1, size => 3 },
  "src_state",
  { data_type => "char", is_nullable => 1, size => 3 },
  "src_city",
  { data_type => "char", is_nullable => 1, size => 20 },
  "dst_customerid",
  { data_type => "text", is_nullable => 1 },
  "dst_idtype",
  { data_type => "char", is_nullable => 1, size => 32 },
  "dst_id",
  { data_type => "char", is_nullable => 1, size => 32 },
  "dst_country",
  { data_type => "char", is_nullable => 1, size => 3 },
  "dst_state",
  { data_type => "char", is_nullable => 1, size => 3 },
  "dst_city",
  { data_type => "char", is_nullable => 1, size => 20 },
  "is_payby_src",
  { data_type => "boolean", is_nullable => 1 },
  "is_cartage_paid",
  { data_type => "boolean", is_nullable => 1 },
  "cartage_price",
  { data_type => "numeric", is_nullable => 1, size => [12, 2] },
  "is_door_delivery",
  { data_type => "boolean", is_nullable => 1 },
  "transporter_invoiceid",
  { data_type => "text", is_nullable => 1 },
  "transactionid",
  { data_type => "bigint", is_nullable => 1 },
  "agentid",
  { data_type => "text", is_nullable => 1 },
  "created_at",
  { data_type => "timestamp with time zone", is_nullable => 1 },
  "update_userid",
  { data_type => "text", is_nullable => 1 },
  "tripid",
  { data_type => "integer", is_nullable => 1 },
  "tripcargo_valid",
  { data_type => "boolean", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yNeL+9SWDy32laEsHmLSTA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
