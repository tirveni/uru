use utf8;
package Taipan::Schema::Result::VCargo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::VCargo

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<v_cargo>

=cut

__PACKAGE__->table("v_cargo");
__PACKAGE__->result_source_instance->view_definition(" SELECT cr.cargoid,\n    cr.valid,\n    cr.src_customerid,\n    cr.dst_customerid,\n    cr.product_name,\n    tr.tripid,\n    tr.valid AS is_ontheway,\n    tr.is_delivered,\n    tr.delivered_time\n   FROM (cargo cr\n     LEFT JOIN tripcargo tr ON ((cr.cargoid = tr.cargoid)))\n  WHERE (cr.valid = true)");

=head1 ACCESSORS

=head2 cargoid

  data_type: 'integer'
  is_nullable: 1

=head2 valid

  data_type: 'boolean'
  is_nullable: 1

=head2 src_customerid

  data_type: 'text'
  is_nullable: 1

=head2 dst_customerid

  data_type: 'text'
  is_nullable: 1

=head2 product_name

  data_type: 'text'
  is_nullable: 1

=head2 tripid

  data_type: 'integer'
  is_nullable: 1

=head2 is_ontheway

  data_type: 'boolean'
  is_nullable: 1

=head2 is_delivered

  data_type: 'boolean'
  is_nullable: 1

=head2 delivered_time

  data_type: 'timestamp with time zone'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "cargoid",
  { data_type => "integer", is_nullable => 1 },
  "valid",
  { data_type => "boolean", is_nullable => 1 },
  "src_customerid",
  { data_type => "text", is_nullable => 1 },
  "dst_customerid",
  { data_type => "text", is_nullable => 1 },
  "product_name",
  { data_type => "text", is_nullable => 1 },
  "tripid",
  { data_type => "integer", is_nullable => 1 },
  "is_ontheway",
  { data_type => "boolean", is_nullable => 1 },
  "is_delivered",
  { data_type => "boolean", is_nullable => 1 },
  "delivered_time",
  { data_type => "timestamp with time zone", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 19:05:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kdZj7PjQkGMH8zGj34VekA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
