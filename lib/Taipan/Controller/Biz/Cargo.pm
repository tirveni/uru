package Taipan::Controller::Biz::Cargo;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

use TryCatch;

use Class::Utils
  qw(makeparm selected_language unxss unxss_an xnatural xfloat valid_email
     chomp_date valid_date get_array_from_argument trim user_login);

use Class::Address;
use Class::Currency;
use Class::General;

my ($rows_per_page,$c_userid,$h_rest);
{
  $rows_per_page = 10;
}


=head1 NAME

Taipan::Controller::Biz::Branch - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 Listing for the business

List stuff for the business

=cut


=head2 auto

Permission is checked here

=cut

sub auto : Private
{
  my $self		= shift;
  my $c			= shift;

  my $f = "Biz/List/auto";
  #$c->log->info("$f Begin  $in_branchid");

  my ($h_i,$errors);
  $c_userid = user_login($c);

  my $dbic = $c->model('TDB')->schema;

  my $is_allowed = 1;

  if ($is_allowed > 0)
  {
    $h_i->{admin}	=	1;
  }
  else
  {
    my $msg = pop(@$errors);
    $self->status_forbidden($c,message => "Unauthorized Access, $msg");
    return;
  }
}

=head1 Cargo

List cargo.

=cut

sub index :Local :Path(/biz/cargo)  :ActionClass('REST') { }

=head2 index /biz/cargo		 GET

Get Cargo Infox

=cut

sub index_GET
{
  my $self		= shift;
  my $c			= shift;
  my $in_cargoid	= shift;

  my $fn			= "Biz/cargo_GET";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_cargo,$o_cargo);

  $o_cargo = Biz::Cargo->new($dbic,$in_cargoid);
  $row_cargo = $o_cargo->dbrecord
    if(defined($o_cargo));

  ##Fill in the Data
  if (defined($row_cargo))
  {
    my $h_cargo = cargo_info($dbic,$o_cargo,$row_cargo);
    push(@list,$h_cargo);
  }

  $c->log->info("$fn Cargo:$o_cargo, $row_cargo");

  ##Success?
  if($o_cargo)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    my $msg = "$err_msg Cannot find cargo";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}

=head2 index /biz/cargo		 PUT

Edit Cargo.

=cut

sub index_PUT
{
  my $self		= shift;
  my $c			= shift;
  my $in_cargoid	= shift;

  my $fn			= "Biz/cargo_PUT";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);
  my ($err_msg,$row_cargo,$o_cargo,$row_cargo_updated);
  my $x_reason	= $in_data->{reason} || 'Edit';

  ##Exception: Handle city Data
  arrange_city($c,$in_data);

  $o_cargo = Biz::Cargo->new($dbic,$in_cargoid);
  $row_cargo = $o_cargo->dbrecord
    if(defined($o_cargo));


  ##Edit Cargo
  if($x_reason && $o_cargo)
  {
    ($err_msg,$row_cargo_updated) =
      $o_cargo->edit($in_data,$c_userid,$x_reason);
    $c->log->info("$fn Editing Cargo,$err_msg");
  }

  ##Fill in the Data
  if (defined($row_cargo_updated))
  {
    my %x_data	= $row_cargo->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);
  }

  $c->log->info("$fn Cargo:$o_cargo, $row_cargo");

  ##Success?
  if ($row_cargo_updated)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  elsif($o_cargo && $row_cargo)
  {
    my $msg = "$err_msg Cannot edit cargo";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }
  else
  {
    my $msg = "$err_msg Cannot find cargo";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }

}


=head2 index /biz/cargo		 DELETE

DELETE Cargo. This Disables DELETE

=cut

sub index_DELETE
{
  my $self		= shift;
  my $c			= shift;
  my $in_cargoid	= shift;

  my $fn			= "Biz/cargo_DELETE";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);
  my ($err_msg,$row_cargo,$o_cargo,$row_cargo_updated);
  my $x_reason	= $in_data->{reason};

  $o_cargo = Biz::Cargo->new($dbic,$in_cargoid);
  $row_cargo = $o_cargo->dbrecord
    if(defined($o_cargo));

  ##Change Stage
  if ($x_reason)
  {
    ($err_msg,$row_cargo_updated) =
      $o_cargo->disable($in_data,$c_userid,$x_reason);
  }


  ##Fill in the Data
  if (defined($row_cargo_updated))
  {
    my $h_cargo = cargo_info($dbic,$o_cargo,$row_cargo);
    push(@list,$h_cargo);
  }

  $c->log->info("$fn Cargo:$o_cargo, $row_cargo");

  ##Success?
  ##Success?
  if ($row_cargo_updated)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  elsif($o_cargo && $row_cargo)
  {
    my $msg = "$err_msg Cannot delete cargo";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }
  else
  {
    my $msg = "$err_msg Cannot find cargo";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}



=head2 index /biz/cargo		 POST

Add Cargo

=cut

sub index_POST
{
  my $self		= shift;
  my $c			= shift;

  my $fn			= "Biz/cargo";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_cargo,$o_cargo);

  ##Exception: Handle city Data
  arrange_city($c,$in_data);

  ##Add IF got the Data
  if ($c_userid && $in_data)
  {
    ($err_msg,$row_cargo,$o_cargo)= Biz::Cargo::create
      ($dbic,$in_data,$c_userid);
  }

  ##Fill in the Data
  if (defined($row_cargo))
  {
    my %x_data = $row_cargo->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);
  }

  $c->log->info("$fn Cargo:$o_cargo, $row_cargo");

  ##Success?
  if($o_cargo)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    my $msg = "$err_msg Cannot add cargo";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }


}


=head1 PVT Functions

=head2 cargo_info

Get cargo JSON hash from here.

=cut

sub cargo_info
{
  my $dbic	=	shift;
  my $o_cargo	=	shift;
  my $row_cargo	=	shift;

  my $fn = "Cargo/cargo_info";
  my $h_xd = $o_cargo->full_info($dbic,$row_cargo);

  return $h_xd;
}


=head2 arrange_city

Handles Exception: City,State,Country Input

=cut

sub arrange_city
{
  my $c		= shift;
  my $in_data	= shift;

  my $fn = "Cargo/arrange_city";

  if ($in_data)
  {

    my ($src_co,$src_st,$src_ci);
    $src_co = $in_data->{src_country};
    $src_st = $in_data->{src_state};
    $src_ci = $in_data->{src_city};
    if (!$src_co || !$src_st || !$src_ci)
    {
      my ($co_code,$st_code,$city_code) =
	Class::City::sin_city($in_data,'src_citycode');
      $in_data->{src_country}	= $co_code;
      $in_data->{src_state}	= $st_code;
      $in_data->{src_city}	= $city_code;
      $c->log->info("$fn $co_code/$st_code/$city_code");
    }
  }

  if ($in_data)
  {

    my ($dst_co,$dst_st,$dst_ci);
    $dst_co = $in_data->{dst_country};
    $dst_st = $in_data->{dst_state};
    $dst_ci = $in_data->{dst_city};
    if (!$dst_co || !$dst_st || !$dst_ci)
    {
      my ($co_code,$st_code,$city_code) =
	Class::City::sin_city($in_data,'dst_citycode');
      $in_data->{dst_country}	= $co_code;
      $in_data->{dst_state}	= $st_code;
      $in_data->{dst_city}	= $city_code;
      $c->log->info("$fn $co_code/$st_code/$city_code");
    }
  }

  return $in_data;
}

=head1 AUTHOR

Tirveni Yadav,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
