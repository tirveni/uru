package Taipan::Controller::Biz::Mover;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

use TryCatch;

use Class::Utils
  qw(makeparm selected_language unxss unxss_an xnatural xfloat valid_email
     chomp_date valid_date get_array_from_argument trim user_login);

use Class::Address;
use Class::Currency;
use Class::General;

use Biz::Mover;

my ($rows_per_page,$c_userid,$h_rest);
{
  $rows_per_page = 10;
}


=head1 NAME

Taipan::Controller::Biz::Mover

=head1 DESCRIPTION

Mover: Agent,Staff of URU.

=head2 auto

Permission is checked here

=cut

sub auto : Private
{
  my $self		= shift;
  my $c			= shift;

  my $f = "Biz/List/auto";
  #$c->log->info("$f Begin  $in_branchid");

  my ($h_i,$errors);
  $c_userid = user_login($c);

  my $dbic = $c->model('TDB')->schema;

  my $is_allowed = 1;

  if ($is_allowed > 0)
  {
    $h_i->{admin}	=	1;
  }
  else
  {
    my $msg = pop(@$errors);
    $self->status_forbidden($c,message => "Unauthorized Access, $msg");
    return;
  }
}

=head1 Mover (Agent,Staff)

List 

=cut

sub index :Local :Path(/biz/mover)  :ActionClass('REST') { }

=head2 index /biz/cargo		 GET

Get Mover Infox

=cut

sub index_GET
{
  my $self		= shift;
  my $c			= shift;
  my $in_agentid	= shift;

  my $fn			= "biz/mover_GET";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_mover,$o_mover);

  $o_mover = Biz::Mover->new($dbic,$in_agentid);
  $row_mover = $o_mover->dbrecord
    if(defined($o_mover));

  ##Fill in the Data
  if (defined($row_mover))
  {
    my $h_mover = mover_info($o_mover,$dbic);
    push(@list,$h_mover);
  }

  $c->log->info("$fn Mover:$o_mover, $row_mover");

  ##Success?
  if($o_mover)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    my $msg = "$err_msg Cannot find mover";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}

=head2 index /biz/mover		 PUT

Edit Mover.

=cut

sub index_PUT
{
  my $self		= shift;
  my $c			= shift;
  my $in_agentid	= shift;

  my $fn			= "biz/mover_PUT";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);
  my ($err_msg,$row_mover,$o_mover,$row_mover_updated);
  my $x_reason	= $in_data->{reason} || 'Edit';

  ##Exception: Handle city Data
  arrange_city($c,$in_data);

  $o_mover = Biz::Mover->new($dbic,$in_agentid);
  $row_mover = $o_mover->dbrecord
    if(defined($o_mover));


  ##Edit Mover
  if($x_reason && $o_mover)
  {
    ($err_msg,$row_mover_updated) = $o_mover->edit
      ($in_data,$c_userid,$x_reason);
    $c->log->info("$fn Editing Mover,$err_msg");
  }

  ##Fill in the Data
  if (defined($row_mover_updated))
  {
    my $h_xd = mover_info($o_mover,$dbic);
    push(@list,$h_xd);
  }

  $c->log->info("$fn Mover:$o_mover, $row_mover");

  ##Success?
  if ($row_mover_updated)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  elsif($o_mover && $row_mover)
  {
    my $msg = "$err_msg Cannot edit Mover";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }
  else
  {
    my $msg = "$err_msg Cannot find Mover";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }

}

=head2 index /biz/mover		 POST

Add Mover

in data:{userid,name,src_id,src_idtype,src_country,src_state,src_city}

Fills in the SRC_ID as userid if given.

=cut

sub index_POST
{
  my $self		= shift;
  my $c			= shift;

  my $fn			= "biz/mover";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_mover,$o_mover);

  ##AppUser
  my ($h_new_user,$o_appuser_agent,$new_agentid,$new_agent_name);
  {
    $new_agentid		= $in_data->{userid};
    $new_agent_name		= $in_data->{name};
    my $src_id		        = $in_data->{src_id};

    $h_new_user->{userid}	= $new_agentid;
    $h_new_user->{name}		= $new_agent_name;

    if ($src_id)
    {
      $h_new_user->{userid}	= $src_id;
      $new_agentid		= $src_id;
    }

  }

  ##Exception: Handle city Data
  arrange_city($c,$in_data);

  $o_appuser_agent = Class::Appuser->new($dbic,$new_agentid);

  ##Add IF got the Data
  if ($c_userid && $in_data
      &&
      (($new_agentid && $new_agent_name) || ($o_appuser_agent))
     )
  {
    $c->log->info("$fn Adding Mover:$new_agent_name/$new_agentid");

    $o_appuser_agent = Class::Appuser::create($dbic,$h_new_user)
      if(!$o_appuser_agent);

    ##AppUser
    if ($o_appuser_agent)
    {
      ($err_msg,$row_mover,$o_mover)= Biz::Mover::create
	($dbic,$o_appuser_agent,$in_data);
    }

  }

  ##Fill in the Data
  if (defined($o_mover))
  {
    my $h_xd = mover_info($o_mover,$dbic);
    push(@list,$h_xd);
  }

  $c->log->info("$fn Mover:$o_mover, $row_mover");

  ##Success?
  if($o_mover)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    my $msg = "$err_msg Cannot add mover";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }


}


=head1 PVT Functions

Support Controller.

=head2 mover_info

Get mover JSON hash from here.

=cut

sub mover_info
{
  my $o_mover	=	shift;
  my $dbic	= shift;

  my $fn = "Mover/mover_info";
  my $h_xd;
  ##Fill in the Data

  $h_xd = $o_mover->info();
  my $c_agentid = $h_xd->{userid};
  if ($c_agentid)
  {
    my $o_appuser_agent = Class::Appuser->new($dbic,$c_agentid);
    my $aname		= $o_appuser_agent->aname	if($o_appuser_agent);
    $h_xd->{name}	= $aname if($aname);

    my ($co,$st,$ci,$o_city,$src_cname,$src_ccode);

      $co = $o_mover->src_country;
      $st = $o_mover->src_state;
      $ci = $o_mover->src_city;
      $o_city = Class::City->new($dbic,$co,$st,$ci);
      if ($o_city)
      {
        ($src_ccode,$src_cname) = $o_city->x_name();
        $h_xd->{src_cityname} = $src_cname;
        $h_xd->{src_citycode} = $src_ccode;
      }

    my ($o_address,$addressid);
    $addressid = $o_mover->addressid;
    $o_address = Class::Address->new($dbic,$addressid);
    if ($o_address)
    {
      $h_xd->{streetaddress1} = $o_address->streetaddress1;
      $h_xd->{streetaddress2} = $o_address->streetaddress2;
      $h_xd->{streetaddress3} = $o_address->streetaddress3;
    }

  }

  return $h_xd;

}


=head2 arrange_city

Handles Exception: City,State,Country Input

=cut

sub arrange_city
{
  my $c		= shift;
  my $in_data	= shift;

  my $fn = "Mover/arrange_city";

  ##Handle City for The Mover
  if ($in_data)
  {
    my ($src_co,$src_st,$src_ci);
    $src_co = $in_data->{src_country};
    $src_st = $in_data->{src_state};
    $src_ci = $in_data->{src_city};
    if (!$src_co || !$src_st || !$src_ci)
    {
      my ($co_code,$st_code,$city_code) =
	Class::City::sin_city($in_data,'src_citycode');
      $in_data->{src_country}	= $co_code;
      $in_data->{src_state}	= $st_code;
      $in_data->{src_city}	= $city_code;
      $c->log->info("$fn $co_code/$st_code/$city_code");
    }
  }

  return $in_data;
}

=head1 AUTHOR

Tirveni Yadav,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
