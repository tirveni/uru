package Taipan::Controller::Biz::Report;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

use TryCatch;

use Class::Utils qw(makeparm selected_language unxss unxss_an chomp_date 
		    valid_date get_array_from_argument trim user_login);
use Class::Ztime;


my $o_branch;
my ($c_userid,$c_appid,$c_bizrole,$c_branchid);

=head1 NAME

hakuna::Controller::Diner - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 auto

=cut


=head2 auto

Permission is checked here

=cut

sub auto : Private
{
  my $self		= shift;
  my $c			= shift;

  my $f = "Biz/Report/auto";
  #$c->log->info("$f Begin  $in_branchid");

  my ($h_i,$errors);
  $c_userid = user_login($c);

  my $dbic = $c->model('TDB')->schema;

  my $is_allowed = 1;

  if ($is_allowed > 0)
  {
    $h_i->{admin}	=	1;
  }
  else
  {
    my $msg = pop(@$errors);
    $self->status_forbidden($c,message => "Unauthorized Access, $msg");
    return;
  }
}


=head2 biz/report/trip/:tripid

Print Report for Trip.

=cut

sub trip: Path('trip')
{

  my ( $self, $c,$in_tripid ) = @_;

  $c->stash->{template} = 'src/report/trip.tt';
  $c->stash->{html_pure} = 1;
  $c->stash->{page} = {'title' => "Manifest"};

  my $fx = "biz/report/trip";

  #    $c->response->body('Matched hakuna::Controller::Diner in Diner.');
  my $dbic = $c->model('TDB')->schema;
  my ($o_trip,$h_val,$reportid,$stage);
  try  {
    $o_trip	= Biz::Trip->new($dbic,$in_tripid)
      if ($in_tripid);
    $reportid = $o_trip->reportid;
  };


  ##-- 1. Add
  {
    my $bill_css;
    $bill_css = '';
    $c->stash->{bill_css} = $bill_css;
    #$c->log->info("$f CSS:$bill_css");
  }

  my ($h_trip_info,$h_tr_stage_info,$list_cargo);
  if (defined($o_trip ))
  {
    ($h_trip_info,$h_tr_stage_info,$list_cargo)  =
      $o_trip->get_report($dbic);
    $c->log->info("$fx $h_trip_info,$h_tr_stage_info,$list_cargo");
  }

  my ($report_gen_time,$today_now) = _manage_rtime($h_trip_info);
  $h_trip_info->{report_date} = $report_gen_time;


  ##
  if ($h_trip_info && $list_cargo)
  {
    $c->log->info("$fx RID:$reportid");


    $c->stash->{company_name}	=Class::Utils::config(qw/Company name/);
    $c->stash->{company_gst}	=Class::Utils::config(qw/Company GST/);

    $c->stash->{reportid}	= $reportid;#This report
    $c->stash->{trip}		= $h_trip_info;
    $c->stash->{trip_stage}	= $h_tr_stage_info;
    $c->stash->{cargoes}	= $list_cargo;
    $c->stash->{print_date}	= $today_now;
    $c->stash->{page} = {'title' => "Challan: $reportid"};
  }
  else
  {
    $c->stash->{error} = "No Report for this Trip.";
  }


}


=head2 biz/report/cargo/:tripid/:cargoid

Print Report for Trip.

=cut

sub cargo: Path('cargo')
{

  my ( $self, $c,$in_tripid,$in_cargoid ) = @_;

  $c->stash->{template} = 'src/report/cargo.tt';
  $c->stash->{html_pure} = 1;
  $c->stash->{page} = {'title' => "Report"};

  my $fx = "biz/report/trip";

  #    $c->response->body('Matched hakuna::Controller::Diner in Diner.');
  my $dbic = $c->model('TDB')->schema;
  my ($o_trip,$h_val,$reportid,$stage,$o_cargo,$c_cargoid,
     $today_now_utc);
  try
  {
    $o_trip	= 	Biz::Trip->new($dbic,$in_tripid)
      if ($in_tripid);
    $reportid = $o_trip->reportid;
    $o_cargo	=	Biz::Cargo->new($dbic,$in_cargoid);	
    $c_cargoid	=	$o_cargo->cargoid;
    $today_now_utc = Class::Utils::today_now_utc;
  };

  if (!$o_cargo)
  {
    $c->stash->{error} = "No Report for this Trip.";
    return;
  }

  ##-- 1. Add
  {
    my $bill_css;
    $bill_css = '';
    $c->stash->{bill_css} = $bill_css;
    #$c->log->info("$f CSS:$bill_css");
  }

  my ($h_trip_info,$h_tr_stage_info,$list_cargo,$list_single_cargo);
  if (defined($o_trip ))
  {
    ($h_trip_info,$h_tr_stage_info,$list_cargo)  =
      $o_trip->get_report($dbic,$o_cargo);
    $c->log->info("$fx $h_trip_info,$h_tr_stage_info,$list_cargo");
  }

  my ($report_gen_time,$today_now) = _manage_rtime($h_trip_info);
  $h_trip_info->{report_date} = $report_gen_time;

  ##
  if ($h_trip_info && $list_cargo)
  {
    $c->log->info("$fx RID:$reportid");
    $c->stash->{reportid}	= $reportid;#This report
    $c->stash->{trip}		= $h_trip_info;
    $c->stash->{trip_stage}	= $h_tr_stage_info;
    $c->stash->{cargoes}	= $list_cargo;
    $c->stash->{print_date}	= $today_now;
    $c->stash->{page} = {'title' => "GR: $reportid-$c_cargoid"};
  }
  else
  {
    $c->stash->{error} = "No Report for this Trip.";
  }


}


=head2 _manage_rtime($h_trip_info)

Returns: $report_gen_dt_time_tz,$print_dt_time_tz)

Converts into Company TZ.

=cut

sub _manage_rtime
{
  my $h_trip_info = shift;

  my ($today_now_utc,$biz_tz,
      $print_dt_time,$report_gen_dt_time,$report_gen_dt_utc);
  $today_now_utc	= Class::Utils::today_now_utc;
  $biz_tz 		= Class::Utils::config(qw/Company TZ/);
  $report_gen_dt_utc	= $h_trip_info->{report_date};

  try {

    if ($biz_tz)
    {
      my $outc_tz;
      ($print_dt_time,$outc_tz) =
	Class::Ztime::dt_tz_convert($biz_tz,$today_now_utc);

      ($report_gen_dt_time,$outc_tz) =
	Class::Ztime::dt_tz_convert($biz_tz,$report_gen_dt_utc);
    }

  };

  if(!$report_gen_dt_time)
  {
    $report_gen_dt_time = $report_gen_dt_utc;
  }

  if(!$print_dt_time)
  {
    $print_dt_time = $today_now_utc;
  }

  return ($report_gen_dt_time,$print_dt_time);

}



=head1 AUTHOR

Tirveni Yadav,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
