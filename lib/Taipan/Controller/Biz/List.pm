package Taipan::Controller::Biz::List;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

use TryCatch;

use Class::Utils
  qw(makeparm selected_language unxss unxss_an xnatural xfloat valid_email
     chomp_date valid_date get_array_from_argument trim user_login);

use Class::Address;
use Class::Currency;
use Class::General;

use Biz::Trip;

my ($rows_per_page,$c_userid,$h_rest);
{
  $rows_per_page = 10;
}


=head1 NAME

Taipan::Controller::Biz::Branch - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 Listing for the business

List stuff for the business

=cut


=head2 auto

Permission is checked here

=cut

sub auto : Private
{
  my $self		= shift;
  my $c			= shift;

  my $f = "Biz/List/auto";
  #$c->log->info("$f Begin  $in_branchid");

  my ($h_i,$errors);
  $c_userid = user_login($c);

  my $dbic = $c->model('TDB')->schema;

  my $is_allowed = 1;

  if ($is_allowed > 0)
  {
    $h_i->{admin}	=	1;
  }
  else
  {
    my $msg = pop(@$errors);
    $self->status_forbidden($c,message => "Unauthorized Access, $msg");
    return;
  }
}

=head1 Cargo

List cargo.

=head2 cargo /biz/list/cargo/:page

List Taxes

=cut

sub cargo  :Path('/biz/list/cargo') :Args(1)
{
  my $self		= shift;
  my $c			= shift;
  my $startpage		= shift || 1 ;
  my $desired_page	= 0 ;

  my $fn			= "Biz/List/cargo";

  my $dbic = $c->model('TDB')->schema;

  my $rs_cargo = $dbic->resultset('Cargo');

  my $h_search = search_cargo($c);

  if (defined($rs_cargo) && $h_search)
  {
    $rs_cargo = $rs_cargo->search($h_search);
  }

  ##Pgx
  {
    my $order_by = { -desc => [qw/cargoid/] };
    my $attribs ;

    my $page_attribs =
    {
     desiredpage  => $desired_page,
     startpage    => $startpage,
     inputsearch  => $attribs,
     rowsperpage  => $rows_per_page,
     order	  => $order_by,
     listname     => 'Cargo',
     namefn       => "cargo/$startpage",
     nameclass    => 'biz/list',
    };

    $rs_cargo = Class::General::paginationx($c,$page_attribs,$rs_cargo)
      if (defined($rs_cargo));
    $startpage = $c->stash->{listpage}->{page};
  }


  my @list;

  my ($val_count,$val_first,$val_last);
  $val_count = 0;

  while (my $row = $rs_cargo->next)
  {
    my %x_data = $row->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    my $date_time;
    $date_time = $h_xd->{created_at};
    $date_time = substr($date_time,0,16);
    $h_xd->{created_at} = $date_time;

    push(@list,$h_xd);


    my $val_item = $h_xd->{cargoid};
    $val_count++;
    $val_last = $val_item;
    if ($val_count == 1)
    {
      $val_first = $val_item;
    }

  }

  my $h_rest =
    {
     items      => \@list,
     idx_first  => $val_first,
     idx_last   => $val_last,
     page_number	=> $startpage,
    };


  if(@list)
  {
    $c->log->info("Found Data");
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  else
  {
    $c->log->info("No Data");
    $self->status_not_found($c,message => "Cannot Find any Cargo");
  }


}


=head2 users /biz/list/movers/:page

List Taxes

=cut

sub movers  :Path('/biz/list/movers') :Args(1)
{
  my $self		= shift;
  my $c			= shift;
  my $startpage		= shift || 1 ;
  my $desired_page	= 0 ;

  my $fn			= "Biz/List/users";

  # Input Types
  my $pars         = makeparm(@_);
  my $aparams      = $c->request->params;
  my $dbic = $c->model('TDB')->schema;

  my ($rs_appusers,$t_users,$in_name);

  $t_users = $dbic->resultset('Appuser');
  $in_name = $aparams->{name};

  my $list_included_roles = 
    [qw/MANAGER STAFF AGENT DRIVER DISABLED GUEST CLIENT/];

  ##First Search
  {
    $rs_appusers = $t_users->search({role=>$list_included_roles});
  }
  if ($in_name && defined($rs_appusers))
  {
    $rs_appusers = $rs_appusers->search
      (
       {
	name => {'ilike' => "%$in_name%",},
       }
      );
  }

  $c->log->info("$fn StartPG:$startpage,DPage:$desired_page");
  ##Pagination
  my @list;
  {
    my $attribs ;
    my $order_by = { -desc => [qw/userid/] };
    my $page_attribs =
    {
     desiredpage  => $desired_page,
     startpage    => $startpage,
     inputsearch  => $attribs,
     rowsperpage  => $rows_per_page,
     order	  => $order_by,
     listname     => 'Users',
     namefn       => "users/$startpage",
     nameclass    => 'biz/list',
    };

    $rs_appusers =
      Class::General::paginationx($c,$page_attribs,$rs_appusers)
	  if (defined($rs_appusers));
    $startpage = $c->stash->{listpage}->{page};
  }

  my ($val_count,$val_first,$val_last);
  $val_count = 0;

  while (my $row = $rs_appusers->next)
  {
    my %x_data = $row->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);


    my $val_item = $h_xd->{userid};
    $val_count++;
    $val_last = $val_item;
    if ($val_count == 1)
    {
      $val_first = $val_item;
    }

  }

  my $h_rest =
    {
     items      => \@list,
     idx_first  => $val_first,
     idx_last   => $val_last,
     page_number	=> $startpage,
    };

  $c->log->info("$fn Data @list");
  if(@list)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    $c->log->info("No Data");
    #$self->status_not_found($c,message => "Cannot Find any Agents");
    $self->status_no_content($c);
  }

}

=head1 Trips

List Trips.

=head2 trips /biz/list/trips/:page

List Trips

=cut

sub trips  :Path('/biz/list/trips') :Args(1)
{
  my $self		= shift;
  my $c			= shift;
  my $startpage		= shift || 1 ;
  my $desired_page	= 0 ;

  my $fn			= "Biz/List/cargo";

  my $dbic = $c->model('TDB')->schema;

  my ($rs_trip,$h_search);
  $rs_trip = $dbic->resultset('Trip');
  #$h_search = search_trip($c);

  if (defined($rs_trip) && $h_search)
  {
    $rs_trip = $rs_trip->search($h_search);
  }

  ##Pgx
  {
    my $order_by = { -desc => [qw/tripid/] };
    my $attribs ;

    my $page_attribs =
    {
     desiredpage  => $desired_page,
     startpage    => $startpage,
     inputsearch  => $attribs,
     rowsperpage  => $rows_per_page,
     order	  => $order_by,
     listname     => 'Trip',
     namefn       => "trip/$startpage",
     nameclass    => 'biz/list',
    };

    $rs_trip = Class::General::paginationx($c,$page_attribs,$rs_trip)
      if (defined($rs_trip));
    $startpage = $c->stash->{listpage}->{page};
  }


  my @list;

  my ($val_count,$val_first,$val_last);
  $val_count = 0;

  while (my $row = $rs_trip->next)
  {
    my %x_data = $row->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);


    my $val_item = $h_xd->{tripid};
    $val_count++;
    $val_last = $val_item;
    if ($val_count == 1)
    {
      $val_first = $val_item;
    }

  }

  my $h_rest =
    {
     items      => \@list,
     idx_first  => $val_first,
     idx_last   => $val_last,
     page_number	=> $startpage,
    };


  if(@list)
  {
    $c->log->info("Found Data");
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  else
  {
    $c->log->info("No Data");
    $self->status_not_found($c,message => "Cannot Find any Trip");
  }


}

=head1 Vehicles

List vehicles.

=head2 vehicles /biz/list/vehicles/:page

List Vehicles

=cut

sub vehicles  :Path('/biz/list/vehicles') :Args(1)
{
  my $self		= shift;
  my $c			= shift;
  my $startpage		= shift || 1 ;
  my $desired_page	= 0 ;

  my $fn			= "Biz/List/cargo";

  my $dbic = $c->model('TDB')->schema;

  my ($rs_vehicle,$h_search);
  $rs_vehicle = $dbic->resultset('Vehicle');
  #$h_search = search_vehicle($c);

  if (defined($rs_vehicle) && $h_search)
  {
    $rs_vehicle = $rs_vehicle->search($h_search);
  }

  ##Pgx
  {
    my $order_by = { -desc => [qw/vehicleid/] };
    my $attribs ;

    my $page_attribs =
    {
     desiredpage  => $desired_page,
     startpage    => $startpage,
     inputsearch  => $attribs,
     rowsperpage  => $rows_per_page,
     order	  => $order_by,
     listname     => 'Vehicle',
     namefn       => "vehicle/$startpage",
     nameclass    => 'biz/list',
    };

    $rs_vehicle = Class::General::paginationx
      ($c,$page_attribs,$rs_vehicle)     if (defined($rs_vehicle));
    $startpage = $c->stash->{listpage}->{page};
  }


  my @list;

  my ($val_count,$val_first,$val_last);
  $val_count = 0;

  while (my $row = $rs_vehicle->next)
  {
    my %x_data = $row->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);


    my $val_item = $h_xd->{vehicleid};
    $val_count++;
    $val_last = $val_item;
    if ($val_count == 1)
    {
      $val_first = $val_item;
    }

  }

  my $h_rest =
    {
     items      => \@list,
     idx_first  => $val_first,
     idx_last   => $val_last,
     page_number	=> $startpage,
    };


  if(@list)
  {
    $c->log->info("Found Data");
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  else
  {
    $c->log->info("No Data");
    $self->status_not_found($c,message => "Cannot Find any Vehicle");
  }


}

=head1 TripReports

List reports.

=head2 Tripreports /biz/list/tripreports/:page

List Reports

=cut

sub tripreports  :Path('/biz/list/tripreports') :Args(1)
{
  my $self		= shift;
  my $c			= shift;
  my $startpage		= shift || 1 ;
  my $desired_page	= 0 ;

  my $fn			= "Biz/List/tripReports";

  my $dbic = $c->model('TDB')->schema;

  my ($rs_report,$h_search);
  $rs_report = $dbic->resultset('Reporttrip');
  #$h_search = search_report($c);

  if (defined($rs_report) && $h_search)
  {
    $rs_report = $rs_report->search($h_search);
  }

  ##Pgx
  {
    my $order_by = { -desc => [qw/reportid/] };
    my $attribs ;

    my $page_attribs =
    {
     desiredpage  => $desired_page,
     startpage    => $startpage,
     inputsearch  => $attribs,
     rowsperpage  => $rows_per_page,
     order	  => $order_by,
     listname     => 'Report',
     namefn       => "report/$startpage",
     nameclass    => 'biz/list',
    };

    $rs_report = Class::General::paginationx
      ($c,$page_attribs,$rs_report)     if (defined($rs_report));
    $startpage = $c->stash->{listpage}->{page};
  }


  my @list;

  my ($val_count,$val_first,$val_last);
  $val_count = 0;

  while (my $row = $rs_report->next)
  {
    my %x_data = $row->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);


    my $val_item = $h_xd->{reportid};
    $val_count++;
    $val_last = $val_item;
    if ($val_count == 1)
    {
      $val_first = $val_item;
    }

  }

  my $h_rest =
    {
     items      => \@list,
     idx_first  => $val_first,
     idx_last   => $val_last,
     page_number	=> $startpage,
    };


  if(@list)
  {
    $c->log->info("Found Data");
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  else
  {
    $c->log->info("No Data");
    $self->status_not_found($c,message => "Cannot Find any Report");
  }


}




=head1 PVT Functions

Supporters

=head2 search_cargo($c)

Fields:$src_customerid,$src_cityx,$src_id,$src_idtype,
$is_pay_by_src,$dst_customerid,$dst_cityx,$dst_id,
$dst_idtype,$is_cartage_paid

This is AND Search.

Returns: The a HashRef ready of Search in Table Cargo.

=cut

sub search_cargo
{
  my $c = shift;

  my $fn = "biz/search_cargo";
  my $h_search;
  my ($src_customerid,$src_cityx,$src_id,$src_idtype,$is_payby_src);
  my ($dst_customerid,$dst_cityx,$dst_id,$dst_idtype,$is_cartage_paid);
  my ($src_co,$src_st,$src_city);
  my ($dst_co,$dst_st,$dst_city);
  my ($from_date,$till_date,$is_booked_only,$transporter_invoiceid,
     $cargoid);


  ##Booked Only
  {
    $is_booked_only =Class::General::input_header($c,'booked_only');
  }

  ##
  {
    $from_date		= Class::General::input_header($c,'from_date');
    $from_date		= Class::Utils::valid_date($from_date);

    $till_date		= Class::General::input_header($c,'till_date');
    $till_date		= Class::Utils::valid_date($till_date);

    $transporter_invoiceid
      = Class::General::input_header($c,'transporter_invoiceid');
    $c->log->info("$fn GR: $transporter_invoiceid");

    $cargoid	= Class::General::input_header($c,'cargoid');
    $c->log->info("$fn CID: $cargoid");

  }

  ##Boolean Stuff
  {
    $is_payby_src	= Class::General::input_header($c,'is_payby_src');
    $is_payby_src	= Class::Utils::valid_boolean($is_payby_src);
    $is_cartage_paid	= 
      Class::General::input_header($c,'is_cartage_paid');
    $is_cartage_paid	= Class::Utils::valid_boolean($is_cartage_paid);
  }

  ##SRC
  {
    $src_cityx 	= Class::General::input_header($c,'src_citycode');
    $src_id 	= Class::General::input_header($c,'src_id');
    $src_idtype	= Class::General::input_header($c,'src_idtype');
    $src_customerid	= 
      Class::General::input_header($c,'src_customerid');

    $c->log->info("$fn Search on: $src_customerid,$src_cityx,".
		  "$src_id,$src_idtype,$is_payby_src,$is_cartage_paid");
  }

  ##DST
  {
    $dst_cityx 	= Class::General::input_header($c,'dst_citycode');
    $dst_id 	= Class::General::input_header($c,'dst_id');
    $dst_idtype	= Class::General::input_header($c,'dst_idtype');
    $dst_customerid	= 
      Class::General::input_header($c,'dst_customerid');
    $c->log->info("$fn Search on: $dst_customerid,$dst_cityx".
		  ",$dst_id,$dst_idtype");
  }

  {
    $h_search->{is_payby_src}	= $is_payby_src      if($is_payby_src);
    $h_search->{is_cartage_paid}= $is_cartage_paid   if($is_cartage_paid);

    ##Src
    $h_search->{src_id}		= $src_id
      if($src_id);
    $h_search->{src_idtype}	= $src_idtype
      if($src_idtype);
    $h_search->{src_customerid} = $src_customerid
      if($src_customerid);
    ($src_co,$src_st,$src_city)	= split(/:/,$src_cityx)
      if($src_cityx);
    $h_search->{src_city}	= $src_city
      if($src_city);
    $h_search->{src_state}	= $src_st
      if($src_st);
    $h_search->{src_country}	= $src_co
      if($src_co);

    ##DST
    $h_search->{dst_id}		= $dst_id
      if($dst_id);
    $h_search->{dst_idtype}	= $dst_idtype
      if($dst_idtype);
    $h_search->{dst_customerid} = $dst_customerid
      if($dst_customerid);
    ($dst_co,$dst_st,$dst_city)	= split(/:/,$dst_cityx)
      if($dst_cityx);
    $h_search->{dst_city}	= $dst_city
      if($dst_city);
    $h_search->{dst_state}	= $dst_st
      if($dst_st);
    $h_search->{dst_country}	= $dst_co
      if($dst_co);

    $h_search->{transporter_invoiceid}	= $transporter_invoiceid
      if($transporter_invoiceid);

    $h_search->{created_at}	= {'>=',$from_date}
      if($from_date);
    $h_search->{created_at}	= {'<=',$till_date}
      if($till_date);

    $h_search->{stageid} = 'BOOKED'
      if($is_booked_only);
  }

  return $h_search;

}


=head1 AUTHOR

Tirveni Yadav,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
