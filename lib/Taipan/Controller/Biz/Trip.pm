package Taipan::Controller::Biz::Trip;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

use JSON qw(decode_json);
use TryCatch;

use Class::Utils
  qw(makeparm selected_language unxss unxss_an xnatural xfloat valid_email
     chomp_date valid_date get_array_from_argument trim user_login);

use Class::Address;
use Class::Currency;
use Class::General;

my ($rows_per_page,$c_userid,$h_rest);
{
  $rows_per_page = 10;
}


=head1 NAME

Biz::Trip - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 Listing for the business

List stuff for the business

=cut


=head2 auto

Permission is checked here

=cut

sub auto : Private
{
  my $self		= shift;
  my $c			= shift;

  my $f = "Biz/Trip/auto";
  #$c->log->info("$f Begin  $in_branchid");

  my ($h_i,$errors);
  $c_userid = user_login($c);

  my $dbic = $c->model('TDB')->schema;

  my $is_allowed = 1;

  if ($is_allowed > 0)
  {
    $h_i->{admin}	=	1;
  }
  else
  {
    my $msg = pop(@$errors);
    $self->status_forbidden($c,message => "Unauthorized Access, $msg");
    return;
  }
}

=head1 Trip

List cargo.

=cut

sub index :Local :Path(/biz/trip)  :ActionClass('REST') { }

=head2 index /biz/cargo		 GET

Get Trip Infox

=cut

sub index_GET
{
  my $self		= shift;
  my $c			= shift;
  my $in_cargoid	= shift;

  my $fn			= "Biz/Trip_GET";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_trip,$o_trip);

  $o_trip = Biz::Trip->new($dbic,$in_cargoid);
  $row_trip = $o_trip->dbrecord
    if(defined($o_trip));

  ##Fill in the Data
  if (defined($row_trip))
  {
    my $h_cargo = trip_info($dbic,$o_trip,$row_trip);
    push(@list,$h_cargo);
  }

  $c->log->info("$fn Trip:$o_trip, $row_trip");

  ##Success?
  if($o_trip)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    my $msg = "$err_msg Cannot find Trip";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}

=head2 index /Biz/Trip		 PUT

Edit Trip.

InputData:{start_date,end_date,reportid,src_country,src_state,src_city,
dst_Country,dst_state,dst_city,vehicle_cost,vehicleid,
driverid,trip_transportedid,is_trip_complete,finished_on,valid
}

Special Options: {create_report,unblock_force,reason}

create_report:	 creates report and blocks the Cargo Add/Remove.

unblock_force:	 Unblock the Cargo Add/Remove. Reason is required.



=cut

sub index_PUT
{
  my $self		= shift;
  my $c			= shift;
  my $in_tripid	= shift;

  my $fn			= "Biz/Trip_PUT";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);
  my ($err_msg,$row_trip,$o_trip,$row_trip_updated);
  my $x_reason	= $in_data->{reason};

  ##Exception: Handle city Data
  Taipan::Controller::Biz::Cargo::arrange_city($c,$in_data);

  $o_trip = Biz::Trip->new($dbic,$in_tripid);
  $row_trip = $o_trip->dbrecord
    if(defined($o_trip));

  ##Report Input
  my ($is_new_report,$is_unblock);
  {
    $is_new_report	= $in_data->{create_report};
    $is_unblock		= $in_data->{unblock_force};
  }

  ##Edit Trip

  if ($o_trip && $is_new_report)
  {
    $c->log->info("$fn Create Report ");
    my ($row_report,$reportid,$is_blocked);

    $row_report		= $o_trip->make_report($dbic,$c_userid);
    $c->log->info("$fn Report:$row_report ");

    $reportid		= $row_report->reportid
      if(defined($row_report));
    $c->log->info("$fn ReportID:$reportid.Now Block ");

    $is_blocked		= $o_trip->cargo_block($reportid)
      if($reportid);
    $row_trip_updated	= $o_trip->dbrecord
      if($reportid && $row_report);

    $c->log->info("$fn is_Blocked:$is_blocked ");
  }
  elsif ($o_trip && $is_unblock && $x_reason)
  {
    my ($is_blocked);
    $c->log->info("$fn UnBlock this Trip:$in_tripid ");
    $is_blocked			= $o_trip->cargo_unblock($x_reason);
    $row_trip_updated		= $o_trip->dbrecord;
    $c->log->info("$fn is_Blocked:$is_blocked ");
  }
  elsif($o_trip && $in_data)
  {
    $x_reason = "EDIT";
    ($err_msg,$row_trip_updated) =
      $o_trip->edit($in_data,$c_userid,$x_reason);
    $c->log->info("$fn Editing Trip,$err_msg");
  }

  ##Fill in the Data
  if (defined($row_trip_updated))
  {
    my %x_data	= $row_trip->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);
  }

  $c->log->info("$fn Trip:$o_trip, $row_trip");

  ##Success?
  if ($row_trip_updated)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  elsif($o_trip && $row_trip)
  {
    my $msg = "$err_msg Cannot edit trip";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }
  else
  {
    my $msg = "$err_msg Cannot find trip";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }

}


=head2 index /Biz/Trip		 DELETE

DELETE Trip. This Disables DELETE

=cut

sub index_DELETE
{
  my $self		= shift;
  my $c			= shift;
  my $in_cargoid	= shift;

  my $fn			= "Biz/Trip_DELETE";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);
  my ($err_msg,$row_trip,$o_trip,$row_trip_updated);
  my $x_reason	= $in_data->{reason};

  $o_trip = Biz::Trip->new($dbic,$in_cargoid);
  $row_trip = $o_trip->dbrecord
    if(defined($o_trip));

  ##Change Stage
  if ($x_reason)
  {
    ($err_msg,$row_trip_updated) =
      $o_trip->disable($in_data,$c_userid,$x_reason);
  }


  ##Fill in the Data
  if (defined($row_trip_updated))
  {
    my $h_cargo = trip_info($dbic,$o_trip,$row_trip);
    push(@list,$h_cargo);
  }

  $c->log->info("$fn Trip:$o_trip, $row_trip");

  ##Success?
  ##Success?
  if ($row_trip_updated)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );
  }
  elsif($o_trip && $row_trip)
  {
    my $msg = "$err_msg Cannot delete trip";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }
  else
  {
    my $msg = "$err_msg Cannot find trip";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}



=head2 index /Biz/Trip		 POST

Add Trip

=cut

sub index_POST
{
  my $self		= shift;
  my $c			= shift;

  my $fn			= "Biz/Trip";

  my $dbic = $c->model('TDB')->schema;

  my @list;

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_trip,$o_trip);

  ##Exception: Handle city Data: src,dst
  Taipan::Controller::Biz::Cargo::arrange_city($c,$in_data);

  ##Add IF got the Data
  if ($c_userid && $in_data)
  {
    ($err_msg,$row_trip,$o_trip)= Biz::Trip::create
      ($dbic,$in_data,$c_userid);
  }

  ##Fill in the Data
  if (defined($row_trip))
  {
    my %x_data = $row_trip->get_columns;
    my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
    push(@list,$h_xd);
  }

  $c->log->info("$fn Trip:$o_trip, $row_trip");

  ##Success?
  if($o_trip)
  {
    $h_rest->{items} = \@list;
    $self->status_ok( $c, entity => $h_rest );

  }
  else
  {
    my $msg = "$err_msg Cannot add Trip";
    $c->log->info("$fn $msg");
    $self->status_bad_request($c,message => $msg);
  }


}

=head1 Trip

List cargo.

=cut

sub cargo :Local :Path(/biz/trip/cargo)  :ActionClass('REST') { }

=head2 index /biz/trip/cargo		 GET

Get Trip's Cargo

=cut

sub cargo_GET
{
  my $self		= shift;
  my $c			= shift;
  my $in_tripid	= shift;

  my $fn			= "Biz/Trip/Cargo_GET";

  my $dbic = $c->model('TDB')->schema;

  my @list_trip;
  my @list_cargo;
  my ($rs_vtcargo,$list_cargoids);

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my ($err_msg,$row_trip,$o_trip);

  $o_trip = Biz::Trip->new($dbic,$in_tripid);
  $row_trip = $o_trip->dbrecord
    if(defined($o_trip));

  ##Fill in the Data
  if (defined($row_trip))
  {
    my $h_cargo = trip_info($dbic,$o_trip,$row_trip);
    push(@list_trip,$h_cargo);
  }

  ##Cargo for the Trip.
  {
    $rs_vtcargo = $o_trip->list_cargo($dbic,1);
    while (my $row = $rs_vtcargo->next)
    {
      my %x_data = $row->get_columns;
      my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
      push(@list_cargo,$h_xd);
    }
  }

  $c->log->info("$fn Trip:$o_trip, $row_trip");

  ##Success?
  if($o_trip)
  {
    $h_rest->{tripid}	= $in_tripid;
    $h_rest->{trip}	= \@list_trip;
    $h_rest->{cargo}	= \@list_cargo;
    $self->status_ok( $c, entity => $h_rest );
  }
  else
  {
    my $msg = "$err_msg Cannot find Trip";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}

=head2 index /biz/trip/cargo		 POST

Get Trip's Cargo

=cut

sub cargo_POST
{
  my $self		= shift;
  my $c			= shift;
  my $in_tripid	= shift;

  my $fn			= "Biz/Trip/Cargo_POST";

  my $dbic = $c->model('TDB')->schema;

  my @list_trip;
  my @list_cargo;
  my ($rs_vtcargo,$list_cargoids);

  my $in_data;
  $in_data = Class::General::get_json_hash($c);

  my $aparams   = $c->request->params;
  my ($err_msg,$row_trip,$o_trip);

  $o_trip = Biz::Trip->new($dbic,$in_tripid);
  $row_trip = $o_trip->dbrecord
    if(defined($o_trip));

  ##Fill in the Data
  if (defined($row_trip))
  {
    my $h_cargo = trip_info($dbic,$o_trip,$row_trip);
    push(@list_trip,$h_cargo);
  }

  ##Get the Input Cargo
  my ($list_new_cargoids,$list_remove_cargoids);
  {
    $list_remove_cargoids = 
      decode_json($aparams->{remove_cargoids});
    $list_new_cargoids = 
      decode_json($aparams->{new_cargoids});
  }

  $c->log->info("$fn $list_new_cargoids");
  $c->log->info("$fn $list_remove_cargoids");

  ##Mange Cargo
  {
    my $h_mx;
    $h_mx->{userid}			= $c_userid;
    $h_mx->{list_new_cargoids}		= $list_new_cargoids;
    $h_mx->{list_remove_cargoids}	= $list_remove_cargoids;
    $rs_vtcargo = $o_trip->manage_cargo($dbic,$h_mx);
  }

  ##--Finally,List Cargo for the Trip.
  if (defined($rs_vtcargo))
  {
    while (my $row = $rs_vtcargo->next)
    {
      my %x_data = $row->get_columns;
      my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);
      push(@list_cargo,$h_xd);
    }
  }

  $c->log->info("$fn Trip:$o_trip, $row_trip");

  ##Success?
  if($o_trip)
  {
    $h_rest->{tripid}	= $in_tripid;
    $h_rest->{trip}	= \@list_trip;
    $h_rest->{cargo}	= \@list_cargo;
    $self->status_ok( $c, entity => $h_rest );
  }
  else
  {
    my $msg = "$err_msg Cannot find Trip";
    $c->log->info("$fn $msg");
    $self->status_not_found($c,message => $msg);
  }


}


=head1 PVT Functions

=head2 trip_info

Get cargo JSON hash from here.

=cut

sub trip_info
{
  my $dbic	=	shift;
  my $o_trip	=	shift;
  my $row_trip	=	shift;

  my $fn = "Trip/trip_info";
  my $h_xd = $o_trip->full_info($dbic,$row_trip);

  return $h_xd;
}


=head1 AUTHOR

Tirveni Yadav,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
