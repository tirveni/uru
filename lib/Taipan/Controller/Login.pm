package Taipan::Controller::Login;
use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

use Captcha::reCAPTCHA;
use Class::Utils qw(unxss trim config valid_email);
use Class::Appuser;


use Class::General;
use TryCatch;


=head1 NAME

Taipan::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

Action to login an user to the application.

=cut

sub index :Path :Args(0)
{
  my ( $self, $c ) = @_;

# Input Types
  my $aparams = $c->request->params;
  my $m = "Login/index";

# Page and Template
  $c->stash->{template} = 'src/user/login.tt';

  # Get the username and password from form
  my $app_userid	= trim($aparams->{userid});
  my $password		= trim($aparams->{password});
  $c->log->debug("$m: $app_userid:$password");

  my $o_ltry;
  my ($ip,$user_agent,$h_attempt);
  {
    $ip		= $c->req->address;
    $user_agent = $c->request->user_agent();
    $h_attempt->{user_agent}	= $user_agent;
    $h_attempt->{ip}		= $ip	;
    $h_attempt->{userid}	= $app_userid ;
    $h_attempt->{login_success} = 'f';
    $h_attempt->{url}		= "login";
  }

  ##Manage Captch/ReCaptcha
  my ($is_valid_captcha,$is_use_recaptcha,$is_use_captcha,$is_show_captcha);
  {
    $is_use_recaptcha  = config(qw/Recaptcha in_use/);
    $is_use_captcha    = config(qw/Captcha in_use/);

    if (!$is_use_captcha && !$is_use_recaptcha )
    {	
      $is_valid_captcha = 1;
    }
    elsif ($is_use_captcha)
    {
      $is_valid_captcha = _pvt_captcha($c);
      $is_show_captcha = 1;
    }
    elsif ($is_use_recaptcha)
    {
      $is_valid_captcha = _google_recaptcha($c);
      $is_show_captcha = 1;
    }

    $c->log->info("$m Show Captcha:$is_show_captcha");
    $c->stash->{'is_show_captcha'} = $is_show_captcha;
  }


#End Recaptcha, google
  my $dbic		= $c->model('TDB')->schema;
  my $rs_appuser	= $dbic->resultset('Appuser');
  my $user_validated;
  my $row_appuser;

  # If the username and password values were found in form
  if ($app_userid && $password && $is_valid_captcha)
  {

    $row_appuser = $rs_appuser->find({ userid => $app_userid });
    my $user_validated;
    $c->log->debug("$m: User Obj: $row_appuser");

    $user_validated = $row_appuser->get_column('active')
      if($row_appuser);
    $c->log->debug("$m Validated:$user_validated ");

    if ($row_appuser && ($user_validated eq 't' || $user_validated > 0) )
    {

      my $encoded_password = 
	Class::Appuser::encode_password($password);
      $c->log->debug("$m: Encoded PW: $encoded_password");

      my $h_user_au = 
      {
       userid	=> $app_userid,
       #password	=> $password, 
       #Simple Un-Encrypted Password
       password	=> $encoded_password,
      };

      $c->log->debug("$m: Going for authentication.");
      if($c->authenticate($h_user_au, 'simpledb'))
      {
	#      $c->set_authenticated($row_appuser); 
	$c->log->info("L/index: We are through");

	##For increased security change the session after Login
	$c->change_session_id;

	##Change the Session Period, Does not work
	$c->change_session_expires( 4000 );

	#my $biscuit = Class::Appuser::set_auth_keys
	#  ($dbic,$app_userid);##Testing Purpose only.
	try
	{
	  $o_ltry = Class::Logintry::login_valid($dbic,$h_attempt);
	}

	##
	$c->response->redirect('/home') ;

	return;
      }

    }##IF Appuser

    my $user_is_logged_in = $c->user_exists;
    if (!$user_is_logged_in)
    {
      my $err_msg;
      if ($row_appuser &&
	     ($user_validated < 1 || $user_validated eq 'f')
	    )
      {
	$err_msg  = "This Email is not verified.";
      }
      else
      {
	$err_msg = "Wrong username or password.";
      }


      $c->stash(error_msg => $err_msg );

      try
      {
	$o_ltry = Class::Logintry::user_pass_invalid($dbic,$h_attempt);
      }

    }
    #my $loggedin_userid = $c->user->get('userid');
  }
  else
  {
    # Set an error message
    #$c->stash(error_msg => "Captcha is Invalid.");
    $c->stash(error_msg => "");
  }

}

=head2 login/captcha

Creates a Captcha Image.

Use:
	<img src="[% c.uri_for('/login/captcha') %]" alt="Captcha"
              				 />
        <input type="text" name='captcha_in' required />

=cut

sub captcha :Path('/login/captcha') :Args(0)
{
  my ( $self, $c ) = @_;

  # Input Types
  my $aparams = $c->request->params;
  my $m = "Login/index";

  $c->create_captcha();

}

=head1 Pvt Functions

=head2 _google_recaptcha

=cut

sub _google_recaptcha
{
  my $c = shift;

  ## Input Types
  my $aparams	= $c->request->params;
  my $m		= "Login/_google_recaptcha";

    #Realperson Code, Captcha
  my ($cap_response_field,$o_captcha,$is_valid_catpcha,
      $pvt_key_captcha,$pub_key_captcha,$cap_challenge_field);
  {
    $pvt_key_captcha  = config(qw/Recaptcha private_key/);
    $pub_key_captcha  = config(qw/Recaptcha public_key/);
    $c->log->debug("$m: C Pub Key: $pub_key_captcha");
    $o_captcha = Captcha::reCAPTCHA->new;

    $cap_challenge_field =$aparams->{recaptcha_challenge_field};
    $cap_response_field  =$aparams->{recaptcha_response_field};

  }

  if ($cap_response_field && $cap_response_field)
  {

    my $ip = $c->req->address;
    $c->log->debug("$m: IP:$ip");

    my $cap_result = $o_captcha->check_answer
      ($pvt_key_captcha,$ip,
       $cap_challenge_field,$cap_response_field
      );


    if ( $cap_result->{is_valid} )
    {
      $c->log->debug("$m: CAPTCHA(Valid) $ip ");
      $is_valid_catpcha=1;
    }
    else
    {
      # Error
      my $error = $cap_result->{error};
      $c->log->debug("$m: CAPTCHA(InValid) $ip $error ");

    }
  }

  if (!$is_valid_catpcha)
  {
    ##Recaptcha Start
    my $captcha_html = $o_captcha->get_html($pub_key_captcha);
    $c->stash->{captcha_html} = $captcha_html;
  }

  return $is_valid_catpcha;

}


=head2

=cut

sub _pvt_captcha
{
  my $c = shift;

  my $is_valid_captcha ;
  my $fn = "L/_pvt_captcha";
  my $in_field = "captcha_in";

  if($c->validate_captcha($c->req->param($in_field))	)
  {
    $c->log->info("$fn Captcha Validated:Yes");
    $is_valid_captcha = 1;
  }
  else
  {
    $c->log->info("$fn Captcha Validated:No");
  }

  return $is_valid_captcha;

}


=head1 AUTHOR

Tirveni Yadav,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as AGPLv3 itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
