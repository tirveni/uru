#!/usr/bin/perl -w
#
# Biz/Vehicle
#

package Biz::Vehicle;

use Moose;
use namespace::autoclean;

use TryCatch;


use Class::Utils
  qw(selected_language unxss_pk unxss unxss_an valid_email valid_boolean
     chomp_date valid_date get_array_from_argument trim );

my ($str_class);
{
  $str_class = "Biz::Vehicle";
}

=pod
=head1 NAME

Biz::Vehicle - Utilities for handling vehicle

=head1 SYNOPSIS

    use Biz::Vehicle;
    $o_vh	= Biz::Vehicle->new( $dbic, $vehicleid );
    $row	= $o_vh->dbrecord();

=head1 INHERITS

Biz::Vehicle

=cut

=head1 ADMINISTRIVIA

=over

=item B<new( $dbic, $vehicleid )>

Accept a Vehicle (either as a Vehicle=ID or as a DBIx::Class::Row
object and create a fresh Biz::Vehicle object from it. A DBIx obj
must be provided.

Return the Biz::Vehicle object, or undef if the Vehicle couldn't be
found.

=cut

# Constructor
sub new
{
  my $class		= shift;
  my $dbic		= shift;
  my $arg_vehicleid	= shift;

  my $m = "B/Vehicle/new";

  my $row    = $arg_vehicleid;

  unless ( ref($arg_vehicleid) )
  {
    $arg_vehicleid = unxss_pk($arg_vehicleid);
    if ($arg_vehicleid)
    {
      my $rs_cargo	= $dbic->resultset('Vehicle');
      $row	     	= $rs_cargo->find($arg_vehicleid);
    }
  }

  return (undef)
    unless $row;

  my $self = bless( {}, $class );
  $self->{vehicle_dbrecord}		= $row;

  return $self;

}

=item B<dbrecord()>

Return the DBIx::Class::Row object for this trip.

=cut
# Get the database object
sub dbrecord
{
  my
    $self = shift;
  return( $self->{vehicle_dbrecord} );
}

sub vehicle_dbrecord
{
  my
    $self = shift;
  return( $self->{vehicle_dbrecord} );
}

=head1 ACCESSORS

From Table Vehicle

=head2 vehicleid

Returns: vehicleid

=cut

sub vehicleid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'vehicleid';

  try  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 valid

Returns: valid

=cut

sub valid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'valid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 name

Returns: name

=cut

sub name
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'name';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 ownerid

Returns: ownerid

=cut

sub ownerid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'ownerid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head1 OPERATIONS

Operations

=head2 create($dbic,{vehicleid,name},userid)

Rseturns ($err_msg,$row_vehicle,$o_vehicle);

=cut

sub create
{
  my $dbic	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;

  my $fnx = "$str_class/create";
  my ($o_vehicle,$rs_vehicle,$row_vehicle,$vehicleid,$ownerid,
      $valid,$h_new,$err_msg);

  $valid = 't';
  my $max_length=14;
  my $in_vid;
  $in_vid	= $h_in->{vehicleid};
  print "$fnx Vehicle:$in_vid \n";

  ##Filter VehicleID
  {
    $in_vid	= Class::Utils::unxss_pk($in_vid)
      if($in_vid);

    $in_vid	= substr($in_vid,0,$max_length) 	if($in_vid);
    $in_vid	= uc($in_vid)			if($in_vid);


    $ownerid	= $h_in->{ownerid};
  }

  print "$fnx Vehicle: $in_vid \n";

  {
    $h_new->{vehicleid}			= $in_vid;
    #$h_new->{name}			= trim($h_in->{name});
    $h_new->{valid}			= $valid;
    $h_new->{ownerid}			= $ownerid if($ownerid);
    $h_new->{update_userid}		= $in_userid;
  }


  ##--Now create
#  try  {

    if ($h_new && defined($dbic) && $in_userid)
    {
      $rs_vehicle	= $dbic->resultset('Vehicle');
      $row_vehicle	= $rs_vehicle->create($h_new)
	if (defined($rs_vehicle));
    }


#  }
#    catch($err_msg){print "$fnx $err_msg \n"};

  if(defined($row_vehicle))
  {
    $o_vehicle = Biz::Vehicle->new($dbic,$row_vehicle);
  }

  return ($err_msg,$row_vehicle,$o_vehicle);

}

=head2 edit({name,valid},$userid)

Rseturns ($err_msg,$row_vehicle_updated);

=cut

sub edit
{
  my $self	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;

  my $fnx		= "$str_class/edit";
  print "$fnx Begin \n";

  my ($row_vehicle,$row_vehicle_updated,$is_valid,$h_new,$err_msg);
  print "$fnx Begin \n";

  {
    $h_new->{name}			= $h_in->{name};
    $h_new->{valid}			= $h_in->{valid};
    $h_new->{update_userid}		= $in_userid;
  }

  ##--Now create
  try  {

    if ($h_new && $in_userid)
    {
      $row_vehicle		= $self->dbrecord;
      $row_vehicle_updated	= $row_vehicle->update($h_new)
	if (defined($row_vehicle));
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_vehicle_updated);

}

=back

=cut

1;
