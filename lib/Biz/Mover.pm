#!/usr/bin/perl -w
#
# Copyright, Tirveni Yadav, 2018-02-11
# License: GPL v3
#

package Biz::Mover;


use Moose;
use namespace::autoclean;

use TryCatch;

use Class::Appuser;

use Class::Utils
  qw(makeparm selected_language unxss unxss_an valid_email
     chomp_date valid_date trim );

our $VERSION = "0.1";



my ($o_redis,$c_expire_ina_day,$str_class);
my ($prefix_buyer_branch);
{
  $o_redis = Class::Utils::get_redis;
  $c_expire_ina_day = $Class::Rock::seconds_day;
  $str_class = "Biz/Mover";
}


=head1 Mover

Agent,Staff or Driver

=cut

=pod

=head1 NAME

Biz::Mover - Utilities for handling Movers related data.

=head1 SYNOPSIS

    use Biz::Mover;
    $byr = Biz::Mover->new( $dbic, $userid );
    $userid	      = $o_byr->userid();


=head2 new

Get Info

=cut

sub new
{
  my $class		= shift;
  my $dbic		= shift;
  my $arg_userid	= shift;

  my $m = "B/Mover->new";

  my $row    = $arg_userid;

  unless ( ref($arg_userid) )
  {
    if ($arg_userid)
    {
      my $rs_ci		= $dbic->resultset('Customerinfo');
      $row	     	= $rs_ci->find($arg_userid);
    }
  }

  return (undef)
    unless $row;

  my $self = bless( {}, $class );
  $self->{mover_dbrecord}		= $row;

  return $self;

}

=item B<dbrecord()>

Return the DBIx::Class::Row object for this CI.

=cut

sub dbrecord
{
  my
    $self = shift;
  return( $self->{mover_dbrecord} );
}


=head2 info($dbic)

Argument: $dbic

Returns: Hash Ref from CustomerInfo Table.

=cut

sub info
{
  my $self		=	shift;
  my $dbic		=	shift;

  my $row_ci = $self->dbrecord;

  my %x_data = $row_ci->get_columns;
  my $h_xd    = Class::Utils::trim_hash_vals(\%x_data);

  return $h_xd;
}



=head1 ACCESSORS

 From the CustomerInfo Table

=head2 src_idtype

Returns: src_idtype

=cut

sub src_idtype
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_idtype';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}



=head2 src_country

Returns: src_country

=cut

sub src_country
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_country';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 src_state

Returns: src_state

=cut

sub src_state
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_state';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}



=head2 src_city

Returns: src_city

=cut

sub src_city
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_city';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 addressid

Returns: addressid

=cut

sub addressid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'addressid';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}



=head2 created_at

Returns: created_at

=cut

sub created_at
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'created_at';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}



=head2 src_id

Returns: src_id

=cut

sub src_id
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_id';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 userid

Returns: userid

=cut

sub userid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'userid';

  try
  {
    my $row_ci = $self->dbrecord;
    $x_value = $row_ci->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head1 OPERATIONS

OPS of Movers

=head2 create($dbic,$o_appuser,{userid,src_id,src_idtype,src_country,
src_state,src_city})


=cut

sub create
{
  my $dbic	= shift;
  my $o_appuser	= shift;
  my $in_h	= shift;

  my $fn = "Biz/Mover/create";
  my ($h_new,$row_ci,$err_msg);

  my $userid	= $o_appuser->userid;
  my $o_mover	= Biz::Mover->new($dbic,$userid);
  {
    $h_new->{userid}		= $o_appuser->userid;
    $h_new->{src_id}		= $in_h->{src_id};
    $h_new->{src_idtype}	= $in_h->{src_idtype};

    $h_new->{src_country}	= $in_h->{src_country};
    $h_new->{src_state}		= $in_h->{src_state};
    $h_new->{src_city}		= $in_h->{src_city};

    $h_new->{addressid}		= $in_h->{addressid};
  }

  try
  {
    if (!$o_mover && $h_new)
    {
      my $rs_ci = $dbic->resultset('Customerinfo');

      $row_ci	= $rs_ci->create($h_new)
	if($rs_ci);
    }
  }
    catch($err_msg)
      {
	print "$fn $err_msg \n";
      };

  if ($row_ci)
  {
    $o_mover = Biz::Mover->new($dbic,$row_ci);
  }

  return ($err_msg,$row_ci,$o_mover);

}

=head2 edit({userid,src_id,src_idtype,src_country,
src_state,src_city})

Edit Mover

=cut

sub edit
{
  my $self		=shift;
  my $in_h	= shift;

  my $fn = "Biz/Mover/edit";
  my ($h_edit,$row_ci,$err_msg,$row_updated);

  $row_ci = $self->dbrecord;
  my $dbic;
  $dbic = $row_ci->result_source->schema;

  my $is_address_update = $in_h->{update_address};
  my $exist_addressid	= $self->addressid;
  my $o_address_exist;
  if ($exist_addressid)
  {
    $o_address_exist = Class::Address->new($dbic,$exist_addressid);
  }

  my ($co,$st,$city);
  {
    $h_edit->{src_id}		= $in_h->{src_id}
      if($in_h->{src_id});
    $h_edit->{src_idtype}	= $in_h->{src_idtype}
      if($in_h->{src_idtype});


    $co			= $in_h->{src_country};
    $st			= $in_h->{src_state};
    $city		= $in_h->{src_city};

    $h_edit->{src_country}	= $in_h->{src_country}
      if($co);
    $h_edit->{src_state}	= $in_h->{src_state}
      if($st);
    $h_edit->{src_city}		= $in_h->{src_city}
      if($city);

  }


  ##Address Add
  {
    my ($line1,$line2,$line3,$o_address);
    $line1 = $in_h->{streetaddress1};
    $line2 = $in_h->{streetaddress2};
    $line3 = $in_h->{streetaddress3};
    my $h_ax;
    if ($line1 && $co && $st && $city 
	&& (!$o_address_exist || $is_address_update > 0 )
       )
    {
      $h_ax->{streetaddress3} 	= $line3;
      $h_ax->{streetaddress2} 	= $line2;
      $h_ax->{streetaddress1} 	= $line1;
      $h_ax->{address_city}	= $city;
      $h_ax->{address_state}   	= $st;
      $h_ax->{address_country} 	= $co;
      $o_address = Class::Address::create($dbic,$h_ax);

      if ($o_address)
      {
	$h_edit->{addressid}	= $o_address->addressid;
      }
    }
  }


  try
  {
    if ($row_ci && $h_edit)
    {
      $row_updated	= $row_ci->update($h_edit);
    }

  }
    catch($err_msg)
    {
      print "$fn $err_msg \n";
    }
  ;

  return ($err_msg,$row_updated);



}

1;

=head1 LICENSE

This is free software; you can redistribute it and/or modify it under
AGPLv3. Copyright tirveni@udyansh.org

=cut
