#!/usr/bin/perl -w
#
# Biz/Cargo.pm
#

package Biz::Cargo;

use Moose;
use namespace::autoclean;

use TryCatch;


use Class::Utils
  qw(selected_language unxss unxss_an valid_email valid_boolean
     chomp_date valid_date get_array_from_argument trim );

my ($str_class);
{
  $str_class = "Biz::Cargo";
}

=pod
=head1 NAME

Biz::Cargo - Utilities for handling address-related data

=head1 SYNOPSIS

    use Biz::Cargo;
    $oa = Biz::Cargo->new( $dbic, $address );
    $row = $oa->dbrecord();

=head1 INHERITS

Biz::Cargo

=cut

=head1 ADMINISTRIVIA

=over

=item B<new( $dbic, $cargoid )>

Accept a cargo (either as a Cargo ID or as a DBIx::Class::Row
object and create a fresh Biz::Cargo object from it. A DBIx obj
must be provided.

Return the Biz::Cargo object, or undef if the Cargo couldn't be
found.

=cut

# Constructor
sub new
{
  my $class		= shift;
  my $dbic		= shift;
  my $arg_cargoid	= shift;

  my $m = "C::Cargo->new";

  my $row    = $arg_cargoid;

  unless ( ref($arg_cargoid) )
  {
    $arg_cargoid = unxss($arg_cargoid);
    if ($arg_cargoid)
    {
      my $rs_cargo	= $dbic->resultset('Cargo');
      $row	     	= $rs_cargo->find($arg_cargoid);
    }
  }

  return (undef)
    unless $row;

  my $self = bless( {}, $class );
  $self->{cargo_dbrecord}		= $row;

  return $self;

}

=item B<dbrecord()>

Return the DBIx::Class::Row object for this cargo.

=cut
# Get the database object
sub dbrecord
{
  my
    $self = shift;
  return( $self->{cargo_dbrecord} );
}

sub cargo_dbrecord
{
  my
    $self = shift;
  return( $self->{cargo_dbrecord} );
}

=head1 ACCESSORS

From Table Cargo.

=head2 cargoid

Returns: cargoid

=cut

sub cargoid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'cargoid';

  try  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }catch($err_msg){print "$str_class $err_msg \n"};

  $x_value = trim($x_value);

  return $x_value;

}

=head2 product_name

Returns: product_name

=cut

sub product_name
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'product_name';

  try  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 valid

Returns: valid

=cut

sub valid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'valid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);
  }  catch($err_msg){print "$str_class $err_msg \n"};



  return $x_value;

}

=head2 description

Returns: description

=cut

sub description
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'description';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 comments

Returns: comments

=cut

sub comments
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'comments';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);


  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 stageid

Returns: stageid

=cut

sub stageid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'stageid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 shipping_weight

Returns: shipping_weight

=cut

sub shipping_weight
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'shipping_weight';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 is_payby_src

Returns: is_payby_src

=cut

sub is_payby_src
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'is_payby_src';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 is_cartage_paid

Returns: is_cartage_paid

=cut

sub is_cartage_paid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'is_cartage_paid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 cartage_price

Returns: cartage_price

=cut

sub cartage_price
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'cartage_price';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 transporter_invoiceid

Returns: transporter_invoiceid

=cut

sub transporter_invoiceid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'transporter_invoiceid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 transactionid

Returns: transactionid

=cut

sub transactionid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'transactionid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 created_at

Returns: created_at

=cut

sub created_at
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'created_at';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 update_userid

Returns: update_userid

=cut

sub update_userid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'update_userid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 src_customerid

Returns: src_customerid

=cut

sub src_customerid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_customerid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 src_idtype

Returns: src_idtype

=cut

sub src_idtype
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_idtype';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 src_id

Returns: src_id

=cut

sub src_id
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_id';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 src_country

Returns: src_country

=cut

sub src_country
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_country';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 src_state

Returns: src_state

=cut

sub src_state
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_state';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 src_city

Returns: src_city

=cut

sub src_city
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_city';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 dst_customerid

Returns: dst_customerid

=cut

sub dst_customerid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_customerid';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 dst_idtype

Returns: dst_idtype

=cut

sub dst_idtype
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_idtype';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 dst_id

Returns: dst_id

=cut

sub dst_id
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_id';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 dst_country

Returns: dst_country

=cut

sub dst_country
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_country';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 dst_state

Returns: dst_state

=cut

sub dst_state
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_state';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 dst_city

Returns: dst_city

=cut

sub dst_city
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_city';

  try
  {
    my $row_cargo = $self->dbrecord;
    $x_value = $row_cargo->get_column($x_field);
    $x_value = trim($x_value);

  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 full_info($dbic,$row_cargo)

Returns: HashRef

=cut

sub full_info
{
  my $self	=	shift;
  my $dbic	=	shift;
  my $row_cargo	=	shift;

  my $fn = "Cargo/cargo_info";
  my ($h_xd,$row_cmore,$h_morex);

  if (!defined($row_cargo))
  {
    $row_cargo = $self->dbrecord;
  }

  {
    my $cargoid = $self->cargoid;
    my $str_related_cmore = 'cargomore';
    $row_cmore	= $row_cargo->find_related
      ($str_related_cmore,{cargoid=>$cargoid});

    my %mx_data	= $row_cmore->get_columns if(defined($row_cmore));
    $h_morex	= Class::Utils::trim_hash_vals(\%mx_data);

  }



  if (defined($row_cargo))
  {
    my %x_data	= $row_cargo->get_columns;
    $h_xd	= Class::Utils::trim_hash_vals(\%x_data);

    my $is_cartage_paid = $h_xd->{is_cartage_paid};
    my $is_pay_by_src	= $h_xd->{is_pay_by_src};
    print "$fn paid:$is_cartage_paid,by_src:$is_pay_by_src \n";

    ##Src
    {
      my ($co,$st,$ci,$o_city,$src_cname,$src_ccode);

      $co = $h_xd->{src_country};
      $st = $h_xd->{src_state};
      $ci = $h_xd->{src_city};
      $o_city = Class::City->new($dbic,$co,$st,$ci);
      if ($o_city)
      {
	($src_ccode,$src_cname) = $o_city->x_name();
	$h_xd->{src_cityname} = $src_cname;
	$h_xd->{src_citycode} = $src_ccode;
      }

      my ($src_customerid,$src_customer_name,$o_appuser_src);
      $src_customerid	= $self->src_customerid;
      $o_appuser_src	= Class::Appuser->new($dbic,$src_customerid);
      if ($o_appuser_src)
      {
	$h_xd->{src_customer_name} = $o_appuser_src->aname;
      }

      my ($o_address_src,$str_src_address);
      my $is_src = 't';
      $o_address_src = $self->get_address($dbic,$is_src);
      $str_src_address = $o_address_src->brief_text($dbic)
	if($o_address_src);
      $h_xd->{str_src_address} = $str_src_address;
      print "$fn SRC Address: $str_src_address \n";
    }

    ##DST
    {
      my ($co,$st,$ci,$o_city,$dst_cname,$dst_ccode);
      $co = $h_xd->{dst_country};
      $st = $h_xd->{dst_state};
      $ci = $h_xd->{dst_city};
      $o_city = Class::City->new($dbic,$co,$st,$ci);
      if ($o_city)
      {
	($dst_ccode,$dst_cname) = $o_city->x_name();
	$h_xd->{dst_cityname} = $dst_cname;
	$h_xd->{dst_citycode} = $dst_ccode;
      }

      my ($dst_customerid,$dst_customer_name,$o_appuser_dst);
      $dst_customerid	= $self->dst_customerid;
      $o_appuser_dst	= Class::Appuser->new($dbic,$dst_customerid);
      if ($o_appuser_dst)
      {
	$h_xd->{dst_customer_name} = $o_appuser_dst->aname;
      }

      my ($o_address_dst,$str_dst_address);
      my $is_src = 'f';
      $o_address_dst = $self->get_address($dbic,$is_src);
      $str_dst_address = $o_address_dst->brief_text($dbic)
	if($o_address_dst);
      $h_xd->{str_dst_address} = $str_dst_address;
      print "$fn SRC Address: $str_dst_address \n";

    }

  }

  ##Join both the Hashes
  my %both;
  if ($h_morex)
  {
    %both = (%$h_xd, %$h_morex);
  }
  else
  {
    %both = %$h_xd;
  }

  return \%both;



}

=head1 OPERATIONS

Operations on Cargo

=head2 stage_types

Stages Available for a Cargo

Returns: Array Ref to Stages

BOOKED  INTRANSIT LOST DELIVERED CANCEL

=cut

sub stage_types
{
  my @list = ('BOOKED','INTRANSIT','LOST','DELIVERED','CANCEL');

  return \@list;

}


=head2 create($dbic,$h_input)

Rseturns ($err_msg,$row_cargo,$o_cargo);

=cut

sub create
{
  my $dbic	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;

  my $fnx = "$str_class/create";
  my ($o_cargo,$rs_cargo,$row_cargo,
      $default_stageid,$valid,$h_new,$err_msg);
  my ($src_customerid,$src_country,$src_state,$src_city,$product_name);

  print "$fnx Begin \n";
  ##--Default Stuff
  $default_stageid	= 'BOOKED';
  $valid		= 't';

  {
    $src_city		= $h_in->{src_city};
    $src_state		= $h_in->{src_state};
    $src_country	= $h_in->{src_country};

    $src_customerid	= $h_in->{src_customerid};
    $product_name	= $h_in->{product_name};
  }

  if($src_customerid && $src_country && $src_state && $src_city
    && $product_name)
  {
    $h_new->{update_userid}		= $in_userid;
    $h_new->{product_name}	= $product_name;
    ##Default Fill
    $h_new->{stageid}		= $default_stageid;
    $h_new->{valid}		= $valid;

    ##Source
    $h_new->{src_city}		= $src_city;
    $h_new->{src_country}	= $src_country;
    $h_new->{src_state}	        = $src_state;
    $h_new->{src_customerid}	= $src_customerid  ;

    $h_new->{src_idtype}	= $h_in->{src_idtype}
      if($h_in->{src_idtype}) ;
    $h_new->{src_id}		= $h_in->{src_id}
      if($h_in->{src_id}) ;



    ##Destination
    $h_new->{dst_city}		= $h_in->{dst_city} if($h_in->{dst_city});
    $h_new->{dst_country}	= $h_in->{dst_country}
      if($h_in->{dst_country});;
    $h_new->{dst_state}	        = $h_in->{dst_state}
      if($h_in->{dst_state});;

    $h_new->{dst_customerid}	= $h_in->{dst_customerid}
      if($h_in->{dst_customerid}) ;
    $h_new->{dst_idtype}	= $h_in->{dst_idtype}
      if($h_in->{dst_idtype}) ;
    $h_new->{dst_id}		= $h_in->{dst_id}
      if($h_in->{dst_id}) ;

    $h_new->{shipping_weight}		= $h_in->{shipping_weight}
      if($h_in->{shipping_weight}) ;

    $h_new->{is_payby_src}	=	valid_boolean($h_in->{is_payby_src});

    $h_new->{cartage_price}		= $h_in->{cartage_price}
      if($h_in->{cartage_price}) ;

  }

  ##--Now create
  try
  {
    if ($h_new && defined($dbic) && $in_userid)
    {
      $rs_cargo		= $dbic->resultset('Cargo');
      $row_cargo	= $rs_cargo->create($h_new)
	if (defined($rs_cargo));
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  if(defined($row_cargo))
  {
    $o_cargo = Biz::Cargo->new($dbic,$row_cargo);
  }

  return ($err_msg,$row_cargo,$o_cargo);

}


=head2 edit_allowed()

Reeturns $is_allowed

1 / OnDef

IF Delivered. Then Not Allowed.

IF delivered, then change status to something else, then only any edit alowed.

=cut

sub edit_allowed
{
  my $self	= shift;

  my $mx = "B/cargo/edit_allowed";
  my $cargo_stageid	= $self->stageid;
  my $is_allowed;

  print "$mx STAGE: $cargo_stageid. \n";

  if ($cargo_stageid eq 'INTRANSIT' || $cargo_stageid eq 'DELIVERED')
  {
    $is_allowed	=	undef;
  }
  else
  {
    $is_allowed = 1;
  }

  return $is_allowed;

}


=head2 edit($h_input,$userid,$reason)

Returns ($err_msg,$row_cargo_updated);

=cut

sub edit
{
  my $self	= shift;
  my $h_in	= shift;
  my $in_staffid	= shift;
  my $reason	= shift;

  my $fnx		= "$str_class/edit";
  print "$fnx Begin \n";

  my ($dbic,$row_cargo,$row_cargo_updated);
  my ($row_cmore,$h_new_more,$row_cmore_updated);

  my ($is_valid,$cargoid,$err_msg);
  my ($cargo_stageid,$txnid,$is_allowed);

  ##--1. Base Stuff:dbic,Row_cargo,stage,Editable
  {
    $cargoid		= $self->cargoid;
    $row_cargo		= $self->dbrecord;
    $cargo_stageid	= $self->stageid;
    $txnid		= $self->transactionid;
    $is_allowed = $self->edit_allowed();
    $dbic = $row_cargo->result_source->schema;
  }

  print "$fnx Begin, Edit:$is_allowed \n";

  ##--2. Cargo More Table
  my $is_more;
  {
    ($is_more,$h_new_more,$row_cmore) =
      _prepare_cmore($row_cargo,$h_in);
    print "$fnx Cmore:$row_cmore \n";
  }##More Table

  ##--3. Customer: SRC/DST Input, And Their input Addresses
  my ($in_src_guestid,$in_dst_guestid,$h_in_src,$h_in_dst,$o_addr_ofsrc_guest,
     $o_addr_ofdst_guest,$o_mover_src,$o_mover_dst);
  {
    ($h_in_src,$o_addr_ofsrc_guest,$o_mover_src)=
      _fill_src_guest($dbic,$h_in);
    ($h_in_dst,$o_addr_ofdst_guest,$o_mover_dst)=
      _fill_dst_guest($dbic,$h_in);

    $in_src_guestid = $o_mover_src->userid if($o_mover_src);
    $in_dst_guestid = $o_mover_dst->userid if($o_mover_dst);
  }

  ##--4. Addresses of SRC/DST
  my ($current_src_customerid,$current_dst_customerid);
  my ($is_src_address_update,$is_dst_address_update);
  {

    ##--a. Specific Request by User for Address Update.
    $is_src_address_update	= $h_in->{src_address_update};
    $is_dst_address_update	= $h_in->{dst_address_update};

    $current_src_customerid	= $self->src_customerid;
    $current_dst_customerid	= $self->dst_customerid;

    print "$fnx SRC: in($in_src_guestid),Exist($current_src_customerid) \n";
    print "$fnx DST: in($in_dst_guestid),Exist($current_dst_customerid) \n";


    ##--b. Address Edited if Different customer.
    if ($in_dst_guestid && ($current_dst_customerid ne $in_dst_guestid))
    {
      $is_dst_address_update = 1;
    }
    if ($in_src_guestid && ($current_src_customerid ne $in_src_guestid))
    {
      $is_src_address_update = 1;
    }
    print "$fnx Src Address:$is_src_address_update ".
      "DST Address:$is_src_address_update:  \n";
  }

  ##Address Update Logic
  {
    my ($o_address_src_exist,$o_address_dst_exist);
    my ($is_src_add_force,$is_dst_add_force,$h_srcx,$h_dstx);
    $h_srcx =
    {
     staff_request	=>	$is_src_address_update,
     exist_address	=>	$o_address_src_exist,
     guest_address	=>	$o_addr_ofsrc_guest,
     in_guestid	  	=> 	$in_src_guestid,
     exist_guestid	=>	$current_src_customerid,
    };
    $is_src_add_force = _address_edit_reqd($h_srcx);

    $h_dstx =
    {
     staff_request	=>	$is_dst_address_update,
     exist_address	=>	$o_address_dst_exist,
     guest_address	=>	$o_addr_ofdst_guest,
     in_guestid	  	=> 	$in_dst_guestid,
     exist_guestid	=>	$current_dst_customerid,

    };
    $is_dst_add_force = _address_edit_reqd($h_dstx);
  }


  ##--5. Cargo Table Hash for Update
  my $h_new;
  {
    $h_new = _prepare_cargo($h_in,$in_staffid);
  }

  ##--Now create
  try  {

    if ($h_new && ($is_allowed) && $in_staffid)
    {
      $dbic->txn_do
	(sub
	 {
	   $row_cargo_updated	= $row_cargo->update($h_new)
	     if (defined($row_cargo));

	   if ($is_more)
	   {
	     $h_new_more->{update_userid} = $in_staffid;
	     $row_cmore_updated =
	       _edit_cmore($row_cargo_updated,$h_new_more,$row_cmore);
	   }

	   if ($in_dst_guestid && $is_dst_address_update
	       && $o_addr_ofdst_guest && $in_staffid)
	   {
	     my $is_src = 'f';
	     $self->attach_address
	       ($dbic,$is_src,$o_addr_ofdst_guest,$in_dst_guestid,$in_staffid);
	   }

	   if ($in_src_guestid && $is_src_address_update
	       && $o_addr_ofsrc_guest && $in_staffid)
	   {
	     my $is_src = 't';
	     $self->attach_address
	       ($dbic,$is_src,$o_addr_ofsrc_guest,$in_src_guestid,$in_staffid);
	   }

	 });
    }
    elsif (!$is_allowed)
    {
      print "$fnx: Allowed:$is_allowed \n";
      $err_msg = "Edit is not allowed for this cargo.";
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_cargo_updated);

}

=head2 _address_edit_reqd({staff_request,exist_address,guest_address})

Returns: If Update Required.

exist_address,guest_address: are Class::Address Objects.

{
staff_request	=> 1,
exist_address	=> $o_address_src_exist,
guest_address	=> $o_address_src_guest,
in_guestid,
exist_guestid
}

=cut

sub _address_edit_reqd
{
  my $h_ax			=	shift;

  my $fx = "b/cargo/_address_edit_reqd";
  my $is_request_bystaff	=	$h_ax->{staff_request};
  my $o_address_exist		=	$h_ax->{exist_address};
  my $o_address_guest		=	$h_ax->{guest_address};
  my $in_guestid		=	$h_ax->{in_guestid};	
  my $exist_guestid		=	$h_ax->{exist_guestid};	

  my ($is_update);
  my ($addressid_guest,$addressid_exist);
  {
    $addressid_guest	= $o_address_guest->addressid
      if($o_address_guest);

    $addressid_exist	= $o_address_exist->addressid
      if($o_address_exist);
  }


  if (($in_guestid ne $exist_guestid) && ($o_address_guest) )
  {
    $is_request_bystaff = 1;
    print "$fx 1: Different Customers.  \n";
  }
  elsif ($is_request_bystaff	&& $o_address_guest)
  {
    $is_update	= 1;
    print "$fx 2: Staff Request. Guest has address. YES \n";
  }
  elsif ($addressid_exist	&& !$addressid_guest)
  {
    $is_update = undef;
    print "$fx 3: Current Address Exist,New Doesn't.NO \n";
  }
  elsif (!$addressid_exist	&& $addressid_guest)
  {
    $is_update = 1;
    print "$fx 4: Current No , New Does. YES \n";
  }
  elsif ($addressid_guest	!= $addressid_exist)
  {
    $is_update = 1;
    print "$fx 5: Current NOT EQ NEW. YES \n";
  }

  return ($is_update);

}


=head2 change_status($h_input,$in_userid[,$reason])

Rseturns ($err_msg,$row_cargo_updated);

BOOKED,INTRANSIT,DELIVERED.

=cut

sub change_status
{
  my $self	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;
  my $reason	= shift;

  my $fnx		= "$str_class/change_status";
  my ($h_new,$err_msg,$row_cargo_updated,$row_cargo,$h_cargostage);
  my ($cargo_stageid,$txnid,$is_delivered,$dbic,$is_allowed,
     $new_stageid);
  {
    $cargo_stageid	= $self->stageid;
    $txnid		= $self->transactionid;
    $row_cargo		= $self->dbrecord;
    $dbic		= $row_cargo->result_source->schema;

    $is_allowed = $self->edit_allowed();
  }

  {
    my $in_stageid = $h_in->{stageid};
    print "$fnx New:$in_stageid  \n";
    my $list_types = Biz::Cargo::stage_types();
    my @a_list = @$list_types;
    print "$fnx ARR: @a_list  \n";


    $new_stageid
      = Class::Utils::str_in_array($list_types,$in_stageid);
  }
  print "$fnx New:$new_stageid  \n";


  my $msg = $reason;
  if($reason)
  {
    $msg = "Old Status: $cargo_stageid, Reason:$reason";
  }

  {
    $h_new->{stageid}	        = $new_stageid
      if ($new_stageid);

    $h_cargostage->{reason}	= $msg;
    $h_cargostage->{update_userid}	= $in_userid;
    $h_cargostage->{city}		= $h_in->{city}
      if ($h_in->{city});
    $h_cargostage->{state}	        = $h_in->{state}
      if ($h_in->{state});
    $h_cargostage->{country}		= $h_in->{country}
      if ($h_in->{country});
    $h_cargostage->{stageid}	        = $new_stageid
      if ($new_stageid);

  }

  my $f_cargostages = 'cargostages';

  ##--Now Edit
  try  {

    if ($h_new && $reason && $new_stageid && defined($row_cargo))
    {
      $dbic->txn_do
	(sub
	 {
	   $row_cargo->create_related($f_cargostages,$h_cargostage);
	   $row_cargo_updated	= $row_cargo->update($h_new);
	 }
	);
    }
    else
    {
      print "$fnx: Missing Fields \n";
      $err_msg = "Reason and New StageID are required.";
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_cargo_updated);

}



=head2 disable($h_input,$in_userid[,$reason])

Rseturns ($err_msg,$row_cargo_updated);

=cut

sub disable
{
  my $self	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;
  my $reason	= shift;

  my $fnx		= "$str_class/invalid";
  my $dbic;
  my ($h_new,$err_msg,$row_cargo_updated,$row_cargo,$h_cargostage);
  my ($cargo_stageid,$txnid,$is_delivered,$new_stageid);
  $cargo_stageid	= $self->stageid;
  $txnid		= $self->transactionid;
  if($cargo_stageid eq 'DELIVERED')
  {
    $is_delivered = 1;
  }
  my $is_allowed = $self->edit_allowed();

  $new_stageid = $h_in->{stageid};
  $row_cargo	= $self->dbrecord;
  $dbic		= $row_cargo->result_source->schema;

  my $msg = $reason;
  if($reason)
  {
    $msg = "Old Status: $cargo_stageid, Reason:$reason";
  }

  {
    $h_new->{stageid}	        = $new_stageid
      if ($new_stageid);
    $h_new->{valid}	        	= 'f';

    $h_cargostage->{stageid}	        = $new_stageid
      if ($new_stageid);
    $h_cargostage->{reason}		= $msg;
    $h_cargostage->{update_userid}	= $in_userid;
  }

  my $f_cargostages = 'cargostages';

  ##--Now Edit
  try  {

    if ($h_new && $is_allowed && $new_stageid && defined($row_cargo) )
    {
      $dbic->txn_do
	(sub
	 {
	   $row_cargo->create_related($f_cargostages,$h_cargostage);
	   $row_cargo_updated	= $row_cargo->update($h_new);
	 }
	);
    }
    elsif (!$is_allowed)
    {
      print "$fnx: Allowed:$is_allowed \n";
      $err_msg = "Edit is not allowed for this cargo.";
    }
    else
    {
      print "$fnx: Allowed:$is_allowed \n";
      $err_msg = "To Disable cargo, Required fields are missing.";
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_cargo_updated);

}


=head2 payment_complete($h_input,$in_userid[,$reason])

Returns ($err_msg,$row_cargo_updated);

=cut

sub payment_complete
{
  my $self	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;
  my $reason	= shift;

  my $fnx		= "$str_class/invalid";
  my ($h_new,$err_msg,$row_cargo_updated,$row_cargo);
  my ($cartage_price,$txnid,$is_delivered,$dbic);
  $cartage_price = $self->cartage_price;
  $row_cargo	= $self->dbrecord;
  $dbic		= $row_cargo->result_source->schema;

  {
    $h_new->{is_cartage_paid}	        	= 't';

    if($h_in->{transactionid})
    {
      $h_new->{transactionid}			= $h_in->{transactionid}
    }
    else
    {
      $h_new->{transactionid}		= 1;
    }

  }

  my $f_cargostages = 'cargostages';

  ##--Now Edit
  try  {

    if ($h_new && ($cartage_price > 0 ) && defined($row_cargo) )
    {
      $dbic->txn_do
	(sub
	 {
	   $row_cargo_updated	= $row_cargo->update($h_new);
	 }
	);
    }
  }
    catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_cargo_updated);

}

=head1 MORE

Address and CargoMore

=head2 cargo_more($h_input)

Return ($err_msg,$row_cargomore_updated)

=cut

sub cargo_more
{
  my $self	=	shift;
  my $h_in	= shift;
  my $in_userid	= shift;

  my $fnx		= "$str_class/invalid";
  my ($dbic,$row_cargo,$row_cargomore,$err_msg,$row_cargomore_updated);
  $row_cargo = $self->dbrecord;
  $dbic		= $row_cargo->result_source->schema;

  my $is_allowed = $self->edit_allowed;
  $h_in->{update_userid} = $in_userid;

  my $f_cargomore = 'cargomore';

  $row_cargomore = $row_cargo->find_related($f_cargomore);

  ##--Now Edit

  try  {

    if ($is_allowed && $h_in )
    {

      if (defined($row_cargomore))
      {
	$row_cargomore_updated = $row_cargomore->update($h_in);
      }
      else
      {
	$row_cargomore_updated =
	  $row_cargo->create_related($f_cargomore,$h_in);
      }

    }
    elsif (!$is_allowed)
    {
      print "$fnx: Allowed:$is_allowed \n";
      $err_msg = "Edit is not allowed for this cargo.";
    }
    else
    {
      $err_msg = "Edit Failed for unknow reason";
    }

  }
      catch($err_msg){print "$fnx $err_msg \n"};



  return ($err_msg,$row_cargomore_updated);

}


=head2 add_address($dbic,{streetaddress1,streetaddress2,
streetaddress2,pincode,directions,address_city,address_state,address_country} 
[,phone])

Address is added and Attached to Branch Order.

=cut

sub add_address
{
  my $self      = shift;
  my $dbic      = shift;
  my $h_in      = shift;
  my $customerid = shift;
  my $is_src	= shift;
  my $in_staffid = shift;

  my $fnx = "B/Cargo/add_address";
  my $err_msg;
  my $is_allowed = $self->edit_allowed;
  my ($o_address);

  try {

    if ($is_allowed)
    {
      $dbic->txn_do
	(sub
	 {
	   $o_address = Class::Address::create($dbic,$h_in);
	   $self->attach_address
	     ($dbic,$o_address,$customerid,$is_src,$in_staffid);
	 });
    }
    elsif (!$is_allowed)
    {
      print "$fnx: Allowed:$is_allowed \n";
      $err_msg = "Add Address is not allowed for this cargo.";
    }

  };

  return ($err_msg,$o_address);

}

=head2 attach_address($dbic,$is_src,$o_address,$customerid,$in_staffid)

Attach Existing Address

=cut

sub attach_address
{
  my $self		= shift;
  my $dbic		= shift;
  my $is_src		= shift;
  my $o_address 	= shift;
  my $customerid 	= shift;
  my $in_staffid	= shift;

  my $fnx = "B/Cargo/attach_address";
  my ($addressid,$row_cargo,$row_ca_updated,$row_ca,$err_msg);
  my ($is_allowed,$h_cargoadd,$f_cargoaddress,$rs_cargoaddress);
  $row_cargo 			= $self->dbrecord;
  $addressid       		= $o_address->addressid;
  $is_allowed			= $self->edit_allowed;

  ##Fill in Source and CustomerID
  $h_cargoadd->{is_src}		= $is_src;
  $h_cargoadd->{customerid}	= $customerid;

  ##get Existing Address Attachment
  {
    $f_cargoaddress		= 'cargoaddresses';
    $rs_cargoaddress		= $row_cargo->search_related
      ($f_cargoaddress,$h_cargoadd);
    $row_ca			= $rs_cargoaddress->first
      if (defined($rs_cargoaddress));
  }

  ##Fill in AddressID
  if($o_address)
  {
    $addressid			= $o_address->addressid;
    $h_cargoadd->{addressid}	= $addressid;
    $h_cargoadd->{update_userid} = $in_staffid;
  }

  ##Attach Address
    try    {

      if ($is_allowed && $h_cargoadd)
      {

	if (defined($row_ca))
	{
	  $row_ca_updated = $row_ca->update($h_cargoadd);
	}
	elsif ($h_cargoadd)
	{
	    $row_ca_updated = $row_cargo->create_related
	      ($f_cargoaddress,$h_cargoadd);
	}
      }
      elsif (!$is_allowed)
      {
	print "$fnx: Allowed:$is_allowed \n";
	$err_msg = "Attaching address is not allowed for this cargo.";
      }
      else
      {
	$err_msg = "Failed";
      }

    }
      catch($err_msg){print "$fnx $err_msg \n"};


  return ($err_msg,$row_ca_updated);

}

=head2 get_address($dbic,$is_src)

Gets the DST address for A Cargo.

If is_src = 'f', then Soruce Address for the Cargo.

Returns: (Row_CargoAddress,$o_address)

=cut

sub get_address
{
  my $self = shift;
  my $dbic	=	shift;
  my $in_is_src	=	shift;

  my $mx = "b/cargo/get_address";
  my $is_src = 'f';
  if ($in_is_src eq 't' || $in_is_src > 0)
  {
    $is_src = 't';
  }

  my ($addressid,$row_cargo,$row_ca_updated,$row_ca,$err_msg);
  my ($f_cargoaddress,$rs_cargoaddress);
  ##Fill in Source and CustomerID
  my $h_cargoadd;
  $h_cargoadd->{is_src}		= $is_src;
  $h_cargoadd->{cargoid}	= $self->cargoid	    ;
  $row_cargo = $self->dbrecord;

  ##get Existing Address Attachment
  {
    $f_cargoaddress		= 'cargoaddresses';
    $rs_cargoaddress		= $row_cargo->search_related
      ($f_cargoaddress,$h_cargoadd);
    $row_ca			= $rs_cargoaddress->first
      if (defined($rs_cargoaddress));
  }

  my $row_address = $row_ca->addressid if(defined($row_ca));
  my $o_address = Class::Address->new($dbic,$row_address);

  return ($row_address,$o_address);

}

=head1 TRIP OPS

ToDO

=head2 attach_trip($dbic,$o_trip,$in_staffid)

Return ($err_msg,$row_tripcargo_updated)

ToDO

=cut

sub attach_trip
{
  my $self		= shift;
  my $dbic		= shift;
  my $o_trip 		= shift;
  my $in_staffid	= shift;
  my $o_address 	= shift;

  my $fnx		= "B/Cargo/attach_trip";
  my ($tripid,$row_cargo,$row_tripcargo_updated,$row_ca,$err_msg);
  my ($is_allowed,$h_tripadd,$f_cargotrip,$rs_cargotrip);
  $row_cargo 		= $self->dbrecord;
  $tripid       	= $o_trip->tripid;
  $is_allowed		= $self->edit_allowed;

  $h_tripadd->{tripid}	= $tripid;

  ##get Existing Trip Attachment
  {
    $f_cargotrip		= 'tripcargoes';
    $rs_cargotrip		= $row_cargo->search_related
      ($f_cargotrip,$h_tripadd);
    $row_ca			= $rs_cargotrip->first
      if (defined($rs_cargotrip));
  }

  ##Fill in TripID
  if($o_trip)
  {
    $tripid			= $o_trip->tripid;
    $h_tripadd->{tripid}	= $tripid;
    $h_tripadd->{update_userid} = $in_staffid;
    $h_tripadd->{addressid}	= $o_address->addressid
      if($o_address);
    $h_tripadd->{valid}		= 't';
  }

  ##Cargostatus
  my ($h_cargo_status,$str_cargo_status_reason);
  {
    $h_cargo_status->{stageid}	= "INTRANSIT";
    $str_cargo_status_reason   	= "Ready to Load";

  }

  ##Attach Trip
#    try    {

      if ($is_allowed && $h_tripadd)
      {

#	$dbic->txn_do	  (sub	   {

	     if (defined($row_ca))
	     {
	       $row_tripcargo_updated = $row_ca->update($h_tripadd);
	     }
	     elsif ($h_tripadd)
	     {
	       $row_tripcargo_updated = $row_cargo->create_related
		 ($f_cargotrip,$h_tripadd);
	     }

	     ##--Change Status
	     if (defined($row_tripcargo_updated))
	     {
	       $self->change_status
		 ($h_cargo_status,$in_staffid,$str_cargo_status_reason);
	     }

#	   });##txn
	

      }
      elsif (!$is_allowed)
      {
	print "$fnx: Allowed:$is_allowed \n";
	$err_msg = "Attaching Trip is not allowed for this cargo.";
      }
      else
      {
	$err_msg = "Failed";
      }

#    }
#      catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_tripcargo_updated);

}


=head2 unload_trip($dbic,$o_trip,$in_staffid[,$o_address])

Return ($err_msg,$row_tripcargo_updated)

ToDO

=cut

sub unload_trip
{
  my $self		= shift;
  my $dbic		= shift;
  my $o_trip 		= shift;
  my $in_staffid	= shift;

  my $fnx		= "B/Cargo/attach_trip";
  my ($tripid,$row_cargo,$row_tripcargo_updated,$row_ca,$err_msg);
  my ($is_allowed,$h_tripadd,$f_cargotrip,$rs_cargotrip);
  $row_cargo 		= $self->dbrecord;
  $tripid       	= $o_trip->tripid;
  $is_allowed		= $self->edit_allowed;

  $h_tripadd->{tripid}	= $tripid;

  ##get Existing Trip Attachment
  {
    $f_cargotrip		= 'tripcargoes';
    $rs_cargotrip		= $row_cargo->search_related
      ($f_cargotrip,$h_tripadd);
    $row_ca			= $rs_cargotrip->first
      if (defined($rs_cargotrip));
  }

  ##Fill in TripID
  if($o_trip)
  {
    $tripid			= $o_trip->tripid;
    $h_tripadd->{tripid}	= $tripid;
    $h_tripadd->{update_userid} = $in_staffid;
    $h_tripadd->{valid}		= 'f';
  }

  ##Cargostatus
  my ($h_cargo_status,$str_cargo_status_reason);
  {
    $h_cargo_status->{stageid}	= "CANCEL";
    $str_cargo_status_reason   	= "UnLoading Cancelled";
  }


  ##Attach Trip
#    try    {

      if ($is_allowed && $h_tripadd && $row_ca)
      {

#	$dbic->txn_do	  (sub	   {

	     $row_tripcargo_updated = $row_ca->update($h_tripadd);

	     ##--Change Status
	     if (defined($row_tripcargo_updated))
	     {
	       ##A. cancel
	       $self->change_status
		 ($h_cargo_status,$in_staffid,$str_cargo_status_reason);

	       ##B. Back to Booked
	       $h_cargo_status->{stageid} = "BOOKED";
	       $self->change_status
		 ($h_cargo_status,$in_staffid,$str_cargo_status_reason);
	     }

#	   });

      }
      elsif (!$is_allowed)
      {
	print "$fnx: Allowed:$is_allowed \n";
	$err_msg = "Attaching Trip is not allowed for this cargo.";
      }
      else
      {
	$err_msg = "Failed";
      }



#    }
#      catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_tripcargo_updated);

}


=head1 PRIVATE Functions

Supporters Functions

=head2 _fill_src_guest($dbic,input_hash)

Fills Customer ID and ID-Type,City-Country-state in SRC

Returns  ($h_in,$o_address,$o_mover)

=cut

sub _fill_src_guest
{
  my $dbic	=	shift;
  my $h_in	=	shift;##This is filled.

  my $fnx = "B/cargo/_fill_dst_guest";
  my ($customerid,$id_type,$id);
  {
    $customerid		=	$h_in->{src_customerid};
    $id_type		=	$h_in->{src_idtype};
    $id			=	$h_in->{src_id};
  }
  my ($co,$st,$ci);
  {
    $co			=	$h_in->{src_country};
    $st			=	$h_in->{src_state};
    $ci			=	$h_in->{src_city};
  }

  my $o_mover;
  my ($addressid,$o_address);
  if($customerid)
  {
    $o_mover 	= Biz::Mover->new($dbic,$customerid);
    $addressid	= $o_mover->addressid	if($o_mover);

    $o_address	= Class::Address->new($dbic,$addressid)
      if($addressid);
  }


  if($o_mover && (!$id || !$id_type))
  {
    $id		=	$o_mover->src_id;
    $id_type	=	$o_mover->src_idtype;
    if ($id && $id_type)
    {
      $h_in->{src_id}		= $id;
      $h_in->{src_idtype}	= $id_type;
    }

  }

  if($o_mover && (!$co || !$st || $ci))
  {
    $co		=	$o_mover->src_country;
    $st		=	$o_mover->src_state;
    $ci		=	$o_mover->src_city;

    if ($co && $st && $ci)
    {
      $h_in->{src_country}	= $co;
      $h_in->{src_state}	= $st;
      $h_in->{src_city	}	= $ci;
    }
  }

  return ($h_in,$o_address,$o_mover);

}


=head2 _fill_dst_guest($dbic,input_hash)

Fills Customer ID and ID-Type,City-Country-state in DST

Returns  ($h_in,$o_address,$o_mover)

=cut

sub _fill_dst_guest
{
  my $dbic	=	shift;
  my $h_in	=	shift;##This is filled.

  my $fnx = "B/cargo/_fill_dst_guest";
  my ($customerid,$id_type,$id);
  {
    $customerid		=	$h_in->{dst_customerid};
    $id_type		=	$h_in->{dst_idtype};
    $id			=	$h_in->{dst_id};
  }
  my ($co,$st,$ci);
  {
    $co			=	$h_in->{dst_country};
    $st			=	$h_in->{dst_state};
    $ci			=	$h_in->{dst_city};
  }

  my $o_mover;
  my ($addressid,$o_address);
  if($customerid)
  {
    $o_mover 	= Biz::Mover->new($dbic,$customerid);
    $addressid	= $o_mover->addressid if($o_mover);

    $o_address	= Class::Address->new($dbic,$addressid)
      if($addressid);
  }

  if($o_mover && (!$id || !$id_type))
  {
    $id		=	$o_mover->src_id;
    $id_type	=	$o_mover->src_idtype;
    if ($id && $id_type)
    {
      $h_in->{dst_id}		= $id;
      $h_in->{dst_idtype}	= $id_type;
    }
  }

  if($o_mover && (!$co || !$st || $ci))
  {
    $co		=	$o_mover->src_country;
    $st		=	$o_mover->src_state;
    $ci		=	$o_mover->src_city;

    if ($co && $st && $ci)
    {
      $h_in->{dst_country}	= $co;
      $h_in->{dst_state}	= $st;
      $h_in->{dst_city	}	= $ci;
    }
  }

  return ($h_in,$o_address,$o_mover);

}


=head2 _prepare_cargo($h_in,$staffid)

Returns: $h_update

=cut

sub _prepare_cargo
{
  my $h_in		=	shift;
  my $in_staffid	=	shift;

  my $fnx = "b/cargo/_prepare_cargo";
  my $h_new;

  $h_new->{product_name}	= $h_in->{product_name}
    if ($h_in->{product_name}) ;

  ##Source
  $h_new->{src_customerid}	= $h_in->{src_customerid}
    if ($h_in->{src_customerid}) ;
  $h_new->{src_idtype}	= $h_in->{src_idtype}
    if ($h_in->{src_idtype}) ;
  $h_new->{src_id}		= $h_in->{src_id}
    if ($h_in->{src_id}) ;
  ##--SRC-City
  $h_new->{src_city}		= $h_in->{src_city}
    if ($h_in->{src_city});
  $h_new->{src_country}	= $h_in->{src_country}
    if ($h_in->{src_country});;
  $h_new->{src_state}	        = $h_in->{src_state}
    if ($h_in->{src_state});;

  ##Destination
  $h_new->{dst_customerid}	= $h_in->{dst_customerid}
    if ($h_in->{dst_customerid}) ;
  $h_new->{dst_idtype}	= $h_in->{dst_idtype}
    if ($h_in->{dst_idtype}) ;
  $h_new->{dst_id}		= $h_in->{dst_id}
    if ($h_in->{dst_id}) ;
  ##--DST-City
  $h_new->{dst_city}		= $h_in->{dst_city}
    if ($h_in->{dst_city});
  $h_new->{dst_country}	= $h_in->{dst_country}
    if ($h_in->{dst_country});;
  $h_new->{dst_state}	        = $h_in->{dst_state}
    if ($h_in->{dst_state});;

  ##GR ID
  $h_new->{transporter_invoiceid}		=
    $h_in->{transporter_invoiceid}    if ($h_in->{transporter_invoiceid}) ;

  ##Agent
  $h_new->{agentid}		= $h_in->{agentid}
    if ($h_in->{agentid}) ;

  ##Details Extra
  $h_new->{description}		= $h_in->{description}
    if ($h_in->{description}) ;
  $h_new->{comments}		= $h_in->{comments}
    if ($h_in->{comments}) ;



  $h_new->{shipping_weight}		= $h_in->{shipping_weight}
    if ($h_in->{shipping_weight}) ;
  $h_new->{cartage_price}		= $h_in->{cartage_price}
    if ($h_in->{cartage_price}) ;

  my ($is_cartage_paid,$is_payby_src,$is_door_delivery);
  $is_payby_src			= valid_boolean($h_in->{is_payby_src});
  $is_cartage_paid		= valid_boolean($h_in->{is_cartage_paid});
  $is_door_delivery		= valid_boolean($h_in->{is_door_delivery});
  print "$fnx ON/OFF:$is_cartage_paid/$is_payby_src \n";

  if ($is_payby_src)
  {
    $h_new->{is_payby_src}		= $is_payby_src;
    #print "$fnx PAYBY_SRC:$is_payby_src \n";
  }
  if ($is_door_delivery)
  {
    $h_new->{is_door_delivery}		= $is_door_delivery;
    #print "$fnx PAYBY_SRC:$is_payby_src \n";
  }

  if ($is_cartage_paid)
  {
    $h_new->{is_cartage_paid}		= $is_cartage_paid;
    #print "$fnx CARTAGE PAID:$is_cartage_paid \n";

  }

  $h_new->{update_userid}		= $in_staffid;

  return $h_new;

}

=head2 _prepare_cmore($row_cargo,$h_in)

Returns:  ($is_more,$h_new_more,$row_cmore)

Handles CargoMore Table Insert/Update Hash.

=cut

sub _prepare_cmore
{
  my $row_cargo = shift;
  my $h_in	= shift;

  my ($row_cmore,$h_new_more,$cargoid,$is_more);
  my $fnx = "B/cargo/_prepare_cmore";
  $cargoid = $row_cargo->get_column('cargoid');

  ##Fields available for Editing
  my $more_cargo =
    [qw(cargo_type cargo_sub_type
	supply_type supply_comments
	txn_type txn_type_outward txn_type_inward txn_type_comment
	cargonumber hsn insurance_info 
	cargo_value unit_details unit_number
	dimension_length dimension_width  dimension_thickness
	amt_hamali amt_green_tax amt_other
      )];
  #print "$fnx More:@$more_cargo \n";

  ##HAndle Total fields
  my $amt_total = 0;
  my $is_total_required;

  ##Fill the Fields
  foreach my $ix (@$more_cargo)
  {
    my $valx = $h_in->{$ix};
    if ($valx)
    {
      $h_new_more->{$ix} = $valx;
      $is_more = 1;
      #print "$fnx More $ix:$valx \n";
    }

    ##Handle The Total amount
    if ($ix eq 'amt_hamali'||$ix eq 'amt_green_tax' ||$ix eq 'amt_other')
    {
      $is_total_required =1;
      $amt_total += $valx;
      #print "$fnx Add Total:$ix \n";
    }
  }

  ##Fill in the Total HEre
  if ($is_total_required)
  {
    $h_new_more->{amt_grand_total} = $amt_total;
    #print "$fnx Fill Total:$amt_total \n";	
  }

  ##Find the CargoMore Record
  my $str_related_cmore = 'cargomore';
  $row_cmore	= $row_cargo->find_related
    ($str_related_cmore,{cargoid=>$cargoid});
  #print "$fnx Cmore:$row_cmore \n";

  return ($is_more,$h_new_more,$row_cmore);

}


=head2 _edit_cmore($row_cargo,$in_h[,$row_cargomore])

Update CargoMore

=cut

sub _edit_cmore
{
  my $row_cargo		=	shift;
  my $in_h_more		=	shift;
  my $row_cmore		=	shift;

  my $fx = "b/cargo/_edit_cmore";
  my $str_related_cmore = 'cargomore';
  my ($row_updated,$err_msg);

  if (!$row_cmore)
  {
    print "$fx: Create \n";
    $row_cargo->create_related($str_related_cmore,$in_h_more);
  }
  else
  {
    print "$fx: Update \n";
    $row_updated = $row_cmore->update($in_h_more);
  }

  return ($err_msg,$row_updated);

}


=back

=cut

1;
