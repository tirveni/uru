#!/usr/bin/perl -w
#
# Biz/Trip
#

package Biz::Trip;

use Moose;
use namespace::autoclean;

use TryCatch;
use JSON qw(decode_json encode_json);

use Class::Utils
  qw(selected_language unxss_pk unxss unxss_an valid_email valid_boolean
     chomp_date valid_date get_array_from_argument trim );

my ($str_class);
{
  $str_class = "Biz::Trip";
}

=pod
=head1 NAME

Biz::Trip - Utilities for handling Trip

=head1 SYNOPSIS

    use Biz::Trip;
    $oa = Biz::Trip->new( $dbic, $address );
    $row = $oa->dbrecord();

=head1 INHERITS

Biz::Trip

=cut

=head1 ADMINISTRIVIA

=over

=item B<new( $dbic, $tripid )>

Accept a cargo (either as a Cargo ID or as a DBIx::Class::Row
object and create a fresh Biz::Trip object from it. A DBIx obj
must be provided.

Return the Biz::Trip object, or undef if the Cargo couldn't be
found.

=cut

# Constructor
sub new
{
  my $class		= shift;
  my $dbic		= shift;
  my $arg_tripid	= shift;

  my $m = "C::Trip->new";

  my $row    = $arg_tripid;

  unless ( ref($arg_tripid) )
  {
    $arg_tripid = unxss($arg_tripid);
    if ($arg_tripid)
    {
      my $rs_cargo	= $dbic->resultset('Trip');
      $row	     	= $rs_cargo->find($arg_tripid);
    }
  }

  return (undef)
    unless $row;

  my $self = bless( {}, $class );
  $self->{trip_dbrecord}		= $row;

  return $self;

}

=item B<dbrecord()>

Return the DBIx::Class::Row object for this trip.

=cut
# Get the database object
sub dbrecord
{
  my
    $self = shift;
  return( $self->{trip_dbrecord} );
}

sub trip_dbrecord
{
  my
    $self = shift;
  return( $self->{trip_dbrecord} );
}

=head1 ACCESSORS

From Table Trip.

=head2 tripid

Returns: tripid

=cut

sub tripid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'tripid';

  try  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 valid

Returns: valid

=cut

sub valid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'valid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 start_date

Returns: start_date

=cut

sub start_date
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'start_date';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 vehicle_cost

Returns: vehicle_cost

=cut

sub vehicle_cost
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'vehicle_cost';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 vehicleid

Returns: vehicleid

=cut

sub vehicleid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'vehicleid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 trip_transporterid

Returns: trip_transporterid

=cut

sub trip_transporterid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'trip_transporterid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 created_at

Returns: created_at

=cut

sub created_at
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'created_at';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 update_userid

Returns: update_userid

=cut

sub update_userid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'update_userid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 src_country

Returns: src_country

=cut

sub src_country
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_country';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 src_state

Returns: src_state

=cut

sub src_state
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_state';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 src_city

Returns: src_city

=cut

sub src_city
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'src_city';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 dst_country

Returns: dst_country

=cut

sub dst_country
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_country';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 dst_state

Returns: dst_state

=cut

sub dst_state
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_state';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 dst_city

Returns: dst_city

=cut

sub dst_city
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'dst_city';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 driverid

Returns: driverid

=cut

sub driverid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'driverid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 finished_on

Returns: finished_on

=cut

sub finished_on
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'finished_on';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 end_date

Returns: end_date

=cut

sub end_date
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'end_date';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 reportid

Returns: reportid

=cut

sub reportid
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'reportid';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}

=head2 status

Returns: status

=cut

sub status
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'status';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}



=head2 is_trip_complete

Returns: is_trip_complete

Returns: 1 or 0. as per dbic

=cut

sub is_trip_complete
{
  my $self = shift;

  my ($x_value,$x_field,$err_msg);
  $x_field = 'is_trip_complete';

  try
  {
    my $row_trip = $self->dbrecord;
    $x_value = $row_trip->get_column($x_field);
  }  catch($err_msg){print "$str_class $err_msg \n"};

  return $x_value;

}


=head2 full_info($row_trip)

Returns: hash ref

=cut

sub full_info
{
  my $self	=	shift;
  my $dbic	=	shift;
  my $row_trip	=	shift;

  my $fn = "b/Trip/full_info";
  my $h_xd;

  if (defined($row_trip))
  {
    my %x_data	= $row_trip->get_columns;
    $h_xd	= Class::Utils::trim_hash_vals(\%x_data);

    ##Src
    {
      my ($co,$st,$ci,$o_city,$src_cname,$src_ccode);

      $co = $h_xd->{src_country};
      $st = $h_xd->{src_state};
      $ci = $h_xd->{src_city};
      $o_city = Class::City->new($dbic,$co,$st,$ci);
      if ($o_city)
      {
	($src_ccode,$src_cname) = $o_city->x_name();
	$h_xd->{src_cityname} = $src_cname;
	$h_xd->{src_citycode} = $src_ccode;
      }
    }

    ##DST
    {
      my ($co,$st,$ci,$o_city,$dst_cname,$dst_ccode);
      $co = $h_xd->{dst_country};
      $st = $h_xd->{dst_state};
      $ci = $h_xd->{dst_city};
      $o_city = Class::City->new($dbic,$co,$st,$ci);
      if ($o_city)
      {
	($dst_ccode,$dst_cname) = $o_city->x_name();
	$h_xd->{dst_cityname} = $dst_cname;
	$h_xd->{dst_citycode} = $dst_ccode;
      }
    }

  }

  return $h_xd;



}

=head1 OPERATIONS

Operations

=head2 create($dbic,$h_input)

Rseturns ($err_msg,$row_trip,$o_trip)

=cut

sub create
{
  my $dbic	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;

  my $fnx = "$str_class/create";
  my ($o_cargo,$rs_trip,$row_trip,$vehicleid,$start_date,$end_date,
      $valid,$h_new,$err_msg,$ownerid,$is_date_increasing);
  my ($src_country,$src_state,$src_city,);

  print "$fnx Begin \n";
  ##--Default Stuff
  my $default_trip_transporterid	= '';
  $valid		= 't';

  ##Important Fields
  {
    $src_city		= $h_in->{src_city};
    $src_state		= $h_in->{src_state};
    $src_country	= $h_in->{src_country};

    $vehicleid	= $h_in->{vehicleid};
    $ownerid	= $h_in->{ownerid};
    $start_date	= valid_date($h_in->{start_date});
    $end_date	= valid_date($h_in->{end_date});
  }

  ##Date Range Validation
  {
    $is_date_increasing = 1;
    if ($start_date eq $end_date)
    {
      $is_date_increasing = 1;
    }
    elsif ($start_date && $end_date)
    {
      my $date_range_ok =
	Class::Utils::dates_increasing($start_date,$end_date);
      $is_date_increasing = undef
	if ($date_range_ok < 1);
    }
  }
  print "$fnx Date Range: $is_date_increasing \n";

  ##Req Fields Validation
  if($src_country && $src_state && $src_city&& $start_date)
  {
    $h_new->{update_userid}	= $in_userid;

    $h_new->{start_date}	= $start_date;
    $h_new->{end_date}		= $end_date if($end_date);

    ##Source
    $h_new->{src_city}		= $src_city;
    $h_new->{src_country}	= $src_country;
    $h_new->{src_state}	        = $src_state;


    print "$fnx Check Vehicle \n";
    my $o_vehicle;
    if($vehicleid)
    {
      print "$fnx Check Vehicle:$vehicleid by:$in_userid \n";
      $o_vehicle = _new_vehicle($dbic,$h_in,$in_userid);
      $h_new->{vehicleid}		= trim($vehicleid)
	if($o_vehicle);
    }
    print "$fnx Check Vehicle:$o_vehicle \n";

    $h_new->{trip_transporterid} =
      $h_in->{trip_transporterid}	if($h_in->{trip_transporterid}) ;
    $h_new->{driverid}		=
      $h_in->{driverid}	     		if($h_in->{driverid}) ;

    ##Destination
    $h_new->{dst_city}		= $h_in->{dst_city} if($h_in->{dst_city});
    $h_new->{dst_country}	= $h_in->{dst_country}
      if($h_in->{dst_country});;
    $h_new->{dst_state}	        = $h_in->{dst_state}
      if($h_in->{dst_state});;
  }
  else
  {
    $err_msg = "Required Fields are missing.";
  }

  ##--Now create
  try
  {
    if ($h_new && defined($dbic) && $in_userid && $is_date_increasing)
    {
      $rs_trip		= $dbic->resultset('Trip');
      $row_trip		= $rs_trip->create($h_new)
	if (defined($rs_trip));
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  if(defined($row_trip))
  {
    $o_cargo = Biz::Trip->new($dbic,$row_trip);
  }

  return ($err_msg,$row_trip,$o_cargo);

}

=head2 _new_vehicle($dbic,{vehicleid,ownerid},userid)

Adds a vehicle if not added.

=cut

sub _new_vehicle
{
  my $dbic	=	shift;
  my $h_in	=	shift;
  my $userid	=	shift;

  my $fnx = "b/trip/_new_vehicle";
  my ($o_vehicle,$row_vehicle,$vehicleid,$ownerid,$in_ownerid,$err_msg);
  $vehicleid	= unxss_pk($h_in->{vehicleid});
  $in_ownerid	= $h_in->{ownerid};

  print "$fnx: $vehicleid, for: $in_ownerid/ by:$userid \n";

  $o_vehicle = Biz::Vehicle->new($dbic,$vehicleid)
    if($vehicleid);

  my ($o_appuser);
  if($ownerid)
  {
    $o_appuser = Class::Appuser->new($dbic,$ownerid);
    $ownerid = $o_appuser->userid if($o_appuser);
  }

  if(!$o_vehicle && $vehicleid && $userid)
  {
    print "$fnx: Create:$vehicleid \n";

    my $h_newv;
    $h_newv->{vehicleid}	= $vehicleid;
    $h_newv->{ownerid}	 	= $ownerid;
    ($err_msg,$row_vehicle,$o_vehicle) = 
      Biz::Vehicle::create($dbic,$h_newv,$userid);
  }

  return ($err_msg,$row_vehicle,$o_vehicle) ;

}

=head2 edit_allowed()

Returns $is_allowed

1 OR undefined.

=cut

sub edit_allowed
{
  my $self	= shift;

  my $mx = "B/trip/edit_allowed";
  my $is_trip_complete	= $self->is_trip_complete;
  my $is_allowed;
  print "$mx is_trip_complete:$is_trip_complete \n";
  if($is_trip_complete eq 'f' || $is_trip_complete == 0)
  {
    $is_allowed = 1;
  }
  else
  {
    $is_allowed = undef;
  }
  print "$mx is_trip_complete:$is_allowed \n";

  return $is_allowed;

}


=head2 edit($h_input,$userid,$reason)

Rseturns ($err_msg,$row_trip_updated);

=cut

sub edit
{
  my $self	= shift;
  my $h_in	= shift;
  my $in_userid	= shift;
  my $reason	= shift;

  my $fnx		= "$str_class/edit";
  my $row	= $self->dbrecord;
  my $dbic = $row->result_source->schema;

  print "$fnx Begin \n";

  my ($row_trip,$row_trip_updated,$is_valid,
      $h_new,$h_new_more,$err_msg);

  my ($cargo_trip_transporterid,$txnid);
  my $is_allowed = $self->edit_allowed;

  print "$fnx Begin \n";

  {

    $h_new->{start_date}	= $h_in->{start_date}
      if($h_in->{start_date}) ;
    $h_new->{end_date}		= $h_in->{end_date}
      if($h_in->{end_date}) ;


    ##Source
    my $vehicleid	= trim($h_in->{vehicleid});
    my $o_vehicle;
    if($vehicleid)
    {
      print "$fnx Check Vehicle:$vehicleid by:$in_userid \n";
      $o_vehicle = _new_vehicle($dbic,$h_in,$in_userid);
      $h_new->{vehicleid}		= trim($vehicleid)
	if($o_vehicle);
    }
    print "$fnx Check Vehicle:$o_vehicle \n";

    $h_new->{vehicle_cost}	= $h_in->{vehicle_cost}
      if($h_in->{vehicle_cost}) ;
    $h_new->{driverid}		= $h_in->{driverid}
      if($h_in->{driverid}) ;
    $h_new->{trip_transporterid}	= $h_in->{trip_transporterid}
      if($h_in->{trip_transporterid}) ;
    $h_new->{is_trip_complete}		= $h_in->{is_trip_complete}
      if($h_in->{is_trip_complete}) ;
    $h_new->{finished_on}	= $h_in->{finished_on}
      if($h_in->{finished_on}) ;

    $h_new->{valid}	= $h_in->{valid}
      if($h_in->{valid}) ;



    ##--SRC-City
    $h_new->{src_city}		= $h_in->{src_city}
      if($h_in->{src_city});
    $h_new->{src_country}	= $h_in->{src_country}
      if($h_in->{src_country});;
    $h_new->{src_state}	        = $h_in->{src_state}
      if($h_in->{src_state});;

    ##Destination
    ##--DST-City
    $h_new->{dst_city}		= $h_in->{dst_city}
      if($h_in->{dst_city});
    $h_new->{dst_country}	= $h_in->{dst_country}
      if($h_in->{dst_country});;
    $h_new->{dst_state}	        = $h_in->{dst_state}
      if($h_in->{dst_state});;

    $h_new->{update_userid}		= $in_userid;

  }

  ##--Now create
  try  {

    if ($h_new && $is_allowed  && $in_userid)
    {
      $row_trip		= $self->dbrecord;
      $row_trip_updated	= $row_trip->update($h_new)
	if (defined($row_trip));
    }
    elsif(!$is_allowed)
    {
      print "$fnx: Allowed:$is_allowed \n";
      $err_msg = "Edit is not allowed for this trip.";
    }

  }
    catch($err_msg){print "$fnx $err_msg \n"};

  return ($err_msg,$row_trip_updated);

}


=head2 cargo_result($dbic)

Gets all the VTripCargo for a Trip.

Enabled or Disabled.

=cut

sub cargo_result
{
  my $self	=	shift;
  my $dbic	=	shift;

  my $mx = "B/Cargo/cargo_result";
  my ($rs_cargo,$rs_vtcargo,$tripid,$t_vtcargo);
  my @list_cargoid;
  my $h_search;
  {
    $t_vtcargo	=      	$dbic->resultset('VTripcargo');
    $tripid	=	$self->tripid;
  }

  ##-- A
  ##Search on TripCargo
  $h_search->{tripid} = $tripid;

  ##Get TripCargo
  if ($tripid && defined($t_vtcargo))
  {
    $rs_vtcargo = $t_vtcargo->search($h_search);
  }

  return $rs_vtcargo;
}

=head2 list_cargo($dbic[,is_valid])

Returns: $rs_VTripcargo

Gets only the Active ones, unless specified.

=cut

sub list_cargo
{
  my $self	=	shift;
  my $dbic	=	shift;
  my $is_valid	=	shift || 't';

  my $mx = "B/Cargo/list_cargo";
  my ($h_search,$rs_vtcargo);
  my @list_cargoid;

  $is_valid = Class::Utils::valid_boolean($is_valid);
  print "$mx Valid:$is_valid \n";
  ##If Valid
  if ($is_valid eq 'f')
  {
    $h_search->{'me.tripcargo_valid'} = 'f';
    print "$mx Check InValid \n";
  }
  else
  {
    $h_search->{'me.tripcargo_valid'} = 't';
    print "$mx Check Valid \n";
  }

  ##Get TripCargo
  my $t_vtcargo = $self->cargo_result($dbic);
  if (defined($t_vtcargo))
  {
    $rs_vtcargo = $t_vtcargo->search($h_search);
  }


  print "$mx VTC: $rs_vtcargo \n";

  return $rs_vtcargo;

}

=head2 manage_cargo($dbic,$h_in)

Returns: ($errors,$rs_vtcargo)

Adds and Removes Cargo.

h_in: {list_new_cargoids,list_remove_cargoids,userid}

ToDo: check cargo status: if intransit, then do not add.

=cut

sub manage_cargo
{
  my $self	=	shift;
  my $dbic	=	shift;
  my $h_in	=	shift;

  my $mx = "B/trip/manage_cargo";
  my ($rs_vtcargo,$errors,$is_cargo_blocked);

  $is_cargo_blocked = $self->cargo_block();
  print "$mx Blocked:$is_cargo_blocked  \n";
  if($is_cargo_blocked)
  {
    my $xmsg = "Trip is blocked";
    push(@$errors,$xmsg);
    return ($errors,$rs_vtcargo);
  }
  print "$mx Move Ahead.  \n";

  my ($list_new_cargoids,$list_remove_cargoids,$in_userid);
  {
    $list_new_cargoids		= $h_in->{list_new_cargoids};
    $list_remove_cargoids	= $h_in->{list_remove_cargoids};
    $in_userid			= $h_in->{userid};
  }

  ##1. Add Cargo
  if ($list_new_cargoids)
  {
    foreach my $cid(@$list_new_cargoids)
    {
      my $o_cargo = Biz::Cargo->new($dbic,$cid);
      $o_cargo->attach_trip($dbic,$self,$in_userid);
    }
  }

  ##2. Remove Cargo
  if ($list_remove_cargoids)
  {
    foreach my $cid(@$list_remove_cargoids)
    {
      my $o_cargo = Biz::Cargo->new($dbic,$cid);
      $o_cargo->unload_trip($dbic,$self,$in_userid);
    }
  }

  ##3. Get Fresh Status of Cargo
  $rs_vtcargo = $self->list_cargo($dbic);


  return ($errors,$rs_vtcargo);

}


=head1 CARGO BLOCK

Block: Loading/UnLoading of Cargo.

=head2 cargo_block([reportid])

RW

Returns Blocked Status

=cut

sub cargo_block
{
  my $self	=	shift;
  my $reportid	=	shift;

  my $row_trip		=	$self->dbrecord;
  my ($h_tr,$err_msg,$new_status,$row_updated,$is_blocked);

  if($reportid)
  {
    $h_tr->{status}	= 'BLOCK';
    $h_tr->{reportid}	= $reportid;
    $row_updated	= $row_trip->update($h_tr);
    $new_status		= trim($row_updated->get_column('status'));
    $is_blocked		= 1;
  }
  else
  {
    $new_status = trim($row_trip->get_column('status'));
    if($new_status eq 'BLOCK')
    {
      $is_blocked	= 1;
    }
  }

  $new_status = trim($new_status);

  return ($err_msg,$row_updated,$is_blocked);

}


=head2 cargo_unblock(reason)

RW

Returns Blocked Status

=cut

sub cargo_unblock
{
  my $self	=	shift;
  my $reason	=	shift;

  my $row_trip		=	$self->dbrecord;
  my ($h_tr,$err_msg,$new_status,$row_updated,$is_blocked);

  if($reason)
  {
    $h_tr->{status}	= 'UNBLOCK';
    $h_tr->{reportid}	= undef;
    $row_updated	= $row_trip->update($h_tr);
    $new_status		= trim($row_updated->get_column('status'));
    $is_blocked		= undef;

  }
  else
  {
    $new_status = trim($row_trip->get_column('status'));
    if($new_status eq 'BLOCK')
    {
      $is_blocked = 1;
    }
  }

  $new_status = trim($new_status);

  return ($err_msg,$row_updated,$is_blocked);

}


=head1 REPORTS

Handle Trip Reports


=head2 make_report($dbic,$userid)

Return ($err_msg,$row_rt);

=cut

sub make_report
{
  my $self	=	shift;
  my $dbic	=	shift;
  my $in_userid	=	shift;

  my $mx = "B/Trip/make_report";
  my ($t_tr_report,$row_trip,$err_msg,$row_report);
  $row_trip		=	$self->dbrecord;

  my ($h_trip_info,$json_trip_info,$h_new_tr,$rs_vt_cargo);
  {
    $h_trip_info		= $self->full_info($dbic,$row_trip);

    $json_trip_info	= encode_json($h_trip_info)
      if ($h_trip_info);

    $h_new_tr->{trip_info}		= $json_trip_info;
    $h_new_tr->{update_userid}	= $in_userid;
    $h_new_tr->{tripid}		= $self->tripid;


    ##View_TripCargo
    $rs_vt_cargo = $self->list_cargo($dbic);
  }

  my ($row_rt,$reportid,$rs_rt_cargo,$rs_tcargo);
  my @list_cargo;

  ##--2. Cargo of The Trip
  if(defined($rs_vt_cargo))
  {
    print "$mx RS-VT-Cargo:$rs_vt_cargo \n";
    while (my $row = $rs_vt_cargo->next() )
    {
      my ($cargoid,$o_cargo,$h_xd);
      $cargoid       	= $row->get_column('cargoid');
      $o_cargo		= Biz::Cargo->new($dbic,$cargoid);
      $h_xd		= $o_cargo->full_info($dbic);

      #my %x_data	= $row->get_columns;
      #print "$mx $row \n";
      #my $h_xd			= Class::Utils::trim_hash_vals(\%x_data);

      my $json_cargo_info	= encode_json($h_xd);
      my $h_cx;
      $h_cx->{json_info}	= $json_cargo_info;
      $h_cx->{cargoid}		= $h_xd->{cargoid};
      $h_cx->{stageid}		= $h_cx->{stageid};

      push(@list_cargo,$h_cx);
    }
  }


  ##3. Fill in Tripinfo,CargoInfo
  try {
	 $row_report = _imake_report
	   ($dbic,$row_trip,$h_new_tr,\@list_cargo);
  }
    catch($err_msg)
    {
      print "$mx $err_msg \n";
    };

  return ($err_msg,$row_report);
}

=head2 _imake_report($dbic,$row_trip,$h_new_tr,$list_cargo)

Returns: Row_ReportTrip

=cut

sub _imake_report
{
  my $dbic		=	shift;
  my $row_trip		=	shift;
  my $in_h		=	shift;
  my $list_cargo	=	shift;

  my ($row_rt,$reportid,$row_report);
  my $str_table_rtc		= 'reporttripcargoes';
  my $str_table_rt		= 'reporttrips';

  my $t_report			= $dbic->resultset('Report');
  my $t_tr_report		= $dbic->resultset('Reporttrip');

  my ($h_report_new,$h_tr_new,$report_status_cancel,$report_status_ok);
  {
    $report_status_ok		   = 'COMPLETE';
    $report_status_cancel	   = 'CANCEL';
    $h_report_new->{update_userid} = $in_h->{update_userid};
    $h_report_new->{tripid}	   = $in_h->{tripid};
    $h_report_new->{reason}	   = $in_h->{reason};	
    $h_report_new->{status}	   = $report_status_cancel;
  }
  {
    $h_tr_new->{update_userid}	= $in_h->{update_userid};
    $h_tr_new->{tripid}	   	= $in_h->{tripid};
  }

  ##1 Create Report with Cancelled Status
  $row_report   = $t_report->create($h_report_new);

  $dbic->txn_do
    (sub
     {
       ##2. Create ReportTrip
       $row_rt	= $row_report->create_related
	 ($str_table_rt,$in_h);
       $reportid	= $row_report->reportid;

       my $count = 1;

       ##3. Create ReportTripCargo
       foreach my $h_cx (@$list_cargo )
       {
	 my $h_cn;
	 $h_cn->{cargo_info}	= $h_cx->{json_info};
	 $h_cn->{cargoid}	= $h_cx->{cargoid};
	 $h_cn->{stageid}	= $h_cx->{stageid};
	 $h_cn->{entryid}	= $count;
	 $count++;
	 my $row_rtc = $row_rt->create_related($str_table_rtc,$h_cn);
       }

       ##4. Update RowTrip With ReportID
       $row_trip->update({reportid=>$reportid});

       ##5. Update Row-Report with Complete.
       $row_report->update({status=>$report_status_ok});
     });

  return ($row_report);

}


=head2 get_report($dbic,$o_cargo)

Return ($h_trip_info,$h_tr_stage_info,\@list);

=cut

sub get_report
{
  my $self	=	shift;
  my $dbic	=	shift;
  my $o_cargo	=	shift;

  my $mx = "biz/trip/get_report";
  my ($tripid,$t_tr_report,$row_tr_report,$rs_tr_cargo,$reportid);
  my $cargoid;
  my @list_cargoid;
  my $h_search;
  {
    $t_tr_report		=      	$dbic->resultset('Reporttrip');

    $tripid	=	$self->tripid;
    $reportid	=	$self->reportid;
    $cargoid	=	$o_cargo->cargoid if($o_cargo);
  }

  ##Get Existing
  if(defined($t_tr_report) && $reportid)
  {
    $row_tr_report = $t_tr_report->find
      ({
	tripid		=> $tripid,
	reportid	=> $reportid,
       });

    my $str_rtc = "reporttripcargoes";
    $rs_tr_cargo = $row_tr_report->search_related($str_rtc,undef);

    print "$mx CID:$cargoid \n";
    if($cargoid)
    {
      $rs_tr_cargo = $rs_tr_cargo->search({cargoid=>$cargoid});

    }
  }

  print "$mx ROW TI:$row_tr_report \n";

  my ($report_date,$report_userid);
  my ($json_trip_info,$json_trip_stage_info,
      $h_trip_info,$h_tr_stage_info);
  {
    $report_date		= $row_tr_report->created_at;
    $report_userid		=
      $row_tr_report->get_column('update_userid');

    $json_trip_info		= $row_tr_report->trip_info();
    #print "$mx J-TI:$json_trip_info \n";
    $h_trip_info		= decode_json($json_trip_info)
      if($json_trip_info);
    $h_trip_info->{report_date}	       = $report_date;
    $h_trip_info->{report_userid}      = $report_userid;

    #print "$mx H-TI:$h_trip_info \n";

    $json_trip_stage_info	= $row_tr_report->trip_info();
    $h_tr_stage_info		= decode_json($json_trip_stage_info)
      if($json_trip_stage_info);
  }

  my @list_cargo;
  while (my $row_trc = $rs_tr_cargo->next() )
  {
    my ($json_cargo_info,$h_info);
    $json_cargo_info	= $row_trc->cargo_info;
    #print "$mx J-CA:$json_cargo_info \n";

    $h_info		= decode_json($json_cargo_info)
      if($json_cargo_info);
    print "$mx H-CA: $h_info \n";

    push(@list_cargo,$h_info);
  }
  print "$mx Cargoes: @list_cargo \n";


  return ($h_trip_info,$h_tr_stage_info,\@list_cargo);

}

=head1 PVT Functions

Supporters

=head2 _fill_vehicle($dbic,input_hash)

Fills trip_transporterid with ownerid of vehicle

=cut

sub _fill_vehicle
{
  my $dbic	=	shift;
  my $h_in	=	shift;##This is filled.

  my $vehicleid	=	$h_in->{vehicleid};
  my $ownerid	=	$h_in->{trip_transporterid};

  my $o_vehicle;
  if(!$ownerid)
  {
    $o_vehicle = Biz::Vehicle->new($dbic,$vehicleid);
  }

  if($o_vehicle)
  {
    $ownerid	=	$o_vehicle->ownerid;
    $h_in->{trip_transporterid} = $ownerid
      if($ownerid);
  }

}


=back

=cut

1;
