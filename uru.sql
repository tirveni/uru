
\c uru;

\echo *** DROP Tables......

DROP TABLE IF EXISTS CustomerInfo	CASCADE; 
DROP TABLE IF EXISTS ReportTripCargo 	CASCADE;	
DROP TABLE IF EXISTS ReportTrip 	CASCADE;	
DROP TABLE IF EXISTS Report	 	CASCADE;	
DROP TABLE IF EXISTS TripCargo 		CASCADE;
DROP TABLE IF EXISTS CargoAddress 	CASCADE;
DROP TABLE IF EXISTS CargoMore 		CASCADE;
DROP TABLE IF EXISTS Cargo 		CASCADE;

DROP TABLE IF EXISTS TripStage 		CASCADE;
DROP TABLE IF EXISTS Trip 		CASCADE;

DROP TABLE IF EXISTS Vehicle 		CASCADE;


\echo *** CREATE Tables......


CREATE TABLE vehicle
(
	vehicleid		char(24) PRIMARY KEY,
	name			text,
	valid			boolean,
        ownerid          	text REFERENCES 
                AppUser  ON UPDATE CASCADE,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          	text REFERENCES 
                AppUser  ON UPDATE CASCADE

);


CREATE TABLE Trip
(
	tripid			SERIAL PRIMARY KEY,
	start_date		date,
	end_date		date,	-- Prospective Date of Trip Ending

	reportid		integer,
	status			char(24),

	--Src country
	src_country		char(3),
	src_state		char(3),
	src_city		char(20),
	FOREIGN KEY(src_country,src_state,src_city) REFERENCES
		City ON UPDATE CASCADE,

	--DST		
	dst_country		char(3),
	dst_state		char(3),
	dst_city		char(20),
	FOREIGN KEY(dst_country,dst_state,dst_city) REFERENCES
		City ON UPDATE CASCADE,

	-- vehicle		
	vehicle_cost		numeric(8,2),
	vehicleid		char(24)	REFERENCES
		Vehicle ON UPDATE CASCADE,
        driverid          	text REFERENCES 
                AppUser  ON UPDATE CASCADE,

	trip_transporterid	text,	--OwnerID

	is_trip_complete	boolean default 'f',	
	finished_on		timestamp with time zone,	
	valid			boolean	default 't',	

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE


);
-- Trip Cost can be added to Transaction through TRIPID

-- TripStage
CREATE TABLE TripStage
(
	tripid			int	REFERENCES
		Trip ON UPDATE CASCADE,

	stageid			smallint,
	PRIMARY KEY(tripid,stageid),
	valid			boolean,

	-- Prospective
	start_date		date,
	end_date		date,
	is_trip_complete	boolean default 'f',	

	--Reality
        began      	timestamp 
                with time zone, 
        reached      	timestamp 
                with time zone, 

	src_country		char(3),
	src_state		char(3),
	src_city		char(20),
	FOREIGN KEY(src_country,src_state,src_city) REFERENCES
		City ON UPDATE CASCADE,

	dst_country		char(3),
	dst_state		char(3),
	dst_city		char(20),
	FOREIGN KEY(dst_country,dst_state,dst_city) REFERENCES
		City ON UPDATE CASCADE,

	stage_transporterid	text,	
	vehicle_cost		numeric(8,2),
        driverid          	text 		REFERENCES 
                AppUser  ON UPDATE CASCADE,
	vehicleid		char(24)	REFERENCES
		Vehicle ON UPDATE CASCADE,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE

);


-- Get List of Trips.
DROP VIEW IF EXISTS v_atrip;

CREATE VIEW v_atrip  AS
	SELECT 	tr.tripid,tr.driverid as trip_driver,tr.vehicleid as trip_vehicle,
		tr.start_date,tr.src_city as source_city ,tr.dst_city as destination_city,
		st.stageid,st.driverid as stage_driver,st.vehicleid as stage_vehicle,
		st.src_city as source_city_stage,st.dst_city as destination_city_stage
		FROM trip tr LEFT OUTER JOIN tripstage st 
		ON st.tripid=tr.tripid;



CREATE TABLE Cargo
(
	cargoid			SERIAL PRIMARY KEY,
	valid			boolean,

	product_name		text,
	description		text,
	comments		text,


	stageid			char(24) default 'BOOKED',	
	--Cargo Stage: BOOKED,TRANSIT,DELIVERED
        shipping_weight         smallint,

	--Soruce Customer,GST and City	
        src_customerid          text REFERENCES 
                AppUser  ON UPDATE CASCADE,
	src_idtype		char(32),
	src_id			char(32),	
	src_country		char(3),
	src_state		char(3),
	src_city		char(20),
	FOREIGN KEY(src_country,src_state,src_city) REFERENCES
		City ON UPDATE CASCADE,

	--Destination Customer,GST and City	
        dst_customerid          text REFERENCES 
                AppUser  ON UPDATE CASCADE,
	dst_idtype		char(32),
	dst_id			char(32),	
	dst_country		char(3),
	dst_state		char(3),
	dst_city		char(20),
	FOREIGN KEY(dst_country,dst_state,dst_city) REFERENCES
		City ON UPDATE CASCADE,

	is_payby_src		boolean	DEFAULT 'f',
	--Payment by Src Customer.

	--Cartage Info: payment for Transportation
	is_cartage_paid		boolean DEFAULT 'f',
	cartage_price			numeric(12,2) 
		CONSTRAINT positive_price CHECK (cartage_price >= 0),
	is_door_delivery	boolean,
	transporter_invoiceid	text,
	transactionid		bigint,	--Internal
	-- Create A Txn of Debit for the Customer and fillin the CargID in the TXN

        agentid          	text REFERENCES 
                AppUser  ON UPDATE CASCADE,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          	text REFERENCES 
                AppUser  ON UPDATE CASCADE


);

--India Specific GST Info
CREATE TABLE CargoMore
(
	cargoid			int 	REFERENCES
		Cargo ON UPDATE CASCADE,
		PRIMARY KEY(cargoid),

	cargo_type		char(32),
	cargo_sub_type		char(32),	

	supply_type		char(32),	-- supply: Sale,Transfer,Barter/Exchange
	supply_comments		text,

	txn_type		char(32),	--Outward/InWard

	txn_type_outward	char(32),
	--Outward: Supply,Export,JobWork,SKDorCKD,RecipientNotKnown,ForOwnUse,ExhibitionOrFair,LineSale,Others

	txn_type_inward		char(32),
	--Inward:  Supply,Import,JobWorkReturn,SKDorCKD,SalesReturn,ForOwnUse,ExhibitionOrFair,Others

	txn_type_comment	text,

	cargonumber		char(32),	-- Use as Customized Number(Bilti Number)
	HSN			char(32),
	insurance_info		text,


	cargo_value		numeric(12,2),	--Cost of the Cargo
	unit_details		text,		--Unit Type
	unit_number		smallint,	--Quantity		

        -- dimesions in mm
        dimension_length        smallint,
        dimension_width         smallint,
        dimension_thickness     smallint,

	-- Req by PKM	
	amt_hamali		numeric(12,2),
	amt_green_tax		numeric(12,2),
	amt_other		numeric(12,2),
	amt_grand_total		numeric(12,2),

        -- Weight in Gms
        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE

);

-- Manage Address Change.
CREATE TABLE CargoAddress
(
	cargoid			int 	REFERENCES
		Cargo ON UPDATE CASCADE,
	addressid		int	REFERENCES
		Address ON UPDATE CASCADE,
	PRIMARY KEY(cargoid,addressid),

	is_src			boolean,
	valid			boolean,

	comments		text,
        customerid          text REFERENCES 
                AppUser  ON UPDATE CASCADE,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE
	
);


--TripCargo
CREATE TABLE TripCargo
(
	tripid			int REFERENCES
		TRIP ON UPDATE CASCADE,
	cargoid			int REFERENCES
		Cargo ON UPDATE CASCADE,
	PRIMARY KEY(tripid,cargoid),

	stageid			smallint,-- TripStage(tripid,stageid),		

	valid			boolean,

		-- Delivered Info
	addressid		int,
	message			text,
	is_delivered		boolean,
	delivered_time		timestamp with time zone,	
	delivered_to		text,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE

);

DROP TABLE CargoStage;
CREATE TABLE CargoStage
(
	cargoid			int REFERENCES
		Cargo ON UPDATE CASCADE,
	stageid			char(24),
	reason			text,
	country			char(3),
	state			char(3),
	city			char(20),

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE,

	PRIMARY KEY(cargoid,created_at)	
);



DROP VIEW v_cargo;
CREATE VIEW v_cargo 
	AS SELECT 
		cr.cargoid,cr.valid,cr.src_customerid,cr.dst_customerid,cr.product_name,
		tr.tripid,tr.valid as is_ontheway,tr.is_delivered,tr.delivered_time 
	FROM 	cargo cr LEFT OUTER JOIN tripcargo tr 
	ON 	cr.cargoid=tr.cargoid 
	WHERE 	cr.valid='t';



CREATE TABLE CustomerInfo
(
	userid			text REFERENCES	
                AppUser  ON UPDATE CASCADE,
			
	src_idtype		char(32),
	src_id			char(32),	

	src_country		char(3),
	src_state		char(3),
	src_city		char(20),
	FOREIGN KEY(src_country,src_state,src_city) REFERENCES
		City ON UPDATE CASCADE,
	addressid		int,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE,

	PRIMARY KEY(userid)
		
);


DROP VIEW v_tripcargo;
CREATE VIEW v_tripcargo AS 
	SELECT c.*,tc.tripid,
		tc.valid as tripcargo_valid 
	FROM tripcargo tc,cargo c 
	WHERE c.cargoid=tc.cargoid ;


CREATE TABLE Report
(
	reportid		SERIAL PRIMARY KEY,
	status			char(24),

	tripid			int,	-- Optional
	cargoid			int,	-- Optional
	tripstage		int,	-- Optional
	customerid		text,	-- Optional

	reason			text,	

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE
);	

CREATE TABLE ReportTrip
(
	reportid		int	REFERENCES
		Report ON UPDATE CASCADE,
	tripid			int REFERENCES
		TRIP ON UPDATE CASCADE,
	PRIMARY KEY(reportid,tripid),	

	stageid			int,
	trip_info		JSON,	
	trip_stage_info		JSON,

        created_at      	timestamp 
                with time zone default (now() at time zone 'utc')   ,
        update_userid          text REFERENCES 
                AppUser  ON UPDATE CASCADE
);	


CREATE TABLE ReportTripCargo
(
	tripid			int,
	reportid		int,
	FOREIGN KEY(reportid,tripid) REFERENCES 	ReportTrip,

	entryid			int,
	PRIMARY KEY(reportid,tripid,entryid),

	cargoid			int REFERENCES
		CARGO ON UPDATE CASCADE,
		
	stageid			int,

	cargo_info		JSON


);	
