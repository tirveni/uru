/*
	 2015-12-25
	 ender.js
	 
	 Use it in the footer. Used for Ajax generally or end of file stuff.
	 
    This file is part of Taipan.
    Copyright (C) 2016,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
/* init after the page dom to search through ajax. */
$("#sin_city").easyAutocomplete(city_options);
$("#src_city").easyAutocomplete(src_city_options);
$("#dst_city").easyAutocomplete(dst_city_options);
$("#currency").easyAutocomplete(currency_options);

