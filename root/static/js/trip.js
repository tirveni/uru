/*
* Trip.js for URU..

Authors: Tirveni Yadav

Uru (from Taipan) is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Uru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with TAIPAN.  If not, see <http://www.gnu.org/licenses/>.
<GNU AGPL: http://www.gnu.org/licenses/agpl-3.0.html>

*
*/

var $url_trip_list 	= '/biz/list/trips';
var $url_trip_info	= '/biz/trip';
var $url_trip_cargo	= '/biz/trip/cargo';


function ftr_add_trip_btn()
{

		var $btn_add_trip =	"<div class='row><div class='col l12 m12 s12'>"
		 								+" <btn class='btn orange' onClick=uru_spa"
                                           + "('" + "add_trip" + "'" +");>"
                              + " + Add New Trip </btn></div></div> ";

		return $btn_add_trip;
}

/*
* FX: fxu_list_movers: Display Cargo
* @params: incount,inpage,indate
*/

function fxu_list_trips(inpage)
{

  var $fnx =	$url_trip_list;
        
  var $in_rest  = $url_trip_list;
  var $in_page  = inpage || 1;
  var $in_count = 10;
  var $in_date;                
  var $h_search = {};

  var $in_m = $fnx + "/" + $in_page;
  var $rest_pg_txt = "#rest_pagination_text";

  var $loc_msg_wait 		= "Please wait, while Trips are begin fetched....";
  var $loc_msg_ok 	 	= "List of Trips received.";
  
  progress_append($div_xpg,$loc_msg_wait);

  
  $.ajax( 
            { 
           url: 			$in_m ,
           type: 			"GET",
           dataType: 	"json",
           timeout: 		$c_ajax_timeout_milisec,
           headers: 		$h_search,
           error: function (xhr,status,error) 
           {

           				$($div_xpg).empty();				
           				$btn_add = ftr_add_trip_btn();
           				$($div_xpg).append($btn_add);

							bx_toast_empty();
							
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);

							
           },
           success: function( data )
           {
       				
           	      bx_toast_empty();
                 	Materialize.toast($loc_msg_ok, 3000);           	      
                  display_trips(data);
           },
                                       //GetJSON Function_data
        });
        //ajax

}// Function


/*
* FX: display_cargo: Display Cargo 
* @params: data
*/
function display_trips(data)
{
        var items = [];
                       
        var $idx_first 		= data.idx_first;
        var $idx_last 		= data.idx_last;
        var $idx_str 		= $idx_first + "-" + $idx_last;
        var $page_number	= data.page_number;
        var $rest_pg_txt 	= "#rest_pagination_text";
                         
	     $($rest_pg_txt).empty();
        $($rest_pg_txt).append($page_number);//Page Number as other is too big
        var $count = 1;

			$($div_xpg).empty();
			$btn_add = ftr_add_trip_btn();
			$($div_xpg).append($btn_add);
        
                       
         $.each( data.items, function( key, val ) 
         {
					  $alt_color 			= gx_alt_color($count);	
                 $count++;
                 val.row_color 		= $alt_color;

                 var $div_mover =    list_atrip(val);
                 items.push($div_mover);
       	});
       //ForEach
     
       $( "<div/>",  { "class": "row",html: items.join("") }).
               appendTo( "#spa_page" );
                                        
}

var $mst_trip_block = "<div class='row'>"

								+ "{{#reportid}}"

			+ "<div class='col l4 m4 s11'> "	
			+ "{{{btn_print_trip_report}}}"								
  			+ "</div>"						
  			+ "<div class='col l1 m1 s1'></div> "
			+ "<div class='col l4 m4 s12'> "  									
			+ "<btn onClick='ftr_trip_unblock();' class='btn red right-align' >"
					+ "<i class='material-icons'>backspace</i> "		           								
  									+ "Un-Block</btn>"
       	+ "</div>"			  			           									
       				  		+ "{{/reportid}}"
       				  		
       				  		+ "{{^reportid}}"
			+ "<div class='col l4 m4 s8'> "       				  		
  				+ "<btn onClick='ftr_create_report_trip();' class='btn green' >"
					+ "<i class='material-icons'>description</i> "		           								
  				+ "Create Report</btn>"
		   + "</div>"        									
       				  		+ "{{/reportid}}"

		  	+  "</div>";

var $mst_list_atrip =    
              "<div class='card border col l4 m6 s12  {{row_color}}'>" 
              				+ "<div class='card-content black-text'> "
			      	      	+ "<span class='card-title red-text center-text'>" 
			      	      			+ "<i class='material-icons'>airplanemode_active</i> "
                  		   		+ "T-{{tripid}}" 
                  				+ "</span>"
                  		+ "<div class='row'>"
   	               			+ "<div class='col l12 m12 s12'>	Start on {{start_date}}</div>"
   	               			+ "<div class='col l12 m12 s12'>	Ends on  {{end_date}}</div>"
   	               			+ "<div class='col l12 m12 s12'>	Vehicle "
   	               				+"{{#vehicleid}}{{vehicleid}}{{/vehicleid}}"
   	               				+"{{^vehicleid}} N/A {{/vehicleid}}"   	               				
   	               				+"</div>"
   	               			+ "<div class='col l12 m12 s12'>	Report "
   	               				+"{{#reportid}}{{reportid}}{{/reportid}}"
   	               				+"{{^reportid}} N/A {{/reportid}}"   	               				
   	               				+"</div>"
   	               		+ "</div>"
           					+ "<div class='card-action row'>"
   	               						+ "<btn onClick='{{edit_trip}}' class='btn  blue lighten-2 black-text'>"
					      	      			+ "<i class='material-icons'>airplanemode_active</i> "
   	               						+ "Edit </btn>"
       				  		+  "</div>"
           					+ "<div class='card-action row'>"
		           								+ "<btn onClick='{{attach_cargo}}' class='btn green' >"
														+ "<i class='material-icons'>add_shopping_cart</i> "		           								
		           									+ "List Cargo</btn>"
       				  		+  "</div>"


                     + "</div>"
                     + "</div>";

/*
Function list_acargo(val)
*/
function list_atrip(val)
{
		  var $fn_edit = "onClick=uru_spa"
                                           + "('" + "edit_trip" + "',"
                                           + "'"+ val.tripid +"'" 
                      + ");";
        val.edit_trip = $fn_edit;

		  var $fn_attache = "onClick=uru_spa"
                                           + "('" + "list_trip_cargo" + "',"
                                           + "'"+ val.tripid +"'" 
                      + ");";
        val.attach_cargo = $fn_attache;                      
	
        var $div_ax = Mustache.to_html($mst_list_atrip,val);
        return $div_ax;
}


var $mst_trip_info = "<div class='collection blue lighten-2'>"
                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo ID"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{cargoid}}"
                  			+ "</div></div>"

                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Item Name"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{product_name}}"
                  			+ "</div></div>"
                  				+ "<div class='row border collection-item '>"
                  					+"<div class='col l4  m4 s12 '>	"
	                  					+ "Source Client"
                  					+ "</div>"		
                 						+ "<div class='col input_field l8  m8 s12'>	"
                  						+ "{{src_customer_name}},{{src_customerid}}"
                  					+ "</div>" 
                  			+ "</div><div class='row border  collection-item'>"		
               						+ "<div class='col l4  m4 s12 '>	"
	                  					+ "Source City"
                  					+ "</div>"		
                 						+ "<div class='col input_field l6  m6 s12'>	"
		                 					+ "{{src_cityname}}"     
                  					+ "</div></div>"

									+"<div class='row border  collection-item'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{src_idtype}}"
										+ "</div>"	                  		
									+ "</div>"
									+"<div class='row border  collection-item'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{src_id}}"
										+ "</div>"	                  		
									+ "</div>"
                  					                  			
               			+ "<div class='row border  collection-item yellow-lighten-4'>"
               					+"<div class='col l4  m4 s12 '>	"
	                  				+ "Destination client"
                  				+ "</div>"		
                 				+ "<div class='col input_field l8  m8 s12'>	"
                  				+ "{{src_customer_name}},{{dst_customerid}}"
                  			+ "</div>"
                  		+ "</div><div class='row border  collection-item yellow-lighten-4'>"
               				+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Destination City"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
		                 				+ "{{dst_cityname}}"     
                  			+ "</div>"
									+ "</div>"

									+"<div class='row border  collection-item yellow-lighten-4'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{dst_idtype}}"
										+ "</div>"	                  		
									+ "</div>"
									+"<div class='row border  collection-item yellow-lighten-4'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{dst_id}}"
										+ "</div>"	                  		
									+ "</div>"
									
									+"<div class='row border  collection-item'>"                  			              			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Shipping Weight (KGs)</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
            						+ "{{shipping_weight}}"
									+ "</div></div>"
									+"<div class='row border  collection-item'>"                  			
                  			+"<div class='col l4  m4 s12'>	"
                  				+ "Cartage Price</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
							            + "{{cartage_price}}"
                  			+ "</div>"                      
								+ "</div>"
                  		+ "</div>";	

var $mst_atrip_form = 
						"<div class='card-panel '>"
              				+ "{{#tripid}}<div class='row'>"
	                  		+ "<div class='col l12  m12 s12 red-text center-align card-title'>	"
	                  				+ "<b>Trip  {{tripid}}</b></div>"		
              				+ "{{/tripid}}</div>"
              		+ "<form id='edit_trip' > "
                  		+  "<input name='tripid' value='{{tripid}}'  type='hidden' />"              				
								+ "<div class='row border'>"    
	                 			+ $mst_src_city_input
	                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Start Date</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='start_date' value='{{start_date}}' />"
                  			+ "</div></div>"      

									+ "<div class='row yellow lighten-4 border'>"                  			
  	               			+ $mst_dst_city_input
	                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "End Date (tentative)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='end_date' value='{{end_date}}' />"
                  			+ "</div>"      
   	               			
                 			+ "</div>"
     

     							+ "<div class='row border '>"
      							            			
                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Vehicle Number </div>"		
                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='vehicleid' 	 value='{{vehicleid}}'/>"
									+ "</div>"

                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Vehicle Cost</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='vehicle_cost' value='{{vehicle_cost}}' "
	                  					+" type='number' min='0' />"
									+ "</div>"

                  			+ "<div class='col l4  m4 s12 '>	"
                  				+ "Vehicle Owner"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "<input name='name' id='dst_customer_name'	value='{{name}}' required />"
                  			+ "</div>"
                  			+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Owner ID"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input id='dst_customerid' 	name='trip_transporterid' "
											+" value='{{trip_transporterid}}' type='text' />"
                  			+ "</div>"      

                  			+ "<div class='col l4  m4 s12 '>	"
                  				+ "Driver Name"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "<input name='name' id='src_customer_name'	value='{{name}}' required />"
                  			+ "</div>"
                  			+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Driver ID"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input id='src_customerid' 	name='driverid' value='{{driverid}}' type='text' />"
                  			+ "</div>"      
									
								+ "</div>"

                  		+ "<div class='row'><div class='col l12 m12 s12 center-align'>	"
                  				+ "{{#tripid}}"
		                  				+"<btn class='btn green' "
		                  				+" onClick='fxu_update_trip();' " 
		                  				+" >Update Trip</btn> "
		                  				+"</div>"
                  				+ "{{/tripid}}"                  			
                  				+ "{{^tripid}}"
		                  				+"<btn class='btn green' "
		                  				+" onClick='fxu_add_trip("+");' " 
		                  				+" >Add Trip</btn> </div>"
                  				+ "{{/tripid}}"
                  				
	                     + "</div></div>"
                  	+ "</form>"  
               		+ "</div>";


var $mst_edit_trip ="<ul class='collapsible popout collapsible-accordion'  data-collapsible='accordion'>"
					+"<li>" 
                      + "<div class='collapsible-header waves-effect waves-purple black-text center-align active'>"
                                    + "Cargo Info for C-{{cargoid}}"
                      + "</div>"
                      + "<div class='collapsible-body black-text yellow lighten-5'>"
                      				+ $mst_trip_info        
							 + "</div>"
					+ "</li>"		 							
					+	"<li>" 
                      + "<div class='collapsible-header waves-effect waves-purple red-text center-align'>"
                                    + "Edit {{tripid}}"
                      + "</div>"
                      + "<div class='collapsible-body black-text yellow lighten-5'>"
                      				+ $mst_atrip_form        
               		 + "</div>"
               + "</li>"
               + "</ul>";

/*
* FX: fx_form_add_trip()
* Display Form for Adding Trip
*/
function fx_form_add_trip()
{
		var $xval 			= {};
      var $div_ax 		= Mustache.to_html($mst_atrip_form,$xval);
      return $div_ax		;
}

function fxu_add_trip()
{
		var $div_form = '#edit_trip';
		
		var $input_h 	= x_getform_input($div_form);		
		var $userid 	= $input_h.userid;
		
		var $start_date = $input_h.start_date;
		var $end_date = $input_h.end_date;
		
		$is_range_ok = g_dates_increasing($start_date,$end_date);
		
		var $in_m = $url_trip_info;
		Materialize.toast($in_m,3000);
		
		Materialize.toast($userid,3000);
		
  		$.ajax( 
		            { 
		           url: 			$in_m ,
		           type: 			"POST",
		           dataType: 	"json",
		           data: $input_h,
		            error: function (xhr,status,error) 
		            {
									bx_toast_empty();
									var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);
									$($div_xpg).append($error_msg);
		            },
		            success: function( data )
		            {
		            		display_trips(data);
		            },
		                                       //GetJSON Function_data
       });
	        //ajax
	        
	        
	
}

/*
* FX: fx_fetch_cargo($div_pgx,cargoid)
*
*/


function fx_fetch_trip($div_pgx,$tripid)
{
  var $url  		 = $url_trip_info + "/" + $tripid;
  var $rest_pg_txt = "#rest_pagination_text";
  
  $($div_xpg).empty();

  if($tripid)		
  {
  $.ajax( 
            { 
           url: 			$url ,
           type: 			"GET",
           dataType: 	"json",
           error: function (xhr,status,error) 
           {
           				$($div_xpg).empty();				
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);
           },
           success: function( data )
           {
                  fx_form_edit_trip($div_pgx,$tripid,data);
                 	//Materialize.toast("Fetching Cargo", 5000);
                 	$('.collapsible').collapsible('close', 0);
           },
                                       //GetJSON Function_data
        });
        //ajax
	}

}

/*
* Function fx_form_edit_trip(tripid)
* Display Form for editing trip
*/
function fx_form_edit_trip($div_pgx,$tripid,$xdata)
{
     	//Materialize.toast("Cargo", 5000);

		var $x_cargo = ($xdata.items).pop();     	
     	
      var $div_az 	= Mustache.to_html($mst_atrip_form,$x_cargo);
      $($div_pgx).append($div_az);
      
      fxu_city_init();//City ajax init
      fxu_agent_init();
      //return $div_acargo;
}


/*
* Function fxu_update_cargo(val)
* Display Form for Adding Cargo
*/
function fxu_update_trip()
{
		var $div_form 	= '#edit_trip';
		Materialize.toast("Update Tripping now", 5000);
				
	  	var $input_h	= x_getform_input($div_form);
		var $tripid    = $input_h.tripid ;
		var $in_m 		= $url_trip_info + "/" + $tripid;
		Materialize.toast($tripid, 5000);
		
		if($tripid)
		{
	  		$.ajax( 
            { 
           url: 		$in_m ,
           type: 		"PUT",
           dataType: "json",
           data: 		$input_h,
           headers: { 
                    },
            error: function (xhr,status,error) 
            {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            },
            success: function( data )
            {
                  display_trips(data);
            },
                                       //GetJSON Function_data
        });
        //ajax
        }
        	else
        {
        		Materialize.toast("Trip Invalid",3000);
        }

}

var $mst_attach_tripcargo =
	"<ul class='collapsible collapsible-accordion'  data-collapsible='accordion'>"
	+"<li>" 
          + "<div class='collapsible-header waves-effect waves-purple "
          					+ " grey lighten-4 black-text center-align '>"
            					+ "<div class='row'>"
                      					+ "<div class='col  l1  m1  s1 center tag'> &#10095;</div>"
                                    + "<div class='col l11 m11 s11'> Trip T-{{tripid}}</div>"
                           + "</div>"
          + "</div>"
          + "<div class='collapsible-body black-text ' id='trip_info'>"
          		+ "<div class='row' id='trip_info'></div>"
			 + "</div>"
	+ "</li>"		 							
	+"<li>" 
          + "<div class='collapsible-header blue lighten-4  "
          					+ " waves-effect waves-purple black-text center-align active '>"
            					+ "<div class='row'>"
                      					+ "<div class='col  l1  m1  s1 center tag'> &#10095;</div>"
                                    + "<div class='col l11 m11 s11'> List of Cargo for Trip T-{{tripid}}</div>"
                           + "</div>"
          + "</div>"
          + "<div class='collapsible-body black-text yellow lighten-5' >"
          	+"<div class='row' id='list_trip_cargo'></div>"
			 + "</div>"
	+ "</li>"		 							
	+	"<li>" 
          + "<div class='collapsible-header "
          			+" waves-effect waves-purple grey lighten-4 red-text center-align '>"
            					+ "<div class='row'>"
                      					+ "<div class='col  l1  m1  s1 center tag'> &#10095;</div>"
                                    + "<div class='col l11 m11 s11'> Cargo Available for Loading in Trip T-{{tripid}}</div>"
                           + "</div>"
          + "</div>"
          + "<div class='collapsible-body black-text yellow lighten-5' >"
          		+"<div class='row' id='list_other_cargo'></div>"
   		 + "</div>"
   + "</li>"
   + "</ul>";

var $mst_btn_finalize_trip = "<span class='btn red' onClick='ftr_finalize_trip_loading();' >"
									+"Finalize Trip Cargo</span>";


/*
* FX: fx_fetch_trip_cargo($div_pgx,cargoid)
*
*/


function fx_fetch_trip_cargo($div_pgx,$tripid)
{
  var $url  		 = $url_trip_cargo + "/" + $tripid;
  var $rest_pg_txt = "#rest_pagination_text";
  
  $($div_xpg).empty();
  var $fill_tripid = "<input id='input_tripid' value='"+ $tripid +"' type='hidden' />";
  $($div_xpg).append($fill_tripid);

  var $print_url = "/biz/report/trip/"+$tripid;	
  var $btn_print =  "<a class='btn green white-text waves-effect waves-light' "
                                + " type='submit' "
                                +  "href=' " + $print_url + "' target='_blank' " 
                                + " name='action'> "
                                	+ "<i class='material-icons'>local_printshop</i> "		           								
                                +"Print  " 
                        + "</a>";

  //List Trip Info	
  if($tripid)		
  {
  		$.ajax( 
            { 
           url: 			$url ,
           type: 			"GET",
           dataType: 	"json",
           error: function (xhr,status,error) 
           {
           				$($div_xpg).empty();				
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);
           },
           success: function( data )
           {
           	
           			//Finalize Button
      				$($div_pgx).append($mst_btn_finalize_trip);

						//1. Add Collapsible
						var $div_base 	= Mustache.to_html($mst_attach_tripcargo,data);
      				$($div_pgx).append($div_base);

						//2. Add  trip info
						if(data.trip)
						{
							var $list_trips = data.trip; //|| 'Trip ID not available';
							var $a_trip  = $list_trips.pop();
							var $xtripid = $a_trip.tripid; 
							Materialize.toast($xtripid,5000);
							
							//Trip Print / Unblock
							$a_trip.btn_print_trip_report = $btn_print;
							var $div_print = Mustache.to_html($mst_trip_block,$a_trip);
							$('#trip_info').append($div_print);
							//Trip info
							var $div_trip_info = list_atrip($a_trip);
							$('#trip_info').append($div_trip_info);
							
						}

						//3.Listed Cargo		
						var $div_tc = ft_trip_cargo_list(data.cargo);	
						$('#list_trip_cargo').append($div_tc);	
						
                 	$('.collapsible').collapsible('close', 0);

						//4. List unAttached Cargo
						ftr_list_cargo_unattached(1);
	
                 	
           },
                                       //GetJSON Function_data
        });
        //ajax
	}

	
}

/*
*
* FX: fx_trip_cargo_list()
*
*
*/
function ft_trip_cargo_list(datacargo)
{
		var items = [];
		var $count = 1;
		if(datacargo)
		{
		         $.each( datacargo, function( key, val ) 
		         {
							  $alt_color 			= gx_alt_color($count);	
		                 $count++;
		                 val.row_color 		= $alt_color;
		         	
		                 var $div_cargo 		= list_acargo(val);
		                 items.push($div_cargo);
		       	});

		       $( "<div/>",  { "class": "row",html: items.join("") });
		       	
		}	
		else 
		{
						var $xmsg = "No Cargo for this trip;"
						items.push($xmsg);
		}
		return items;
}

/*
* FX: ftr_list_cargo_unattached(page)
*
*/

function ftr_list_cargo_unattached(inpage)
{

  var $fnx ="/biz/list/cargo";
  var $mx  = "ftr_list_cargo_unattached";
        
  var $in_rest  	= "/biz/list/cargo";
  var $in_page  	= inpage || 1;
  var $in_count 	= 10;
  var $div_use 	= '#list_other_cargo'; 
  var $in_tripid	= $("#input_tripid").val() || 'TripID: NA';

  $v_method 	  	= 'ftr_list_cargo_unattached';
  var $pgx_ul    	=	bgx_display_pgx($v_method);
  $($div_use).append($pgx_ul);
	
  var $in_m = $fnx + "/" + $in_page;
  var $rest_pg_txt = "#rest_pagination_text";
  
  var $div_form_search	= '#search_cargo';
  var $h_search 			= x_getform_input($div_form_search);
  //$($div_xpg).empty();
  
  var $loc_msg_wait 		= "Please wait while cargo list is begin fetched....";
  var $loc_msg_ok 	 	= "cargo list received.";
  
  $h_search.booked_only = 1;
  //progress_append($div_xpg,$loc_msg_wait);
	
  $.ajax( 
            { 
           url: 			$in_m ,
           type: 			"GET",
           dataType: 	"json",
           headers: 		$h_search,
           error: function (xhr,status,error) 
           {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
           },
           success: function( data )
           {
           	      bx_toast_empty();
                 	Materialize.toast($loc_msg_ok, 3000);           	      
                  ftr_display_cargo($in_tripid,data);
                 	$('.collapsible').collapsible('close', 0);
           },
                                       //GetJSON Function_data
        });
        //ajax

}// Function


/*
* FX: ftr_display_cargo: Display Cargo 
* @params: data
*/
function ftr_display_cargo($in_tripid,data)
{
        var items = [];
                       
        var $idx_first 		= data.idx_first;
        var $idx_last 		= data.idx_last;
        var $idx_str 		= $idx_first + "-" + $idx_last;
        var $rest_pg_txt 	= "#rest_pagination_text";
                         
	     $($rest_pg_txt).empty();
        $($rest_pg_txt).append($idx_str);
        Materialize.toast($in_tripid,3000);
        
        var $count = 1;
                       
         $.each( data.items, function( key, val ) 
         {

					  $alt_color = gx_alt_color($count);	
                 $count++;
                 val.row_color 		= $alt_color;
                 
                 val.can_be_added_tripid 	= $in_tripid;
                          	
                 var $div_cargo					=    list_acargo(val);
                 items.push($div_cargo);
       	});
       //ForEach

       $( "<div/>",  { "class": "row",html: items.join("") }).
               appendTo( "#list_other_cargo" );
        
		  bx_pgx_btns_init();
                                        
}

/*
* FX: ftr_finalize_trip_loading
* 
*/
function ftr_finalize_trip_loading()
{
		//Materialize.toast("Finalize trip",1000);
		var $new_items =[];
		var $remove_items = [];
		var $in_tripid	= $("#input_tripid").val() || 'TripID: NA';
		var $in_m = $url_trip_cargo + "/" + $in_tripid;
		var $h_dx = {};

      Materialize.toast("Add Cargo:",1000);
 		$( ".new_cargo" ).each(function()
      {
           var $cargoid               = $( this ).find(".new_cargoid").val();
           if($cargoid > 0)
           {
           			Materialize.toast($cargoid,1000);
           			$new_items.push($cargoid);
           }	
      });     

      Materialize.toast("Remove Cargo:",1000);           
 		$( ".attached_cargo" ).each(function()
      {
           var $cargoid               = $( this ).find(".attached_cargoid").val();
           if($cargoid > 0)
           {
           			Materialize.toast($cargoid,1000);
           			$remove_items.push($cargoid);
           }	
      });     
      
      $h_dx.remove_cargoids 	= JSON.stringify($remove_items);
      $h_dx.new_cargoids 		= JSON.stringify($new_items);      

	  $.ajax( 
            { 
           url: 			$in_m ,
           type: 			"POST",
           dataType: 	"json",
           data: 			$h_dx,
           error: function (xhr,status,error) 
           {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
           },
           success: function( data )
           {
           	      bx_toast_empty();
                 	//Materialize.toast($loc_msg_ok, 3000);   
                 	uru_spa('list_trip_cargo',$in_tripid);        	      
           },
                                       //GetJSON Function_data
        });
        //ajax


}

/***** Report(block) and unBlock  of Trip: BEGIN *****/
/*
* FX: ftr_create_report_trip
*
*/

function ftr_create_report_trip()
{
		var $fx = "ftr_create_report_trip";
		var $in_tripid	= $("#input_tripid").val();
		if(!$in_tripid)
		{
			Materialize.toast("Trip ID is not available",5000);
		}
		Materialize.toast($fx + "Trip ID" + $in_tripid,5000);

		var $in_m 		= $url_trip_info + "/" + $in_tripid;
		var $input_h = {};
		$input_h.create_report	=	1;
		
		if($in_tripid)
		{
	  		$.ajax( 
            { 
           url: 		$in_m ,
           type: 		"PUT",
           dataType: "json",
           data: 		$input_h,
           error: function (xhr,status,error) 
            {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            },
            success: function( data )
            {
                  display_trips(data);
            },
                                       //GetJSON Function_data
        });
        //ajax
        }
        	else
        {
        		Materialize.toast("Trip Invalid",3000);
        }
		

}

/*
* FX: ftr_trip_unblock
*
*/

function ftr_trip_unblock()
{
		var $fx = "ftr_trip_unblock";
		var $in_tripid	= $("#input_tripid").val();
		var $in_reason = $("#input_reason").val() || 'testing';
		if(!$in_tripid)
		{
			Materialize.toast("Trip ID is not available.",5000);
		}
		if(!$in_reason)
		{
			Materialize.toast("Unblock Reason is required.",5000);
		}
		Materialize.toast($fx + "Trip ID" + $in_tripid,5000);

		var $in_m 		= $url_trip_info + "/" + $in_tripid;
		var $input_h = {};
		$input_h.unblock_force	=	1;
		$input_h.reason			=  $in_reason ;
		
		if($in_tripid && $in_reason)
		{
	  		$.ajax( 
            { 
           url: 		$in_m ,
           type: 		"PUT",
           dataType: "json",
           data: 		$input_h,
           error: function (xhr,status,error) 
            {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            },
            success: function( data )
            {
                  display_trips(data);
            },
                                       //GetJSON Function_data
        });
        //ajax
        }
        	else
        {
        		Materialize.toast("Trip Invalid",3000);
        }
		
		
}

/*
* FX: ftr_print_trip_report
* Not Needed.
*/

function ftr_print_trip_report()
{
		var $in_tripid	= $("#input_tripid").val();
		var $fx = "ftr_print_trip_report";
		
		if(!$in_tripid)
		{
			Materialize.toast("Trip ID is not available",5000);
		}
		Materialize.toast($fx + "Trip ID" + $in_tripid,5000);

}

/***** Report(block) and unBlock  of Trip: END *****/