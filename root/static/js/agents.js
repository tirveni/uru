/*
* Agents.js for URU..

Authors: Tirveni Yadav

Uru (from Taipan) is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Uru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with TAIPAN.  If not, see <http://www.gnu.org/licenses/>.
<GNU AGPL: http://www.gnu.org/licenses/agpl-3.0.html>

*
*/

var $agent_search_url 	= '/biz/list/movers';

var $btn_add_mover =	"<div class='row><div class='col l12 m12 s12'>"
		 								+" <btn class='btn orange' onClick=uru_spa"
                                           + "('" + "add_mover" + "'" +");>"
                              + " + Add New Agent/Staff </btn></div></div> ";


/*
* FX: fxu_list_movers: Display Cargo
* @params: incount,inpage,indate
*/

function fxu_list_movers(inpage)
{

  var $fnx ="/biz/list/movers";;
        
  var $in_rest  = "/biz/list/movers";
  var $in_page  = inpage || 1;
  var $in_count = 10;
  var $in_date;                
  var $h_search = {};

  var $in_m = $fnx + "/" + $in_page;
  var $rest_pg_txt = "#rest_pagination_text";

  var $loc_msg_wait 		= "Please wait, while Client list is begin fetched....";
  var $loc_msg_ok 	 	= "Client list received.";
  
  progress_append($div_xpg,$loc_msg_wait);

  $.ajax( 
            { 
           url: 			$in_m ,
           type: 			"GET",
           dataType: 	"json",
           timeout: 		$c_ajax_timeout_milisec,
           headers: 		$h_search,
           error: function (xhr,status,error) 
           {
           				$($div_xpg).empty();				
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);

       					$('#spa_page').append($btn_add_mover);							
							
           },
           success: function( data )
           {
           	      bx_toast_empty();
                 	Materialize.toast($loc_msg_ok, 3000);           	      
                  display_movers(data);
           },
                                       //GetJSON Function_data
        });
        //ajax

}// Function


/*
* FX: display_cargo: Display Cargo 
* @params: data
*/
function display_movers(data)
{
        var items = [];

		  if(data)
		  {
			        var $idx_first 		= data.idx_first;
			        var $idx_last 		= data.idx_last;
			        var $idx_str 		= $idx_first + "-" + $idx_last;
			        var $page_number	= data.page_number;
			        var $rest_pg_txt 	= "#rest_pagination_text";
        }
                         
	     $($rest_pg_txt).empty();
        $($rest_pg_txt).append($page_number);//Page Number as other is too big

       $('#spa_page').empty();
       $('#spa_page').append($btn_add_mover);       
        
        var $count = 1;
                       
        $.each( data.items, function( key, val ) 
         {

					  $alt_color = gx_alt_color($count);	
                 $count++;
                 val.row_color 		= $alt_color;
                          	
                 var $div_mover =    list_amover(val);
                 items.push($div_mover);

        });
       //ForEach

       $( "<div/>",  { "class": "row",html: items.join("") }).
               appendTo( "#spa_page" );
                                        
}


var $mst_list_amover =    
              "<div class='card border col l4 m6 s12 {{row_color}} '>" 
              				+ "<div class='card-content black-text'> "
			      	      	+ "<span class='card-title black-text '>"
			      	      		+ "<div class='row'>"
   	               			+ "<div class='col l12 m12 s12'>"
			      	      			+ "<i class='material-icons'>account_box</i> " 
												+ "<span class='str_truncate'>{{name}}</span>"		
   	               				+ "</div>"
   	               				+ "</div>"
                  				+ "</span>"
                  		+ "<div class='row'>"
   	               			+ "<div class='col l12 m12 s12'>ID: {{userid}}"
   	               				+ "</div>"
   	               			+ "<div class='col l12 m12 s12'>	{{role}}</div>"
   	               		+ "</div>"
           					+ "<div class='card-action'>"
           									+ "<btn onClick='{{edit_user_btn}}' class='btn blue lighten-2 black-text '>"
												+ "<i class='material-icons'>account_box</i> "           									
           									+ "Edit Client</btn>"
       				  			+  "</div>"
           					+ "<div class='card-action'>"
           									+ "<btn onClick='{{add_cargo_btn}}' class='btn orange' >"
           										+ "<i class='material-icons'>add_shopping_cart</i> "
           										+ " Add Cargo </btn>"
       				  			+  "</div>"
                     + "</div>"
                     + "</div>";

/*
Function list_acargo(val)
*/
function list_amover(val)
{
		  var $fn_click = "onClick=uru_spa"
                                           + "('" + "add_cargo" + "',"
                                           + "'"+ val.userid +"'" 
                      + ");";
        val.add_cargo_btn = $fn_click;

		  var $fn_edit = "onClick=uru_spa"
                                           + "('" + "edit_mover" + "',"
                                           + "'"+ val.userid +"'" 
                      + ");";
		  val.edit_user_btn = $fn_edit;                       
                       
	
        var $div_ax = Mustache.to_html($mst_list_amover,val);
        return $div_ax;
}


var $mst_mover_info = "<div class='collection blue lighten-2'>"
                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo ID"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{cargoid}}"
                  			+ "</div></div>"

                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Item Name"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{product_name}}"
                  			+ "</div></div>"
                  				+ "<div class='row border collection-item '>"
                  					+"<div class='col l4  m4 s12 '>	"
	                  					+ "Source Client"
                  					+ "</div>"		
                 						+ "<div class='col input_field l8  m8 s12'>	"
                  						+ "{{src_customer_name}},{{src_customerid}}"
                  					+ "</div>" 
                  			+ "</div><div class='row border  collection-item'>"		
               						+ "<div class='col l4  m4 s12 '>	"
	                  					+ "Source City"
                  					+ "</div>"		
                 						+ "<div class='col input_field l6  m6 s12'>	"
		                 					+ "{{src_cityname}}"     
                  					+ "</div></div>"

									+"<div class='row border  collection-item'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{src_idtype}}"
										+ "</div>"	                  		
									+ "</div>"
									+"<div class='row border  collection-item'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{src_id}}"
										+ "</div>"	                  		
									+ "</div>"
                  					                  			
               			+ "<div class='row border  collection-item yellow-lighten-4'>"
               					+"<div class='col l4  m4 s12 '>	"
	                  				+ "Destination client"
                  				+ "</div>"		
                 				+ "<div class='col input_field l8  m8 s12'>	"
                  				+ "{{src_customer_name}},{{dst_customerid}}"
                  			+ "</div>"
                  		+ "</div><div class='row border  collection-item yellow-lighten-4'>"
               				+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Destination City"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
		                 				+ "{{dst_cityname}}"     
                  			+ "</div>"
									+ "</div>"

									+"<div class='row border  collection-item yellow-lighten-4'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{dst_idtype}}"
										+ "</div>"	                  		
									+ "</div>"
									+"<div class='row border  collection-item yellow-lighten-4'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{dst_id}}"
										+ "</div>"	                  		
									+ "</div>"
									
									+"<div class='row border  collection-item'>"                  			              			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Shipping Weight (KGs)</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
            						+ "{{shipping_weight}}"
									+ "</div></div>"
									+"<div class='row border  collection-item'>"                  			
                  			+"<div class='col l4  m4 s12'>	"
                  				+ "Cartage Price</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
							            + "{{cartage_price}}"
                  			+ "</div>"                      
								+ "</div>"
								+"<div class='row border  collection-item'>"                  			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Cartage Paid</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
										+"{{#is_cartage_paid}}Yes{{/is_cartage_paid}}{{^is_cartage_paid}}No{{/is_cartage_paid}}"                 				
                  			+ "</div>"
                  		+"</div>"
                  		+ "<div class='row border  collection-item'>"
                  			+"<div class='col l4  m4 s12'>	"
                  				+ "Pay by Source</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                 					+"{{#is_payby_src}}Yes{{/is_payby_src}}{{^is_payby_src}}No{{/is_payby_src}}"
                  			+ "</div>"
                  		+ "</div>"
                  		+ "</div>";	

var $mst_amover_form = 
						"<div class='card-panel white '>"
              		+ "<form id='edit_mover' > "
                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Customer Name"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "<input name='name' id='src_customer_name'	value='{{name}}' required />"
                  			+ "</div></div>"
                  			
                  			+ "<div class='row'>"
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_idtype' value='{{src_idtype}}' />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_id' 	 value='{{src_id}}'/>"
										+ "</div>"
									+"</div>"

									+ "<div class='row border'>"

									+ "{{#userid}}"		

									+ "<div class='col l4  m4 s12'>	"
	                  				+ "Update Address"
                  				+ "</div>"		
                 				+ "<div class='col l6  m6 s12'>	"
										+"<select class='browser-default ' "
														+ " name='update_address'   >"
                 						+ $mst_boolean_dropbox
                 					+ "</select>"
                  			+ "</div>"  
                  			+ "<div class='col l12 m12 s12'>&nbsp;</div>"

                  																			
                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Address Line1"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input name='streetaddress1' value='{{streetaddress1}}' type='text' />"
                  			+ "</div>"  
                  			+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Address Line2"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input name='streetaddress2' value='{{streetaddress2}}' type='text' />"
                  			+ "</div>"  

                  			+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Address Line3"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input name='streetaddress3' value='{{streetaddress3}}' type='text' />"
                  			+ "</div>"  
									+ "{{/userid}}"
                  			+ "{{^userid}}"
                  			+ "<div class='col l4  m4 s12 '>	"
	                  				+ "User ID"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input id='src_customerid' 	name='userid' value='{{userid}}' type='text' />"
                  			+ "</div>"  
                  			+ "{{/userid}}"
                  			+ $mst_src_city_input
                  			+ "</div>"
									
										                  		
                  			+ "<div class='row'>"
                  			+"<div class='col l12 m12 s12 center-align'>	"
                  				+ "{{#userid}}"
		                  				+"<btn class='btn green' "
		                  				+" onClick='fxu_update_mover();' " 
		                  				+" >Update Client</btn> "
                  						+ "<input name='userid' value='{{userid}}' type='hidden' /> "		                  				
		                  				+"</div>"
                  				+ "{{/userid}}"                  			
                  				+ "{{^userid}}"
		                  				+"<btn class='btn green' "
		                  				+" onClick='fxu_add_mover("+");' " 
		                  				+" >Add Client</btn> </div>"
                  				+ "{{/userid}}"
	                     + "</div></div>"
                  	+ "</form>"  
               		+ "</div>";


var $mst_edit_mover ="<ul class='collapsible popout collapsible-accordion'  data-collapsible='accordion'>"
					+"<li>" 
                      + "<div class='collapsible-header waves-effect waves-purple black-text center-align active'>"
                                    + "Cargo Info for C-{{cargoid}}"
                      + "</div>"
                      + "<div class='collapsible-body black-text yellow lighten-5'>"
                      		+ $mst_mover_info        
							 + "</div>"
					+ "</li>"		 							
					+	"<li>" 
                      + "<div class='collapsible-header waves-effect waves-purple red-text center-align'>"
                                    + "Add {{cargoid}}"
                      + "</div>"
                      + "<div class='collapsible-body black-text yellow lighten-5'>"
                      				+ $mst_add_cargo        
               		 + "</div>"
               + "</li>"
               + "</ul>";

/*
* FX: fx_form_add_mover()
* Display Form for Adding Agent
*/
function fx_form_add_mover($in_val)
{
		var $xval = {};

		if($in_val)		
		{
				 $xval 			= $in_val;			
		}

      var $div_ax 		= Mustache.to_html($mst_amover_form,$xval);
      return $div_ax		;
}

/*
* FX: fxu_add_mover

*/

function fxu_add_mover()
{
		var $div_form = '#edit_mover';
		
		var $input_h 	= x_getform_input($div_form);		
		var $userid 	= $input_h.userid || $input_h.src_id;
		
		var $in_m = 'biz/mover';
		Materialize.toast($in_m,3000);
		
		if($userid)
		{
				Materialize.toast($userid,3000);
		  		$.ajax( 
		            { 
		           url: 			$in_m ,
		           type: 			"POST",
		           dataType: 	"json",
		           data: $input_h,
		            error: function (xhr,status,error) 
		            {
									bx_toast_empty();
									var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);
									$($div_xpg).append($error_msg);
		            },
		            success: function( data )
		            {
		            		display_movers(data);
		            },
		                                       //GetJSON Function_data
		        });
		        //ajax
        }
	
	
}


/*
* FX : fxa_fetch_user

*/

function fxa_fetch_user($div_pgx,$userid)
{
  var $url  		 = "biz/mover" + "/" + $userid;
  var $rest_pg_txt = "#rest_pagination_text";
  
  //Materialize.toast("Getting User:"+$userid,2000);
  $($div_xpg).empty();

  if($userid)		
  {
  $.ajax( 
            { 
           url: 			$url ,
           type: 			"GET",
           dataType: 	"json",
           error: function (xhr,status,error) 
           {
           				$($div_xpg).empty();				
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);
           },
           success: function( data )
           {
           			var $x_user = (data.items).pop();

                  var $div_user = fx_form_add_mover($x_user);
						$($div_xpg).append($div_user);
						fxu_city_init();
           },
                                       //GetJSON Function_data
        });
        //ajax
	}

}


/*
* Function fxu_update_cargo(val)
* Display Form for Adding Cargo
*/
function fxu_update_mover()
{
		var $div_form 	= '#edit_mover';
		Materialize.toast("Update now", 2000);

						
	  	var $input_h	= x_getform_input($div_form);
	  	$userid = $input_h.userid;
		var $in_m 		= "biz/mover" + "/" + $userid;
		Materialize.toast($userid, 5000);
		
		if($userid)
		{
	  		$.ajax( 
            { 
           url: 		$in_m ,
           type: 		"PUT",
           dataType: "json",
           data: 		$input_h,
           headers: { 
                    },
            error: function (xhr,status,error) 
            {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            },
            success: function( data )
            {
                  	display_movers(data);
            },
                                       //GetJSON Function_data
        });
        //ajax
        }
        	else
        {
        		Materialize.toast("User Invalid",3000);
        }
        

}

