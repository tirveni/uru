/*
* Uru JS.
*
* Uru.js for URU..

Authors: Tirveni Yadav

Uru (from Taipan) is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Uru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with TAIPAN.  If not, see <http://www.gnu.org/licenses/>.
<GNU AGPL: http://www.gnu.org/licenses/agpl-3.0.html>
*
*
*/

var $city_search_url 		= "/asearch/cities";
var $currency_search_url 	= '/asearch/currencies';
var $agent_search_url 	= '/biz/list/movers';

var $mst_dst_city_input = "	<div class='col l4  m4 s12'>	"
                  				+ "Destination City</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
     					+ "<input name='dst_cityname'  id='dst_city' 		value='{{dst_cityname}}' required 	/>"
     					+ "<input name='dst_citycode'  id='dst_citycode' 	value='{{dst_citycode}}' type='hidden' />"
									+ "</div>";
									
var $mst_src_city_input = "<div class='col l4  m4 s12'>	"
                  				+ "Source City</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                 	+ "<input name='src_cityname' 	id='src_city' 		value='{{src_cityname}}'	required 	/>"
                 	+ "<input name='src_citycode'  	id='src_citycode' value='{{src_citycode}}'	type='hidden' />"
									+ "</div>";    

var $mst_src_agent ="<div class='col l4  m4 s12 '>	"
										+ "From (Source Client)"
                  	+ "</div>"		
                 		+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input id='src_customer_name' name='customer_name'  value='{{src_customer_name}}'	/>"
										+ "<input id='src_customerid' 	name='src_customerid' value='{{src_customerid}}' type='hidden' />"
                  	+ "</div>";                

var $mst_dst_agent = "<div class='col l4  m4 s12 '>	"
	                  				+ "To (Destination client)"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
										+ "<input id='dst_customer_name' name='dst_customer_name'  	value='{{dst_customer_name}}'	/>"
										+ "<input id='dst_customerid' 	name='dst_customerid' 		value='{{dst_customerid}}' type='hidden'/>"
                  			+ "</div>";                  			
                     			

var $mst_boolean_dropbox =  "<option value='' disabled >Choose Yes / No</option>"
      									+ "<option value='1'>Yes</option>"
      									+ "<option value='0' selected>No  &#9745; </option>";

/*
* FX: list_cargo: Display Cargo
* @params: incount,inpage,indate
*/

function fxu_list_cargo(inpage)
{

  var $fnx ="/biz/list/cargo";;
        
  var $in_rest  = "/biz/list/cargo";
  var $in_page  = inpage || 1;
  var $in_count = 10;
  var $in_date;                

  var $in_m = $fnx + "/" + $in_page;
  var $rest_pg_txt = "#rest_pagination_text";
  
  var $div_form_search	= '#search_cargo';
  var $h_search 			= x_getform_input($div_form_search);

  var $grid					= $h_search.transporter_invoiceid;
  var $cid					= $h_search.cargoid;
  
  $($div_xpg).empty();
  
  var $loc_msg_wait 		= "Please wait while cargo list is begin fetched....";
  var $loc_msg_ok 	 	= "cargo list received.";
  
  progress_append($div_xpg,$loc_msg_wait);
  
  	 	
  $.ajax( 
            { 
           url: 			$in_m ,
           type: 			"GET",
           dataType: 	"json",
           headers: 		$h_search,
           error: function (xhr,status,error) 
           {
           				$($div_xpg).empty();				
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);
           },
           success: function( data )
           {
           	      bx_toast_empty();
                 	Materialize.toast($loc_msg_ok, 3000);           	      
                  display_cargo(data);
                 	$('.collapsible').collapsible('close', 0);
           },
                                       //GetJSON Function_data
        });
        //ajax

}// Function

/*
* FX: display_cargo: Display Cargo 
* @params: data
*/
function display_cargo(data)
{
        var items = [];
                       
        var $idx_first 		= data.idx_first;
        var $idx_last 		= data.idx_last;
        var $idx_str 		= $idx_first + "-" + $idx_last;
        var $rest_pg_txt 	= "#rest_pagination_text";
                         
	     $($rest_pg_txt).empty();
        $($rest_pg_txt).append($idx_str);
        
        var $count = 1;
                       
         $.each( data.items, function( key, val ) 
         {

					  $alt_color = gx_alt_color($count);	
                 $count++;
                 val.row_color 		= $alt_color;
         	
                 var $div_cargo =    list_acargo(val);
                 items.push($div_cargo);
       	});
       //ForEach
                
       var $m_tc = " </tbody>  </table>";
       items.push($m_tc);      

       $('#spa_page').empty();

       $( "<div/>",  { "class": "row",html: items.join("") }).
               appendTo( "#spa_page" );
                                        
}

var $mst_list_acargo =    
              "<div class='card col l4 m6 s12 {{row_color}} border'>" 
              				+ "<div class='card-content black-text'> "
			      	      	+ "<span class='card-title red-text'>" 
			      	      			+ "<i class='material-icons'>assignment</i> "
			      	      			+" C- {{cargoid}}" 
                  				+ "</span>"
                  		+ "<div class='row'>"		
	                  		+ "<div class='col l12 m12 s12'>	{{stageid}}</div>"
	                			+ "<div class='col l12 m12 s12'> GR: {{transporter_invoiceid}}</div>"
	                  		+ "<div class='col l12 m12 s12'>	{{src_city}} to {{dst_city}}</div>"
	                  		+ "<div class='col l12 m12 s12'>By: {{src_customerid}}</div>"
	                  		+ "<div class='col l12 m12 s12'>To: {{dst_customerid}}</div>"
	                  		+ "<div class='col l12 m12 s12'>On:{{created_at}}</div>"
	                  	+ "</div>"
                     + "</div>"
                     	+ "<div class='card-action'> "
                     		+ "{{{btn_actions}}}"
                     	+ "</div>"
							+ "</div>";

                      

/*
Function list_acargo(val)
*/
function list_acargo(val)
{

		  var $btn_edit_cargo;
		  $btn_edit_cargo = "onClick=uru_spa"
                                           + "('" + "edit_cargo" + "',"
                                           + "'"+ val.cargoid +"'" 
                     + ");";
        $cargoid = val.cargoid;	
		  	
		  if(val.can_be_added_tripid)
		  {
				var $x_div = "<div class='row'>"
										+ "<div class='col s10 new_cargo' >"
												+"<select class='browser-default new_cargoid' "
														+ " name='"+ $cargoid +"'   >"
														+ "<option value='' disabled >To Trip</option>"
			      									+ "<option value='"+ $cargoid +"'>Load	</option>"
			      									+ "<option value='0' selected>Do Not Load &#9745; </option>"														
												+ "</select>"
										+"</div>"
									+ "</div>"; 		  		
		  		val.btn_actions 	= $x_div ;
		  }
		  else if(val.tripid)
		  {
				var $x_div = "<div class='row'>"
										+ "<div class='col s10 attached_cargo' >"
												+"<select id='remove_cargoid' class='browser-default attached_cargoid' "
														+ " name='"+ $cargoid +"'   >"
														+ "<option value='' disabled >From Trip</option>"
			      									+ "<option value='0' selected>Loaded &#9745; </option>"
														+ "<option value='"+ $cargoid +"'>Un-Load</option>"			      																							
												+ "</select>"
										+"</div>"
									+ "</div>"; 		  		
		  		val.btn_actions 	= $x_div ;
		  		
		  }
		  else 
		  {
					var $btn = "<btn class='btn blue lighten-2 black-text ' " + $btn_edit_cargo + " >"
            									+ "<i class='material-icons'>shopping_cart </i> " 
            									+ "Edit Cargo</btn>";
                      
               val.btn_actions = $btn;
         }             
        	              
	
        var $div_acargo = Mustache.to_html($mst_list_acargo,val);
        return $div_acargo;
}

var $mst_search_cargo =    
				  "<ul class='collapsible ' > "
				  +"<li>"	
				  +"<div class='collapsible-header blue lighten-3'>"
				  	+ "Search Cargo"
				  +"</div>" 
				  +"<div class='collapsible-body'>"
              +"<div class='card-panel blue lighten-5'>" 
              		+ "<form id='search_cargo' > "
                  			+ "<div class='row'><div class='col l12  m12 s12 center-align green-text'>	"
                  				+ 	"<b>Search Cargo</b>"		
                  			+ "</div></div>"


                  			+ "<div class='row border'><div class='col l4  m4 s12'>	"
                  				+ "Pay by Source</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  			+ "<select class='browser-default' name='is_payby_src'>"
      									+ "<option value='' disabled selected>Choose yes / no</option>"
      									+ "<option value='1'>Yes</option>"
      									+ "<option value='0'>No</option>"
    								+ "</select>"
                  			+ "</div></div>"
                  			
                  			+ "<div class='row border'><div class='col l4  m4 s12'>	"
                  				+ "Cartage Payment Complete</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  			+ "<select class='browser-default' name='is_cartage_paid'>"
      									+ "<option value='' disabled selected>Choose yes / no</option>"
      									+ "<option value='1'>Yes</option>"
      									+ "<option value='0'>No</option>"
    								+ "</select>"
                  			+ "</div></div>"
                  			                                          			                    			       			                			     			
                  			+ "<div class='row border white'>"
                  			+ $mst_src_agent
									+ $mst_src_city_input
	                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_idtype'  />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_id' 	 />"
									+ "</div></div>"


                  			+ "<div class='row border yellow lighten-4'>"
                  				+"<div class='col l12  m12 s12 center-align red-text'>	"
                  					+ "Destination (to) </div>"
	                 			+ $mst_dst_agent
                  			+ $mst_dst_city_input
                  				
	                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='dst_idtype'  />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='dst_id' 	 />"
									+ "</div></div>"

                  			+ "<div class='row border'>"
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "From Date</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  				+ 	"<input name='from_date' value='' /></div>"                  				
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "Till Date</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  				+ 	"<input name='Till_date' value='' /></div>"
                  				
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "GR number</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  				+ 	"<input name='transporter_invoiceid' value='' /></div>"
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "CargoID</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  				+ 	"<input name='cargoid' value='' /></div>"                  				                  				
                  				                  				                  				
                  			+ "</div>"


                  			+ "<div class='row'><div class='col l12 m12 s12 center-align'>	"
                  				+"<btn class='btn green' "
                  				+" onClick='fxu_list_cargo("+");' " 
                  				+" >Search Cargo</btn> </div>"                  			
	                     + "</div></div>"
                  + "</form>"
                 + "</div>"
                 + "</div>"
                 + "<li>"   
               + "</ul>";


/*
* FX: fxu_search_cargo:
* Returns: Div for Search Cargo

*/

function fxu_search_cargo()
{
		 var 		$h_x = {};
       var    	$div_ac = Mustache.to_html($mst_search_cargo,$h_x);
       return 	$div_ac;
}


/**************/

var $mst_add_cargo =    
              "<div class='card-panel white'>" 
              		+ "<form id='add_cargo' > "
                  			+ "<div class='row'><div class='col l12  m12 s12 center-align green-text'>	"
                  				+ 	"<b>{{cargoid}}Add Cargo for {{userid}}</b>"		
                  			+ "</div></div>"
                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Item Name</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "<input name='product_name' required />"
                  				+ 	"<input name='src_customerid' value='{{userid}}' "
                  				+	" type='hidden'/>"
                  			+ "</div></div>"
                  			+ $mst_src_city_input
                  			+ $mst_dst_city_input                  			
	                  		+ "<div class='row border'><div class='col l4  m4 s12'>	"
	                  				+ "ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_idtype'  />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_id' 	 />"
									+ "</div></div>"	                  		
                  			+ "<div class='row'><div class='col l4  m4 s12'>	"
                  				+ "Shipping Weight (KGs)</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  				+ "<input name='shipping_weight' required type='number' min='0.0' />"
                  			+ "</div></div>"                        
                  			+ "<div class='row'><div class='col l4  m4 s12'>	"
                  				+ "Cartage Price</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  				+ "<input name='cartage_price' required />"
                  			+ "</div></div>"                      
                  			+ "<div class='row border'><div class='col l4  m4 s12'>	"
                  				+ "Pay by Source</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                  			+ "<select class='browser-default' name='is_payby_src'>"
                  				+ $mst_boolean_dropbox
    								+ "</select>"
                  			+ "</div></div>"                                          			                    			       			                			     			
                  			+ "<div class='row'><div class='col l12 m12 s12 center-align'>	"
                  				+"<btn class='btn green' "
                  				+" onClick='fxu_create_cargo("+");' " 
                  				+" >Save Cargo</btn> </div>"                  			
	                     + "</div></div>"
                  + "</form>"  
               + "</div>";

var $mst_payby_src = 	"{{#is_payby_src}}"
                  			+ "<select class='browser-default' name='is_payby_src' >"
      									+ "<option value='' disabled >Choose yes / no</option>"
      									+ "<option value='1' selected>Yes &#9745;</option>"
      									+ "<option value='0'>No</option>"
    								+ "</select>"
    							+"{{/is_payby_src}}"
    							+ "{{^is_payby_src}}"
                  			+ "<select class='browser-default' name='is_payby_src' >"
      									+ "<option value='' disabled >Choose yes / no</option>"
      									+ "<option value='1'>Yes</option>"
      									+ "<option value='0' selected>No  &#9745; </option>"
    								+ "</select>"
    							+"{{/is_payby_src}}";

var $mst_cartage_paid ="{{#is_cartage_paid}}"
                  			+ "<select class='browser-default' name='is_cartage_paid'>"
      									+ "<option value='' disabled 	>Choose yes / no</option>"
      									+ "<option value='1' selected	>Yes &#9745;</option>"
      									+ "<option value='0'				>No</option>"
    								+ "</select>"
    							+ "{{/is_cartage_paid}}"
								+ "{{^is_cartage_paid}}"
                  			+ "<select class='browser-default' name='is_cartage_paid'>"
      									+ "<option value='' disabled 	>Choose yes / no</option>"
      									+ "<option value='1'				>Yes</option>"
      									+ "<option value='0' selected	>No &#9745;</option>"
    								+ "</select>"
    								+"{{/is_cartage_paid}}";


var $mst_door_delivery ="{{#is_door_delivery}}"
                  			+ "<select class='browser-default' name='is_door_delivery'>"
      									+ "<option value='' disabled 	>Choose yes / no</option>"
      									+ "<option value='1' selected	>Yes &#9745;</option>"
      									+ "<option value='0'				>No</option>"
    								+ "</select>"
    							+ "{{/is_door_delivery}}"
								+ "{{^is_door_delivery}}"
                  			+ "<select class='browser-default' name='is_door_delivery'>"
      									+ "<option value='' disabled 	>Choose yes / no</option>"
      									+ "<option value='1'				>Yes</option>"
      									+ "<option value='0' selected	>No &#9745;</option>"
    								+ "</select>"
    								+"{{/is_door_delivery}}";    								
    								
var $mst_cargo_info = "<div class='collection blue lighten-2'>"
                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "GR"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{transporter_invoiceid}}"
                  			+ "</div></div>"

                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo ID"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{cargoid}}"
                  			+ "</div></div>"

                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+"Stage"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{stageid}}"
                  			+ "</div></div>"

                  			+ "<div class='row collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Item Name "
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "{{product_name}}"
                  			+ "</div></div>"
                  				+ "<div class='row border collection-item + "+$zebra_a +"'>"
                  					+"<div class='col l4  m4 s12 '>	"
	                  					+ "Source Client Name"
                  					+ "</div>"		
                 						+ "<div class='col input_field l8  m8 s12'>	"
                  						+ "{{src_customer_name}}"
                  					+ "</div>" 
										+ "</div><div class='row border collection-item  " + $zebra_a +" '>"                  					
                  					+"<div class='col l4  m4 s12 '>	"
	                  					+ "Source Emailx"
                  					+ "</div>"		
                 						+ "<div class='col input_field l8  m8 s12'>	"
                  						+ "{{src_customerid}}"
                  					+ "</div>"                   					
                  			+ "</div><div class='row border  collection-item "+$zebra_a +"'>"		
               						+ "<div class='col l4  m4 s12 '>	"
	                  					+ "Source City"
                  					+ "</div>"		
                 						+ "<div class='col input_field l6  m6 s12'>	"
		                 					+ "{{src_cityname}}"     
                  					+ "</div></div>"
									+"<div class='row border  collection-item "+$zebra_a +"'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source Address</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{str_src_address}}"
										+ "</div>"	                  		
									+ "</div>"


									+"<div class='row border  collection-item "+$zebra_a +"'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{src_idtype}}"
										+ "</div>"	                  		
									+ "</div>"
									+"<div class='row border  collection-item "+$zebra_a +"'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{src_id}}"
										+ "</div>"	                  		
									+ "</div>"
                  					                  			
               			+ "<div class='row border  collection-item  "+$zebra_b +" '>"
               					+"<div class='col l4  m4 s12 '>	"
	                  				+ "Destination Client Name"
                  				+ "</div>"		
                 				+ "<div class='col input_field l8  m8 s12'>	"
                  				+ "{{dst_customer_name}}"
                  			+ "</div></div>"
               			+ "<div class='row border  collection-item   "+$zebra_b +" '>"
               					+"<div class='col l4  m4 s12 '>	"
	                  				+ "Destination Email"
                  				+ "</div>"		
                 				+ "<div class='col input_field l8  m8 s12'>	"
                  				+ " {{dst_customerid}}"
                  			+ "</div>"                  			
                  		+ "</div><div class='row border  collection-item   "+$zebra_b +" '>"
               				+ "<div class='col l4  m4 s12 '>	"
	                  				+ "Destination City"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
		                 				+ "{{dst_cityname}}"     
                  			+ "</div>"
									+ "</div>"
									+"<div class='row border  collection-item "+$zebra_b +"'>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination Address</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{str_dst_address}}"
										+ "</div>"	                  		
									+ "</div>"


									+"<div class='row border  collection-item   "+$zebra_b +" '>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{dst_idtype}}"
										+ "</div>"	                  		
									+ "</div>"
									+"<div class='row border  collection-item   "+$zebra_b +" '>"                  			                  			
		                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "{{dst_id}}"
										+ "</div>"	                  		
									+ "</div>"
									
									+"<div class='row border  collection-item'>"                  			              			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Shipping Weight (KGs)</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
            						+ "{{shipping_weight}}"
									+ "</div></div>"
									+"<div class='row border  collection-item'>"                  			
                  			+"<div class='col l4  m4 s12'>	"
                  				+ "Cartage Price</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
							            + "{{cartage_price}}"
                  			+ "</div>"                      
								+ "</div>"
								+"<div class='row border  collection-item'>"                  			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Cartage Paid</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
										+"{{#is_cartage_paid}}Yes{{/is_cartage_paid}}{{^is_cartage_paid}}No{{/is_cartage_paid}}"                 				
                  			+ "</div>"
                  		+"</div>"
                  		+ "<div class='row border  collection-item'>"
                  			+"<div class='col l4  m4 s12'>	"
                  				+ "Pay by Source</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                 					+"{{#is_payby_src}}Yes{{/is_payby_src}}{{^is_payby_src}}No{{/is_payby_src}}"
                  			+ "</div>"
                  		+ "</div>"

                 			+ "<div class='row  border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo Type"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "{{cargo_type}}"
                  			+ "</div></div>"
                  			
                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo Number (Pvt Mark)"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "{{cargonumber}}"
                  			+ "</div></div>"

                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo Value"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{cargo_value}}"
                  			+ "</div></div>"


                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Unit Details"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{unit_details}}"
                  			+ "</div></div>"

                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Unit Number"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{unit_number}}"
                  			+ "</div></div>"

                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Amt Green Tax"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{amt_green_tax}}"
                  			+ "</div></div>"

                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Amt Hamali"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{amt_hamali}}"
                  			+ "</div></div>"


                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Amt other"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{amt_other}}"
                  			+ "</div></div>"

                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Grand total"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "{{amt_grand_total}}"
                  			+ "</div></div>"


                  			+ "<div class='row border  collection-item'><div class='col l4  m4 s12 '>	"
                  				+ "Insurance Info"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "{{insurance_info}}'"
                  			+ "</div></div>"
                  		
                  		+ "</div>";	
var $mst_acargo_more_form =
                   "<div class='card-panel blue lighten-5'> "			                                          			                    			       			                			     			
                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo Type"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "<input name='cargo_type' 	value='{{cargo_type}}' required />"
                  			+ "</div></div>"
                  			
                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo Number (Pvt Mark)"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "<input name='cargonumber' 	value='{{cargonumber}}' required />"
                  			+ "</div></div>"

                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Cargo Value"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "<input name='cargo_value' 	value='{{cargo_value}}' type='number' />"
                  			+ "</div></div>"


                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Unit Details"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "<input name='unit_details' 	value='{{unit_details}}'  />"
                  			+ "</div></div>"

                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Unit Number"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "<input name='unit_number' 	value='{{unit_number}}' type='number' />"
                  			+ "</div></div>"

                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Amt Green Tax"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "<input name='amt_green_tax' 	value='{{amt_green_tax}}' type='number' />"
                  			+ "</div></div>"

                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Amt Hamali"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "<input name='amt_hamali' 	value='{{amt_hamali}}' type='number' />"
                  			+ "</div></div>"


                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Amt other"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' >	"
                  				+ "<input name='amt_other' 	value='{{amt_other}}' type='number' />"
                  			+ "</div></div>"

                  			+ "<div class='row'><div class='col l4  m4 s12 '>	"
                  				+ "Insurance Info"
                  					+ "</div>"		
                 					+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "<input name='insurance_info' 	value='{{insurance_info}}' />"
                  			+ "</div></div>"
                  + "</div>";

var $mst_cargo_update_btn = "<div class='card'>"			                                          			                    			       			                			     			
                 					+ "<div class='row'><div class='col l12 m12 s12 center-align'>	"
                  				+"<btn class='btn green' "
                  				+" onClick='fxu_update_cargo("+");' " 
                  				+" >Update Cargo</btn> </div>"                  			
                     	+ "</div></div>"
                  	+ "</div>"; 	


var $mst_acargo_form ="<div class=''>" 
						+"<div class='card-panel white '>"
              		+ "<form id='edit_cargo' > "
                  			+ "<div class='row'>"
                  			+"<div class='col l4  m4 s12 '>	"
                  				+ "Item Name"
                  				+ "<input name='cargoid' value='{{cargoid}}' type='hidden' /> "
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12' id='product_name'>	"
                  				+ "<input name='product_name' 	value='{{product_name}}' required />"
                  			+ "</div>"

                  			+"<div class='col l4  m4 s12 '>	"
                  				+ "Item Description"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "<input name='description' 	value='{{description}}' required />"
                  			+ "</div>"

                  			+"<div class='col l4  m4 s12 '>	"
                  				+ "Comments"
                  				+ "</div>"		
                 				+ "<div class='col input_field l6  m6 s12'>	"
                  				+ "<input name='comments' 	value='{{comments}}' required />"
                  			+ "</div>"                  			
                  			+"</div>"
                  			+ "<div class='row border'>"
                  			+ $mst_src_agent

									+ "<div class='col l4  m4 s12'>"
	                  				+ "Update Source Address"
                  				+ "</div>"		
                 				+ "<div class='col l6  m6 s12'>	"
										+"<select class='browser-default ' "
														+ " name='src_address_update'   >"
                 						+ $mst_boolean_dropbox
                 					+ "</select>"
                  			+ "</div>"  
									+ "<div class='l12 m12 s12'>&nbsp;</div>"
                  			
                  			+ $mst_src_city_input
	                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_idtype' value='{{src_idtype}}' />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Source ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_id' 	 value='{{src_id}}'/>"
									+ "</div></div>"	                  		
                  			
               			+ "<div class='row border yellow lighten-4'>"
               				+ $mst_dst_agent

									+ "<div class='col l4  m4 s12'>"
	                  				+ "Update Destination Address"
                  				+ "</div>"		
                 				+ "<div class='col l6  m6 s12'>	"
										+"<select class='browser-default ' "
														+ " name='dst_address_update'   >"
                 						+ $mst_boolean_dropbox
                 					+ "</select>"
                  			+ "</div>"  
               				+ "<div class='l12 m12 s12'>&nbsp;</div>"
               				
                  			+ $mst_dst_city_input    
	                  		+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='dst_idtype' value='{{dst_idtype}}' />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "Destination ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='dst_id' 	 value='{{dst_id}}'/>"
									+ "</div></div>"	                  		
                  			              			
                  			+ "<div class='row border'>"

                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Invoice ID (GR Number)</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
            + "<input name='transporter_invoiceid' required value='{{transporter_invoiceid}}' "
            							+" />"
                  			+ "</div>"                        
                  			
                  			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Shipping Weight (KGs)</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
            + "<input name='shipping_weight' required value='{{shipping_weight}}' type='number' min='0.0' />"
                  			+ "</div>"                        
                  			
                  			+"<div class='col l4  m4 s12'>	"
                  				+ "Cartage Price</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
            + "<input name='cartage_price' value='{{cartage_price}}' required type='number' min='0.0' />"
                  			+ "</div>"                      
                 			
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Cartage Paid</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
										+	$mst_cartage_paid
                  			+ "</div>"
                  			+ "<div class='l12 m12 s12'> &nbsp; </div>"
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Pay by Source</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                 					+$mst_payby_src
                  			+ "</div>"

                  			+ "<div class='l12 m12 s12'> &nbsp; </div>"
                  			+ "<div class='col l4  m4 s12'>	"
                  				+ "Door delivery</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
										+	$mst_door_delivery
                  			+ "</div></div>"
                  			
                  			
                  	+ "</div>"//card-panel
                  	+ $mst_acargo_more_form
                  	+ "</form>"
                  	
                  	+ $mst_cargo_update_btn
                  	
               	+"</div>"	;


var $mst_edit_cargo ="<ul class='collapsible collapsible-accordion'  data-collapsible='accordion'>"
					+"<li>" 
                      + "<div class='collapsible-header waves-effect waves-purple "
                      					+" center-align "+ $zebra_a +" active'>"
                      					+ "<div class='row'>"
                      					+ "<div class='col  l1  m1  s1 center tag'> &#10095;</div>"
                                    + "<div class='col l11 m11 s11'> Cargo Info for C-{{cargoid}}</div>"
                                    + "</div>"
                      + "</div>"
                      + "<div class='collapsible-body black-text yellow lighten-5'>"
                      		+ $mst_cargo_info        
							 + "</div>"
					+ "</li>"		 							
					+	"<li>" 
                      + "<div class='collapsible-header waves-effect waves-purple "
                      					+ " "+ $zebra_b +" center-align'>"
                      					+ "<div class='row'>"
                      					+ "<div class='col  l1  m1  s1 center tag'> &#10095;</div>"
                                    + "<div class='col l11 m11 s11'> Edit Cargo C-{{cargoid}}</div>"
                                    + "</div>"
                      + "</div>"
                      + "<div class='collapsible-body black-text yellow lighten-5'>"
                      				+ $mst_acargo_form        
               		 + "</div>"
               + "</li>"
               + "</ul>";

/*
* Function fx_form_add_cargo(src_customerid)
* Display Form for Adding Cargo
*/
function fx_form_add_cargo($userid)
{
		var $xval = {};
		$xval.userid = $userid;
      var $div_acargo = Mustache.to_html($mst_add_cargo,$xval);
      return $div_acargo;
}

/*
* FX: fx_fetch_cargo($div_pgx,cargoid)
*
*/


function fx_fetch_cargo($div_pgx,$cargoid)
{
  var $url  		 = "/biz/cargo/"+$cargoid;
  var $rest_pg_txt = "#rest_pagination_text";
  
  $($div_xpg).empty();
	
  $.ajax( 
            { 
           url: 			$url ,
           type: 			"GET",
           dataType: 	"json",
           error: function (xhr,status,error) 
           {
           				$($div_xpg).empty();				
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_xpg).append($error_msg);
           },
           success: function( data )
           {
                  fx_form_edit_cargo($div_pgx,$cargoid,data);
                 	//Materialize.toast("Fetching Cargo", 5000);
                 	$('.collapsible').collapsible('close', 0);
           },
                                       //GetJSON Function_data
        });
        //ajax


}

/*
* Function fx_form_edit_cargo(cargoid)
* Display Form for editing Cargo
*/
function fx_form_edit_cargo($div_pgx,$cargoid,$xdata)
{
     	//Materialize.toast("Cargo", 5000);

		var $x_cargo = ($xdata.items).pop();     	
     	
      var $div_acargo 	= Mustache.to_html($mst_edit_cargo,$x_cargo);
      $($div_pgx).append($div_acargo);
      
      fxu_city_init();//City ajax init
      fxu_agent_init();
      //return $div_acargo;
}


/*
* Function add_cargo(val)
* Display Form for Adding Cargo
*/
function fxu_create_cargo()
{
		var $div_form			= '#add_cargo';
		
		var $input_h = x_getform_input($div_form);
		
		
		var $in_m = 'biz/cargo';
  		$.ajax( 
            { 
           url: $in_m ,
           type: "POST",
           dataType: "json",
           data: $input_h,
           headers: { 
                    },
            error: function (xhr,status,error) 
            {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            },
            success: function( data )
            {
                  display_cargo(data);
                  
            },
                                       //GetJSON Function_data
        });
        //ajax

}

/*
* Function fxu_update_cargo(val)
* Display Form for Adding Cargo
*/
function fxu_update_cargo()
{
		var $div_form			= '#edit_cargo';
		var $div_form_more	= '#edit_cargo_more';
		
		var $input_h	= x_getform_input($div_form);
		var $cargoid   = $input_h.cargoid;
		
		var $in_m = 'biz/cargo/'+$cargoid;
		
		if($cargoid)
		{
	  		$.ajax( 
           { 
           url: 		$in_m ,
           type: 		"PUT",
           dataType: "json",
           data: 		$input_h,
           error: function (xhr,status,error) 
            {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            },
            success: function( data )
            {
                  display_cargo(data);
                  fx_fetch_cargo($div_xpg,cargoid);
            },
                                       //GetJSON Function_data
        });
        //ajax
        }
        	else
        {
        		Materialize.toast("Cargo Invalid",3000);
        }

}


var $mst_edit_mover =    
              "<div class='card-panel white'>" 
              		+ "<form id='edit_mover_{{userid}}' > "
                  			+ "<div class='row'><div class='col l12  m12 s12 center-align green-text'>	"
                  				+ 	"<b>Edit Client: {{userid}}</b>"		
                  				+ 	"<input name='userrid' value='{{userid}}' "
                  				+	" DISABLED type='hidden'/>"
                  			+ "</div></div>"
                  			+ "<div class='row border'><div class='col l4  m4 s12'>	"
                  				+ "Default City</div>"		
                 				+ "<div class='col l8  m8 s12'>	"
                 					+ "<input name='city' 		id='sin_city' 			required 	/>"
                 					+ "<input name='citycode' 	id='sin_citycode' 	type='text' DISABLED />"
									+ "</div></div>"                  			
	                  		+ "<div class='row border'><div class='col l4  m4 s12'>	"
	                  				+ "ID Type(GST/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_idtype'  />"
	                  			+ "</div>"             
	                  			+ "<div class='col l4  m4 s12'>	"
	                  				+ "ID (GSTIN/PAN)</div>"		
	                 				+ "<div class='col l8  m8 s12'>	"
	                  				+ "<input name='src_id' 	 />"
									+ "</div></div>"	   
                  			+ "<div class='row border'>"
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "Line1</div>"		
                 					+ "<div class='col l8  m8 s12'>	"
                  					+ "<input name='streetaddress1' />"
	                  				+ "</div>"
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "Line2</div>"		
                 					+ "<div class='col l8  m8 s12'>	"
                  					+ "<input name='streetaddress2' />"
	                  				+ "</div>"	                  
                  				+"<div class='col l4  m4 s12'>	"
                  					+ "Line3</div>"		
                 					+ "<div class='col l8  m8 s12'>	"
                  					+ "<input name='streetaddress3' />"
	                  				+ "</div>"	                  	                  								
                  			+"</div>"                      
                  			+ "<div class='row'><div class='col l12 m12 s12 center-align'>	"
                  				+"<btn class='btn green'>Save Client</btn> </div>"                  			
	                     + "</div></div>"
                  + "</form>"  
               + "</div>";

/***** Ajax City,Src_city,DST_city,Currency: BEGIN */
/*** Dependency: EasyAutoComplete. Also handles Multiple Fields of Ajax ***/


/* 
* Selected Fields from Ajax Drop Down for AutoComplete
* FX: eac_get_selected_value  
*/
function eac_get_selected_value()
{
	var $a_value =	$(".easy-autocomplete").
            			find("ul li.selected div[data-item-value]").attr("data-item-value");
   return $a_value;         			
}
/*
* FX: eac_get_selected_name
*/

function eac_get_selected_name()
{
	var $a_value =	$(".easy-autocomplete").
            			find("ul li.selected div[data-item-value]").text();
   return $a_value;         			
}

/*

* Fill the DivID with the Value
*
*/

function eac_fill_citycode($div_id_fill,$div_value_x)
{
        var selectedItemValue = 
        $(".easy-autocomplete").
            	find("ul li.selected div["+ $div_value_x +"]").attr($div_value_x);
        $($div_id_fill).val(selectedItemValue).trigger("change");
}

/********* Search Ajax City ******/

var city_options = {

	 dataType: 			'json',
	 listLocation: 	'cities',
	 theme: 				'blue-light',

    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
        		var $url =$city_search_url +"?q=" + 
            	phrase + "&format=json&"  ;
            return $url;    
        } 
    },
	
	getValue: function(element)
	{
        return element.name;
   },
    
   template: {
        type: "custom",
        method: function(value, item) 
        {
            return "<div data-item-value='" + item.code + "' >" + value + "</div>";
        }
   },
   list: {
		  onSelectItemEvent: function()        
        {
        		$div_fill = '#sin_citycode';
        		$div_value_x = 'data-item-value';
        		eac_fill_citycode($div_fill,$div_value_x);
        }
    }
};

//$("#sin_city").easyAutocomplete(city_options);


/*
* Use this way
 <div class='row'>
   <div class='col l6 m8 s10'>
      <input id="sin_city" 		type="text" class="" name="city" placeholder='City' />
      <input id="sin_citycode" 	type="text" name="sin_citycode" />
    </div>
</div>
*
*/
var src_city_options = {

	 dataType: 			'json',
	 listLocation: 	'cities',
	 theme: 				'blue-light',

    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
        		var $url = $city_search_url + "?q=" +	phrase + "&format=json&"  ;
            return $url;        	
        } 
    },
	
	getValue: function(element)
	{
        return element.name;
   },
    
   template: {
        type: "custom",
        method: function(value, item) 
        {
            return "<div data-item-value_src='" + item.code + "' >" + value + "</div>";
        }
   },
   list: {
		  onSelectItemEvent: function()        
        {
        		$div_fill 		= '#src_citycode';
        		$div_value_x 	= 'data-item-value_src';
        		eac_fill_citycode($div_fill,$div_value_x);
        }
    }
};
//$("#src_city").easyAutocomplete(src_city_options);

/** Destination City

 <div class='row border'>
   <div class='col l6 m8 s10'>DST
      <input id="dst_city" 		type="text" name="dst_city" 		placeholder='DST City' />
		<input id="dst_citycode" 	type="text" name="dst_citycode" 	value=''/>  
    </div>
</div>


*/

var dst_city_options = {

	 dataType: 			'json',
	 theme: 				'green-light',
	 listLocation: 	'cities',//items in the json data

    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
        		var $url = $city_search_url + "?q=" +	phrase + "&format=json&"  ;
            return $url;        	
        } 
    },
	
	getValue: function(xelement)
	{
        return xelement.name;
   },
    
   template: {
        type: "custom",
        method: function(xvalue, xitem) 
        {
            return "<div data-item-value_dst='" + xitem.code + "' >" + xvalue + "</div>";
        }
   },
   list: {
		  onSelectItemEvent: function()        
        {
        		$div_fill 	= '#dst_citycode';
        		$div_value_x 	= 'data-item-value_dst';
        		eac_fill_citycode($div_fill,$div_value_x);
        }
    }
};
//$("#dst_city").easyAutocomplete(dst_city_options);

/**** Currency Auto Search
<div class='row border'>
   <div class='col l6 m8 s10'>CC
      <input id="currency" 		type="text" name="currency" 		placeholder='CC' />
		<input id="ccode" 			type="text" name="ccode" 	value=''/>  
    </div>
</div>
*/
var currency_options = {

	 dataType: 			'json',
	 theme: 				'blue-light',

    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
        		var $url = $currency_search_url + "/" + phrase;
            return $url;    
        } 
    },
	
	getValue: function(xelement)
	{
        return xelement.name;
   },
    
   template: {
        type: "custom",
        method: function(xvalue, xitem) 
        {
            return "<div class='green-text' data-item-value_cc='" 
            				+ xitem.code 
            				+ "' >" + xvalue 
            			+ "</div>";
        }
   },
   list: {
		  onSelectItemEvent: function()        
        {
       	  $div_fill = '#ccode';
        		$div_value_x 	= 'data-item-value_cc';
        		eac_fill_citycode($div_fill,$div_value_x);
        }
    }
};
//$("#currency").easyAutocomplete(currency_options);

/*
* FX: fxu_city_init: 
* City Init: sin_city,src_city,dst_city
*
*/
function fxu_city_init()
{
	$("#sin_city").easyAutocomplete(city_options);
	$("#src_city").easyAutocomplete(src_city_options);
	$("#dst_city").easyAutocomplete(dst_city_options);
	$('div.easy-autocomplete').removeAttr('style');//Hack for input width
}

/*
* FX: fxu_agent_init: 
* City Init: src_customerid,dst_customerid
*

 <div class='row border'>User
   <div class='col l6 m8 s10'>
      <input id="src_customer_name" 		type="text" name="src_customer_name" placeholder='' />
		<input id="src_customerid" 			type="text" name="src_customerid" value=''/>  
    </div>
</div>

<div class='row border'>User
   <div class='col l6 m8 s10'>
      <input id="dst_customer_name" 		type="text" name="dst_customer_name" placeholder='' />
		<input id="dst_customerid" 			type="text" name="dst_customerid" value=''/>  
    </div>
</div>
*
*/

function fxu_agent_init()
{
	$("#src_customer_name").easyAutocomplete(src_agent_options);
	$("#dst_customer_name").easyAutocomplete(dst_agent_options);
	$('div.easy-autocomplete').removeAttr('style');
}

var src_agent_options = {

	 dataType: 			'json',
	 theme: 				'blue-light',
	 listLocation: 	'items',//items in the json data
	 highlightPhrase: true,
	 
    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
        		var $url = $agent_search_url + "/1" + "?name=" + phrase;
            return $url;    
        } 
   },
	getValue: function(xelement)
	{
        return xelement.name;
   },
   template: 
   {
        type: "custom",
        method: function(xvalue, xitem) 
        {
            return "<div class='green-text' data-item-value_src_userid='" 
            				+ xitem.userid 
            				+ "' >" + xitem.name 
            			+ "</div>";
        }
   },
   list: 
   {
		  onSelectItemEvent: function()        
        {
       	  	$div_fill 		= '#src_customerid';
        		$div_value_x 	= 'data-item-value_src_userid';
        		eac_fill_citycode($div_fill,$div_value_x);
        }
    }
};

var dst_agent_options = {

	 dataType: 			'json',
	 theme: 				'green-light',
	 listLocation: 	'items',//items in the json data
	 
    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
        		var $url = $agent_search_url + "/1" + "?name=" + phrase;
            return $url;    
        } 
   },
	getValue: function(xelement)
	{
        return xelement.name;
   },
   template: 
   {
        type: "custom",
        method: function(xvalue, xitem) 
        {
            return "<div class='green-text' data-item-value_dst_userid='" 
            				+ xitem.userid 
            				+ "' >" + xitem.name 
            			+ "</div>";
        }
   },
   list: 
   {
		  onSelectItemEvent: function()        
        {
       	  	$div_fill 		= '#dst_customerid';
        		$div_value_x 	= 'data-item-value_dst_userid';
        		eac_fill_citycode($div_fill,$div_value_x);
        }
    }
};

/***** Ajax City,Src_city,DST_city,Currency: END */

